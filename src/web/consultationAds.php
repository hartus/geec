<?php
include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/check_session.php';
/**
 * @brief : Page de consultation d'annonce
 * @version : 1.0.0
 * @since : 01.02.19
 * @author : Bytyçi
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */

$varCategories = ECategoriesHelper::GetCategories();
$varStates = GetStates();

$email = ESession::getEmail();

$userAccount = EUserHelper::GetUserByEmail($email);
$name = $userAccount->name;
$edumail = $userAccount->email;
$privateEmail = $userAccount->privatemail;
$privatePhone = $userAccount->privatephone;
// Pour afficher la search bar
$SHOW_SEARCH_BAR = true;
// Quel contexte pour les paramètres avancés de la search bar
$SEARCH_BAR_ADVANCED = SB_CONTEXT_ADS;

$idAd = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING,FILTER_NULL_ON_FAILURE);
$annonce = EAdsHelper::getAdById($idAd, ADS_STATUS_ACTIVATED);

$p = ReadPreferences();
// si 1 on est en vacances et on ne met pas a jour les annonces, sinon on peut les mettre a jour automatiquement
if($p->FlagHoliday == 0)
    EAdsHelper::CheckAdsActivations();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bourse aux livres</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript" src="./js/eelauth.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <script src="./js/functions.js"></script>

    <script src="./js/inc.all.constants.js"></script>

    <!-- ============= CUSTOM ============= -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- ============= END ============= -->

</head>

<body style="background-color:transparent;">
    <style>
    .background-image {
    position: absolute;
    background-image: url(../img/test.png);
    background-repeat: no-repeat;
    background-size: cover;
    height: 980px;
    width: 45%;
    top: 0;
    right: 0;
    z-index: -1;
    }
    </style>
    <?php include './php/includes/styles/header.php'; ?>
    <div class="background-image">
        <img src="./img/delimiter3.png" alt="">
    </div>
    <div class="container-fluid" style="margin-top:80px !important;">
        <div class="row">
            <div id="infoMessage"></div>
            <div style="display:none;" class="alert col-md-5 alert-success mx-auto text-center mt-4" id="searchDone">
                Recherche
                terminée</div>

            <div class="col-md-8 offset-md-2">
                <div class="card mt-4 mb-4 shadow-lg uk-animation-slide-bottom-small" style="margin: auto;border:none;border-radius:20px;">
                    <table class="table table-responsive table-bordered table-striped" style="border: transparent;" cellspacing="0" width="100%">
                        <thead>
                            <tr style="border:none;">
                                <th class="th-sm text-center" style="border-left:none;border-top:none;">IMG</th>
                                <th class="th-sm" style="border-top:none;min-width:400px;">Description</th>
                                <th class="th-sm text-center" style="border-top:none;">Catégorie</th>
                                <th class="th-sm text-center" style="border-top:none;">État</th>
                                <th class="th-sm text-center" style="border-top:none;">Prix</th>
                                <th class="th-sm text-center" style="border-right:none;border-top:none;">Date de publication</th>
                            </tr>
                        </thead>

                        <tbody id="tableContent">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>

        var cats = <?php echo json_encode($varCategories);?>;
        var states = <?php echo json_encode($varStates);?>;

        $(document).ready(function () {
            $("#SearchButton").click(searchAds);
            // Au chargement de la page on recherche toutes les annonces.
            searchAds();
        });

        /**
         * Recherche les ads en fonction du text
         */
        function searchAds(event) {
            if (event != undefined)
                event.preventDefault();
            $.ajax({
                method: 'POST',
                url: "./php/ajax/searchAds.php",
                // .serialize permet de placer dans le post toutes les
                // données des input de la forme
                data: $("form[name='searchForm']").serialize(),
                dataType: 'json',
                success: function (data) {
                    var msg = '';
                    switch (data.ReturnCode) {
                        case 0: // tout bon
                            showElementById("searchDone", 3);
                            showAds(data.Data);
                            break;
                        case 2: // problème de récupération de données
                        case 3: // problème d'encodage
                        default:
                            msg = data.Message;
                            break;
                    }
                    if (msg.length > 0) {
                        displayMessageById("#infoMessage", msg, 3);
                    }
                },
                error: function (jqXHR) {
                    // console.log(jqXHR);
                }
            });
        }

        /**
         * Affiche tout les ads qu'on récupère de l'ajax
         * arr l'objet ajax que l'on récupère
         */
        function showAds(arr) {
            $("#tableContent").empty();
            arr.forEach(function (ads) {
                var Category = "#err#";
                if (cats.length > 0){
                    cats.forEach(function(item) {
                        if (ads.categorie == item.id)
                            Category = item.name;
                    });
                }

                var State = "#err#";
                if (states.length > 0){
                    states.forEach(function(item) {
                        if (ads.state == item.code)
                            State = item.name;
                    });
                }
                var row = '<tr><td style="border-left:transparent;text-align:center; position: relative;min-width: 128px"><a href="post_page.php?id=' + ads.Id + '">';
                if (ads.rawImage.length == 0)
                    row += '<img id="img-consult-ads" src="../img/default-img-ads.jpg" width="128" height="128" style="object-fit: contain; text-align:center;"></a></td>';
                else
                    row += '<img id="img-consult-ads" src="' + ads.rawImage + '" width="128" height="128" style="object-fit: contain; text-align:center;"></a></td>';
                row += '<td style="min-width: 165px; min-height: 165px; overflow-y: auto;"><p class="mt-2"><a style="text-decoration:none;color:black;" href="post_page.php?id=' + ads.Id + '"><b>' + ads.Name + '</a></b></p>' + '<p>' + limitString(ads.Description, 100) + '</p></td>' +
                    '<td class="text-center" style="position: relative; min-width: 90px"><span style="position: absolute; transform: translate3D(-50%, -50%, 0); top: 50%; margin: auto;">' + Category + '</span></td>' +
                    '<td class="text-center" style="position: relative; min-width: 155px"><span style="position: absolute; transform: translate3D(-50%, -50%, 0); top: 50%; margin: auto;">' + State + '</span></td>' +
                    '<td class="text-center" style="position: relative; min-width: 75px"><span style="position: absolute; transform: translate3D(-50%, -50%, 0); top: 50%; margin: auto;">' + ads.Price + '.-</span></td>' +
                    '<td style="border-right:transparent;text-align:center;position: relative; min-width: 155px"><span style="position: absolute; transform: translate3D(-50%, -50%, 0); top: 50%">' + ads.publicationDate + '</span></td>' +
                    '</tr>';
                $('table').append(row);
            })
        }

        /**
         * Limite le nombre de caractère d'une chaine de caractère donnée
         * @param {string} s Le string que l'on coupe
         * @param {int} len La longeure le nombre de caractère maximum de la chaine
         * @param {string} trail Le caractère de remplacement après le nombre max Defaul "..."
         */
        function limitString(s, len, trail = "...") {
            if (s.length > len) {
                s = s.substring(0, len - trail.length) + trail;
            }
            return s;
        }
    </script>
    <!--CDN POUR LE JQUERY-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>
</body>
<?php include './php/includes/styles/footer.php'; ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>

</html>