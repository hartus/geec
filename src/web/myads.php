<?php
include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/check_session.php';
    
$varCategories = ECategoriesHelper::GetCategories();
$varStates = GetStates();

$email = ESession::getEmail();

$userAccount = EUserHelper::GetUserByEmail($email);
$name = $userAccount->name;
$edumail = $userAccount->email;
$privateEmail = $userAccount->privatemail;
$privatePhone = $userAccount->privatephone;

$adsarr = EAdsHelper::getAdsByEmail($email, 50, 4, ADS_STATUS_ALL);

if($userAccount->bann == 1)
{
    header("Location: ./banned.php");
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

     <!-- ============= LINKS ============= -->
     <link rel="stylesheet" href="./css/uikit.min.css">

    <link rel="stylesheet" href="../css/style.css">
    <title>Bourse aux livres</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript" src="./js/eelauth.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
        crossorigin="anonymous">

    <!-- ============= CUSTOM ============= -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- ============= END ============= -->

</head>

<body style="background-color:transparent;">
<style>
    .background-image {
    position: absolute;
    background-image: url(../img/test.png);
    background-repeat: no-repeat;
    background-size: cover;
    height: 980px;
    width: 45%;
    top: 0;
    right: 0;
    z-index: -1;
    }
</style>
    <?php include './php/includes/styles/header.php'; ?>
    <div class="background-image">
        <img src="./img/delimiter3.png" alt="">
    </div>
    <div class="container-fluid" style="margin:auto;margin-top:100px !important;">
    <div class="col-md-8 offset-md-2">
        <div id="myadsCard" class="card mb-4 mt-4 shadow-lg uk-animation-slide-bottom-small mx-auto" style="border:transparent;border:none;border-radius:20px;">
            <table class="table table-responsive table-striped table-bordered table-sm" style="border:none;" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="border-top:transparent;border-left:transparent;" class="th-sm text-center">IMG</th>
                        <th style="border-top:transparent;" class="th-sm">Description</th>
                        <th class="th-sm text-center" style="border-top:transparent;">Prix</th>
                        <th style="border-top:transparent;border-right:transparent;" class="th-sm text-center" style="border-top:transparent;border-right:transparent;">Date de publication</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if ($adsarr !== false)
                    {
                        foreach ($adsarr as $ads) {
                            if(strlen($ads->saleDate) == 0){
                    ?>
                    <tr>
                        <td style="object-fit: contain; text-align:center;min-width: 128px;min-height:128px;margin: auto;"><a href="./ads.php?id=<?php echo $ads->Id ?>"><img src="<?php echo $ads->rawImage; ?>" height="128" width="128" style="object-fit: contain;"></a></td>
                        <td class="">
                                <?php 
                                    $x = limitString($ads->Description, 300); 
                                    echo '<p class="mb-2 mt-2"><a style="text-decoration:none;" href="./ads.php?id='.$ads->Id.'"><b>'.$ads->Name.'</b><br><span style="color:black;">'.$x.'</span></a></p>';
                                ?>
                        </td>
                        <td class="text-center" style="min-width: 140px;min-height:140px; transform: translate3D(0%, 30%, 0);margin: auto;">
                            <?php echo $ads->Price . '.- CHF'; ?>
                        </td>
                        <td class="text-center" style="border-right:transparent;min-width: 140px;min-height:140px; transform: translate3D(0%, 30%, 0);margin: auto;">
                            <?php $date = new DateTime($ads->publicationDate); echo $date->format('d.m.Y'); ?>
                        </td>
                    </tr>
                <?php
                } // #end if
                } //#end foreach
                    }// #endif
                ?>
                </tbody>
            </table>

        </div>
    </div>
    </div>

    <?php include './php/includes/styles/footer.php'; ?>
    <!--CDN POUR LE JQUERY-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>
</body>

</html>