<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

// Detruire la session
ESession::Destroy();


?>


<html>
    <head>
        <title>Bourse aux livres</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://apis.google.com/js/platform.js"></script>
        <script type="text/javascript" src="./js/eelauth.js"></script>
    </head>
    <body>
        <script type="text/javascript">

        $(document).ready(function () {

            // alwaysLowered est mis à 1 afin de placer la fenêtre de déconnexion derrière
            // une fois qu'on remet le focus sur la fenêtre principale
            var strWindowFeatures = "menubar=no,location=no,resizable=no,scrollbars=no,status=no,alwaysLowered=1";            
            // Mettre un nom à notre fenêtre
            window.name = "dummyWindows";
            /* @todo Vérifier si nécessaire de revoke all scopes

            // Initialiser l'api people de google
            // en lui passant une call-back pour la mise à jour
            // du status de la personne connectée.
            //EELAuth.EELClientInitialize(updateSigninStatus);

            // Revoke les accès
            //EELAuth.revokeAllScopes();
            */
            
            // On lance la fenêtre de déconnexion
            window.open('https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout', "LogoutEEL", strWindowFeatures);

            window.location = "/index.php";

            });

            function updateSigninStatus(isSignedIn) {
            }

        </script>

    </body>
</html>
