<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';
if(isset($_GET['user'])){
    $email = filter_input(INPUT_GET, 'user', FILTER_SANITIZE_EMAIL);
}
$userAccount = EUserHelper::GetUserByEmail($email);

if($userAccount->bann == 1)
{
    header("Location: ./banned.php");
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/style.css">
    <title>Bourse aux livres</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript" src="./js/eelauth.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
        crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid">
        <div class="card mt-4" style="width: 70rem;margin: auto;">
            <div class="card-body">
                <h5 class="card-title conditions">Conditions Générales d'Utilisation du site <span class="conditions-url">CFPTroc.ch</span></h5>
                <p class="card-text">
                <h6 class="conditions">Article 1 :	OBJET</h6>
Les présentes conditions générales d'utilisation (CGU) ont pour objet l'encadrement juridique des modalités de mise à disposition des services du site <span class="conditions-url">CFPTroc.ch</span> et leur utilisation par l'<span class="conditions-user">Utilisateur</span>.
Les conditions générales d'utilisation doivent être acceptées par tout <span class="conditions-user">Utilisateur</span> souhaitant accéder au site. Elles constituent le contrat entre le site et l'<span class="conditions-user">Utilisateur</span>. L’accès au site par l’<span class="conditions-user">Utilisateur</span> signifie son acceptation des présentes conditions générales d’utilisation.
En cas de non-acceptation des conditions générales d'utilisation stipulées dans le présent contrat, l'<span class="conditions-user">Utilisateur</span> se doit de renoncer à l'accès des services proposés par le site.
<span class="conditions-url">CFPTroc.ch</span> se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes conditions générales d'utilisation.
<br/><span class="conditions-warning">En cas de non-respect de ces conditions générales d'utilisation, l'Utilisateur sera immédiatement et définitivement banni du site.</span>
<br/>
                <h6 class="conditions">Article 2 :	DEFINITIONS</h6>
La présente clause a pour objet de définir les différents termes essentiels du contrat :
    <span class="conditions-user">Utilisateur</span> : ce terme désigne toute personne qui utilise le site ou l'un des services proposés par le site.
Contenu utilisateur : ce sont les données transmises par l'<span class="conditions-user">Utilisateur</span> au sein du site.
Identifiant et mot de passe : c'est l'ensemble des informations nécessaires à l'identification d'un <span class="conditions-user">Utilisateur</span> sur le site. Le mot de passe est confidentiel.


<br/>
                <h6 class="conditions">Article 3 :	ACCES AUX SERVICES</h6>
Le site permet à l'<span class="conditions-user">Utilisateur</span> un accès gratuit aux services suivants :
<ul>
    <li>consultation d'annonces ;</li>
    <li>publication d'annonces ;</li>
    <li>mise en relation avec d'autres <span class="conditions-user">Utilisateurs</span>.</li>
</ul>
Le site est accessible gratuitement et en tout lieu à tout <span class="conditions-user">Utilisateur</span> ayant un accès à Internet. Tous les frais supportés par l'<span class="conditions-user">Utilisateur</span> pour accéder au service (matériel informatique, logiciels, connexion Internet, etc.) sont à sa charge.
Le site met en œuvre tous les moyens mis à sa disposition pour assurer un accès de qualité à ses services. L'obligation étant de moyens, le site ne s'engage pas à atteindre ce résultat.
Tout événement dû à un cas de force majeure ayant pour conséquence un dysfonctionnement du réseau ou du serveur n'engage pas la responsabilité de <span class="conditions-url">CFPTroc.ch</span>.
L'accès aux services du site peut à tout moment faire l'objet d'une interruption, d'une suspension, d'une modification sans préavis pour une maintenance ou pour tout autre cas. L'<span class="conditions-user">Utilisateur</span> s'oblige à ne réclamer aucune indemnisation suite à l'interruption, à la suspension ou à la modification du présent contrat.
L'<span class="conditions-user">Utilisateur</span> a la possibilité de contacter le site par le formulaire de contact.


<br/>
                <h6 class="conditions">Article 4 :	PUBLICATION PAR L’UTILISATEUR</h6>
Le site permet à l’<span class="conditions-user">Utilisateur</span> de publier des annonces. Ces dernières ne doivent servir qu'à proposer un soutien scolaire ou vendre du matériel scolaire utilisé dans le cadre des cours dispensés au sein des formations du CFPT.
L’<span class="conditions-user">Utilisateur</span> ne peut vendre que du matériel lui appartenant ou proposer un soutien scolaire qu'il est en mesure de donner lui-même.
L’<span class="conditions-user">Utilisateur</span> s'engage à fournir une description précise et exacte du matériel qu'il met en vente (état, prix à neuf, ISBN, prix souhaité, etc.) ou du soutien scolaire qu'il propose (branche, niveau, horaires possibles, durée du soutien, tarif horaire, etc.).
Dans ses publications, l’<span class="conditions-user">Utilisateur</span> s’engage à respecter les règlements du CFPT, du DIP, ainsi que les règles de la Netiquette et les règles de droit en vigueur.
Le site exerce une modération sur les publications et se réserve le droit de refuser leur mise en ligne, sans avoir à s’en justifier auprès de l'<span class="conditions-user">Utilisateur</span>.


<br/>
                <h6 class="conditions">Article 5 :	MISE EN RELATION ENTRE UTILISATEURS</h6>
Le site a pour unique vocation de permettre à deux <span class="conditions-user">Utilisateurs</span> d'entrer en contact.
Toute transaction se fait après accord entre les deux parties (acheteur et vendeur), selon leurs termes, et sous leur propre responsabilité.
Il est de la responsabilité de l'acheteur de s'assurer que le matériel qui lui est vendu est bien celui dont il a besoin en cours (par exemple que l'édition convienne et soit approuvée par l'enseignant).
Le CFPT ne propose pas de cours d'appui, ne vend pas de matériel, n'intervient à aucun moment dans les transactions et ne pourra en aucun cas être tenu responsable en cas de transaction litigieuse.

<br/>
                <h6 class="conditions">Article 6 :	PROPRIETE INTELLECTUELLE</h6>
Les marques, logos, signes et tout autre contenu du site font l'objet d'une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d'auteur.
L'<span class="conditions-user">Utilisateur</span> sollicite l'autorisation préalable du site pour toute reproduction, publication, copie des différents contenus.
L'<span class="conditions-user">Utilisateur</span> s'engage à une utilisation des contenus du site dans un cadre exclusivement privé. Une utilisation des contenus à des fins commerciales est strictement interdite.
Tout contenu mis en ligne par l'<span class="conditions-user">Utilisateur</span> est de sa seule responsabilité. L'<span class="conditions-user">Utilisateur</span> s'engage à ne pas mettre en ligne de contenus pouvant porter atteinte aux intérêts de tierces personnes. Tout recours en justice engagé par un tiers lésé contre le site sera pris en charge par l'<span class="conditions-user">Utilisateur</span>. 
Le contenu de l'<span class="conditions-user">Utilisateur</span> peut à tout moment et pour n'importe quelle raison être supprimé ou modifié par le site. L'<span class="conditions-user">Utilisateur</span> ne reçoit aucune justification et notification préalablement à la suppression ou à la modification du contenu utilisateur.


<br/>
                <h6 class="conditions">Article 7 :	DONNEES PERSONNELLES</h6>
L’<span class="conditions-user">Utilisateur</span> se logue sur le site avec ses identifiants EEL.
Le site assure à l’<span class="conditions-user">Utilisateur</span> que les données personnelles supplémentaires qu’il publierait sur le site ne seront pas revendues à des tiers. Le site ne peut cependant pas être garant de la confidentialité de ces données.


<br/>
                <h6 class="conditions">Article 8 :	RESPONSABILITE ET FORCE MAJEURE</h6>
Les sources des informations diffusées sur le site sont réputées fiables. Toutefois, le site se réserve la faculté d'une non-garantie de la fiabilité des sources. Les informations données sur le site le sont à titre purement informatif. Ainsi, l'<span class="conditions-user">Utilisateur</span> assume seul l'entière responsabilité de l'utilisation des informations et contenus du présent site.
L'<span class="conditions-user">Utilisateur</span> s'assure de garder ses informations de connexion (identifiant et mot de passe) secrètes. Toute divulgation de ces dernières, sous quelque forme que ce soit, est interdite.
L'<span class="conditions-user">Utilisateur</span> assume les risques liés à l'utilisation de son identifiant et mot de passe. Le site décline toute responsabilité.
Tout usage du service par l'<span class="conditions-user">Utilisateur</span> ayant directement ou indirectement pour conséquence des dommages doit faire l'objet d'une indemnisation au profit du site.
Une garantie optimale de la sécurité et de la confidentialité des données transmises n'est pas assurée par le site. Toutefois, le site s'engage à mettre en œuvre tous les moyens nécessaires afin de garantir au mieux la sécurité et la confidentialité des données.
La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d'un tiers.



<br/>
                <h6 class="conditions">Article 9 :	ÉVOLUTION DU CONTRAT</h6>
Le site se réserve à tout moment le droit de modifier les clauses stipulées dans le présent contrat.


<br/>
                <h6 class="conditions">Article 10 :	DUREE</h6>
La durée du présent contrat est indéterminée. Le contrat produit ses effets à l'égard de l'<span class="conditions-user">Utilisateur</span> à compter de l'utilisation du service.


<br/>
                <h6 class="conditions">Article 11 :	DROIT APPLICABLE ET JURIDICTION COMPETENTE</h6>
La législation suisse s'applique au présent contrat.

                </p>
                <form action="./php/validationConditions.php" method="POST">
                    <input type="hidden" id="usermail" name="usermail" value="">
                    <input class="card-link" type="checkbox" id="checkboxCond">
                    <label for="checkboxCond">J'ai lu et j'accepte les conditions d'utilisations</label>
                    <div class="row">
                        <div class="col text-right">
                            <button class="btn btn-primary mx-auto" id="confirmButton">Suivant</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            EnableConfirmationButton(false);
            $('#checkboxCond').click(function () {
                EnableConfirmationButton($('#checkboxCond').is(':checked'));
            });

            // Extraire le email de l'url
            var ar = location.search.split("=");
            if (ar[1].length > 0) {
                var email = ar[1];
                $('#usermail').val(email);
            }
            // Le placer dans le formulaire
            var test = document.getElementById("usermail").value = email;
        });


        /**
         * Activer / Desactiver le le bouton de confirmation
         * @param bool True sur le bouton doit être activé, autrement false.
         */
        function EnableConfirmationButton(isChecked) {
            if (isChecked)
                $('#confirmButton').removeAttr("disabled");
            else
                $('#confirmButton').attr("disabled", "disabled");

        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>
</body>

</html>