<?php
/**
 * @description : Appel a la fonction EUserHelper::updateAccept et passe $email en paramètre
 * @version : 1.0.0
 * @since : 13.12.18
 * @author : Cuthbert Sébastien, Bytyci Qendrim
 * @copyright : Entreprise Ecole CFPT-I © 2019
 * @param string $email
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

$email = "";
if (isset($_POST['usermail']))
    $email = filter_input(INPUT_POST, 'usermail', FILTER_SANITIZE_EMAIL);

EUserHelper::updateAccept($email);

header('Location: ..\profile.php');