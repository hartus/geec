<?php
// CTRL K+0 = FERME TOUS CE QUI EST FERMABLE
/**
 * @description : fichier de fonctions.
 * @version : 1.0.0
 * @since : 13.12.18
 * @author : Cuthbert Sébastien, Bytyci Qendrim
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/php/includes/incAll/inc.all.php';

/**
 * Retourne le tableau de ESocialmediaValue pour une annonce donné
 * @param [string] $mail L'email de l'utilisateur
 * @return [array] Tableau d'objets ESocialmediaValue.
 *                 FALSE si un problème survient
 */
function GetSocialMediasForAdd($idAd)
{
    $arr = array();

    // Request permettant de récupérer les valeurs des réseaux sociaux pour cet utilisateur
    $sql = "SELECT ads_socialmedias_default.ADS_ID, ads_socialmedias_default.SOCIALMEDIAS_CODE, socialmedias.NAME, ads_socialmedias_default.DEFAULT FROM ads_socialmedias_default, socialmedias where ads_socialmedias_default.SOCIALMEDIAS_CODE = socialmedias.CODE AND ads_socialmedias_default.ADS_ID = :id";

    try {
        $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
        $stmt->execute(array(':id' => $idAd));

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            // Création du réseau social avec les données provenant de la base de données
            $sm = new ESocialmediaValue($row['USERS_EDU_MAIL'], intval($row['SOCIALMEDIAS_CODE']), utf8_decode($row['SCVALUE']), boolval($row['DEFAULT']));
            array_push($arr, $sm);
        } #end while

        // Si l'on a une exception, qu'on ne retrouve pas de lien vers la BDD / autre,
        // on affiche le message d'erreur correspondant et on on retourne -1 pour l'id afin de dire que la fonction n'est pas valide (Qu'elle n'a pas trouvé les valeurs des réseaux sociaux associés à l'id du réseau social)
    } catch (PDOException  $e) {
        echo "GetSocialMediasForAdd Error: " . $e->getMessage();
        return false;
    }
    // Done
    return $arr;
}

/**
 * Récupère la liste de la table de code d'état
 * 
 * @return array Un tableau de EStateCode
 */
function GetStateCodes()
{
    // Le tableau qui va contenir les EState
    $arr = array();

    $sql = 'SELECT CODE, `NAME` FROM states';
    try {
        $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            // Création de l'état avec les données provenant de la base de données
            $s = new EState($row['CODE'], $row['NAME']);
            array_push($arr, $s);
        } // End while
        // Si l'on a une exception, qu'on ne retrouve pas de lien vers la BDD / autre,
        // on affiche le message d'erreur correspondant et on ne continue pas   
    } catch (PDOException  $e) {
        echo "GetStateCodes Error: " . $e->getMessage();
        return false;
    }
    // Ok je retourne le tableau des EUser
    return $arr;
}

/**
 * Retourne la valeur du status
 * @param [int] $id l'id d'ads
 * @return [string] Le state de l'ads
 *                  FALSE si un problème survient
 */
function GetStateForAds($id)
{
    $sql = "SELECT states.CODE, states.NAME FROM states, ads where states.CODE = ads.STATES_CODE AND ads.ID=:id";

    try {
        $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
        $stmt->execute(array(':id' => $id));

        $s = $stmt->fetchAll();

        // Si l'on a une exception, qu'on ne retrouve pas de lien vers la BDD / autre,
        // on affiche le message d'erreur correspondant et on on retourne -1 pour l'id afin de dire que la fonction n'est pas valide
    } catch (PDOException  $e) {
        echo "GetStateForAds Error: " . $e->getMessage();
        return false;
    }
    // Done
    return $s[0]['NAME'];
}

/**
 * Récupère la liste des états
 * 
 * @return array Un tableau de EState
 */
function GetStates()
{
    // Le tableau qui va contenir les ECategory
    $arr = array();

    // Request permettant de sélectionner le code d'un état et le nom d'un état
    $sql = 'SELECT CODE, `NAME` FROM states';
    try {
        $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            // Création des états avec les données provenant de la base de données
            $s = new EState(intval($row['CODE']), $row['NAME']);
            // Dans 's' on mets les états, dans 'arr' on va mettre
            // le tableau d'états que nous retournera la request situé ci-dessus (CODE,NAME)
            array_push($arr, $s);
        } // End while

        // Si l'on a une exception, qu'on ne retrouve pas de lien vers la BDD / autre,
        // on affiche le message d'erreur correspondant et on ne continue pas
    } catch (PDOException  $e) {
        echo "GetStates Error: " . $e->getMessage();
        return false;
    }
    // Ok je retourne le tableau des EState qu'on aura push au préalable
    return $arr;
}

/**
 * Récupère la liste des réseaux sociaux avec leur code
 * 
 * @return array Un tableau de ESocialmedia
 */
function GetSocialmedias()
{
    // Le tableau qui va contenir les ESocialmedia
    $arr = array();

    // Request permettant de sélectionner le code d'un réseau social
    $sql = 'SELECT CODE, `NAME` FROM socialmedias';
    try {
        $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            // Création des objets 'ESocialmedia' avec les données provenant de la base de données
            $sc = new ESocialmedia(intval($row['CODE']), $row['NAME']);
            // Dans 'sc' on mets les noms des utilisateurs, les noms des réseaux sociaux, leurs valeurs correspondante. On met toutes ces données dans $arr
            // le tableau de réseaux sociaux nous retournera la request situé ci-dessus (EDU_MAIL,NAME,SCVALUE)
            array_push($arr, $sc);
        } // End while

        // Si l'on a une exception, qu'on ne retrouve pas de lien vers la BDD / autre,
        // on affiche le message d'erreur correspondant et on ne continue pas
    } catch (PDOException  $e) {
        echo "GetSocialMedias Error: " . $e->getMessage();
        return false;
    }
    // Ok je retourne le tableau des ESocialMedia qu'on aura push au préalable
    return $arr;
}

/**
 * Fonction permettant de retourner un NAME pour un CODE de réseau social correspondant
 * @return string
 */
function getSocialmediaNameByCode($code)
{
    $sql = 'SELECT `NAME` FROM socialmedias WHERE CODE = :code';
    try {
        // Préparation de la requête
        $req = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
        $req->execute([':code' => $code]);
        $socialmedia = $req->fetch(PDO::FETCH_ASSOC);

        return $socialmedia['NAME'];
    } catch (PDOException  $e) {
        echo "getSocialmediaNameByCode Error: " . $e->getMessage();
        return false;
    }
}

/**
 * Fonction permettant de retourner une VALUE pour un réseau social correspondant
 * @return string
 */
function getSocialmediaValue($id, $email)
{
    $sql = 'SELECT socialmedias.`NAME`, user_socialmedias.SCVALUE
    FROM user_socialmedias, socialmedias, ads
    WHERE user_socialmedias.SOCIALMEDIAS_CODE = socialmedias.`CODE` AND ads.ID=:id AND user_socialmedias.USERS_EDU_MAIL=:email;';
    try {
        // Préparation de la requête
        $req = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
        $req->execute([':id' => $id, ':email' => $email]);

        $socialmedia = array();

        while ($data = $req->fetch(PDO::FETCH_ASSOC)) {
            $socialmedia[$data['NAME']] = $data['SCVALUE'];
        }

        return $socialmedia;
    } catch (PDOException  $e) {
        echo "getSocialmediaValue Error: " . $e->getMessage();
        return false;
    }
}

/**
 * Limiter une chaîne de character à une certaine longueur.
 * Remplacer, si nécessaire la fin par ...
 *
 * @param string $s   La chaîne à traiter
 * @param int $len    La longueur maximum
 * @param string $trail (Optionnel) La chaîne à insérer pour montrer 
 *                                  que la chaîne initiale a été coupée.
 *                      Default "..."
 * @return string   La chaîne coupée ou non.
 */
function limitString($s, $len, $trail = "...")
{

    if (strlen($s) > $len) {
            $s = substr($s, 0, $len - strlen($trail)) . $trail;
        }
    return $s;
}

/**
 * Retourne l'id a partir du nom du status
 *
 * @param [string] $name
 * @return string $result l'id
 */
function GetCodeFromState($name)
{
    $sql = 'SELECT CODE FROM geec.states where `NAME` like :n';
    try {
        $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
        $stmt->execute(array(':n' => $name));

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    } catch (PDOException  $e) {
        echo "GetCodeFromState Error: " . $e->getMessage();
        return false;
    }
    // fail
    return false;
}


/**
 * Retourne le nom a partir du code du status
 *
 * @param [string] $code
 * @return string $result le nom
 */
function GetNameFromState($code){
    $sql = 'SELECT `NAME` FROM states where CODE like :c';
    try {
        $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
        $stmt->execute(array(':c' => $code));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['NAME'];
    } catch (PDOException  $e) {
        echo "GetNameFromState Error: " . $e->getMessage();
        return false;
    }
    // fail
    return false;
}

/**
 * Permet d'envoyer un mail
 *
 * @param [string] $email de la personne à envoyer
 * @param [string] $subject de quoi s'agit-il ?Activé, ban utilisateur...
 * @param [string] $body HTML structure de l'email
 * @param [string] $type data type. (Optional) Par défaut 'text/html'
 * @param [string] $charset     (Optional) Par défaut 'utf-8
 * @return bool true si ok, Swift_TransportException si il y a un problème lors de l'envoi de l'email.
 */
function sendEmail($email, $subject, $body, $type='text/html', $charset = 'utf-8'){
    
    $transport = Swift_SmtpTransport::newInstance(EMAIL_SERVER, EMAIL_PORT, EMAIL_TRANSPORT)
    ->setUsername(EMAIL_EMAIL)
    ->setPassword(EMAIL_PASSWORD);
    
    try {
        
        // On crée un nouvelle instance de mail en utilisant le transport créé précédemment
        $mailer = Swift_Mailer::newInstance($transport);
        // On crée un nouveau message
        $mail = Swift_Message::newInstance();
        // Le sujet du message
        $mail->setSubject($subject);
        // Qui envoie le message
        $mail->setFrom(array('ee.cfpti@gmail.com' => 'Contact Bourse aux livres'));
        // A qui on envoie le message
        $mail->setTo(array($email));

        // On assigne le message et on dit de quel type. Dans notre exemple c'est du html
        $mail->setBody($body, $type, $charset);
        // Maintenant il suffit d'envoyer le message
        $result = $mailer->send($mail);
        if ($result <= 0)
            return false;
    
    } catch (Swift_TransportException $e) {
        return $e;
    }
    // Done
    return true;
}

/**
 * Fonction envoyer un mail après l'activation ou désactivation annonce
 *
 * @param [Eads] $ads de l'annonce
 * @return bool true si ok, Swift_TransportException si il y a un problème lors de l'envoi de l'email.
 */
function sendEmailChangedActivated($ads){
    $email = $ads->Email;
    $status = $ads->actived;
    $name = $ads->Name;
    $id = $ads->Id;
    $user = euserhelper::GetUserByEmail($email);
    //prend le statut actuelle pas encore changé
    if($status == 1){
        $status = "désactivée";
    }
    else{
        $status = "publiée";
    }
    $cat = ECategoriesHelper::GetCategorieForAds($id);
    $cat = strtolower($cat);
    $subject = "Votre annonce ".$ads->Name." a été ".$status.".";

    
    $body = 
    '<html>' .
    ' <head></head>' .
    ' <body>'.
    '  <h3>Bonjour '.strtolower($user->name).'</h3>' .
    '  <p>L\'annonce concernant votre '.$cat.' '.$ads->Name.' a été '.$status.' </p>' .
    '  <a href="https://geec.cfpti.ch/post_page.php?id='.$ads->Id.'">Voir l\'annonce</a>' .
    ' </body>' .
    '</html>';
    return sendEmail($email, $subject, $body);
}

/**
 * Fonction envoyer un mail après la création d'une annonce
 * 
 * @param [EAds] $ads l'annonce que l'on va envoyer
 * @return bool true si ok, Swift_TransportException si il y a un problème lors de l'envoi de l'email.
 */
function sendEmailAddAds($ads){
    $email = ESession::getEmail();
    $subject = "Creation annonce";
    $p = ReadPreferences();
    if($p->FlagHoliday == 0){
        $body = 
        '<html>' .
            ' <head></head>' .
            ' <body>'.
                '<p>Votre annonce "'.$ads->Name.'" a bien été prise en compte.</p>' .
                '<p>Elle sera automatiquement activée et visible sur le site dans '. $p->DayForActivation .' jour(s).</p>'.
                '<p>Veuillez ne pas répondre a ce mail.</p>' .
            ' </body>' .
        '</html>';
    }else{
        $body = 
        '<html>' .
            ' <head></head>' .
            ' <body>'.
                '<p>Votre annonce "'.$ads->Name.'" a bien été prise en compte.</p>' .
                '<p>Le site est actuellement en mode vacances.</p>' .
                '<p>Votre annonce sera automatiquement activée et visible sur le site après les vacances.</p>'.
                '<p>Veuillez ne pas répondre a ce mail.</p>' .
            ' </body>' .
        '</html>';
    }
    $result = sendEmail($email, $subject, $body);
    return $result;
}

/**
 * Fonction envoyer un mail après la création d'une annonce à l'administrateur
 * 
 * @param [EAds] $ads l'annonce que l'on va envoyer
 * @return bool true si ok, Swift_TransportException si il y a un problème lors de l'envoi de l'email.
 */
function sendEmailAddAdsAdmin($ads){
    $varCategories = ECategoriesHelper::GetCategories();
    $cats = $varCategories;

    // Implémentation en PHP
    $Category = ECategoriesHelper::GetNameFromCategorie($ads->Categorie);
    $state = GetNameFromState($ads->State);

    $admin = EUserHelper::GetAllAdmin();
    $email = array();
    $subject = "Création annonce";

    //$body = '<html><body><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAANCAYAAACZ3F9/AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsIAAA7CARUoSoAAAACISURBVChTnZFRCoAgDIZnbz54h4ruf6Ko7uCDD0KlYyutqdQHsUF+zP2qIwA/6Kgi1lrqAKZ+wK9EJhpjqGuDYjpJwntP3Q2KXyYxVzjjYx/eYafKLNuKFcUoZcs2mINcfA5ONB6SeA1qBcVkYpRqQTnnqHuIrXS11tSRKF1PJZvL/wvh1AE4AdF1N2+UK9tMAAAAAElFTkSuQmCC"></body></html>';
    
    $body = 
        '<html>' .
            ' <head>'.
            '</head>'.
            ' <body>'.
                '<p>Ce mail a été envoyé de manière automatique et a pour but de vous informer que l\'élève suivant : '.ESession::getEmail().' a crée une annonce.</p>'.
                '<p>Pour visualiser l\'annonce, cliquez <a href="localhost/post_page.php?id='.$ads->Id.'">ici</a>.</p>'.
                '<p>Vous pouvez activer ou bloquer l\'annonce :</p>'.
                '<a href="localhost/confirmAd.php?id='.$ads->Id.'" style="text-decoration:none;font-weight: bold; font-size: 1rem; color: white; background-color: #47aa5b; padding: 15px; border-radius: 35px;">Activer</a>'.
                '<a href="localhost/blockAd.php?id='.$ads->Id.'" style="text-decoration:none;font-weight: bold; font-size: 1rem; color: white; background-color: #7B0828; padding: 15px; border-radius: 35px;">Bloquer</a>'.
                '<div class="card" style="border:none;border-radius:20px;">'.
                
                '<div class="card-header p-4" style="border-radius:20px 20px 0px 0px;">'.
                    '<div class="row">'.
                        '<div class="col-md-7">'.
                            '<h2 class="mb-0" id="ads-title" style="font-size:2.5rem;;position: absolute; transform: translate3D(0%, 25%, 0);">'.$ads->Name.'</h2>'.
                        '</div>'.
                        '<div>'.
                            '<span id="price" style="font-weight: bold; font-size: 2.5rem; color: #47aa5b; background-color: rgba(86, 194, 107, .3); padding: 15px; border-radius: 35px;">'. $ads->Price.' CHF</span></h2>'.
                        '</div>'.
                    '</div>'.
                '</div>'.
                '<div>'.
                    '<div>'.
                    /*
                    '<div class="col-md-6 col-sm-12 text-center mb-3">'.
                        '<img src="'.$ads->rawImage.'" style="object-fit: contain;border-radius:20px;max-height:300px;">'.
                    '</div>'.
                    */
                    '<div>'.
                    '<h2 style="margin-bottom:15px !important;font-size:17pt;">Informations</h2>'.
                    '<div class="row">'.
                            '<div class="col"><span class="mt-2" style="font-weight:bold">Catégorie : </span> <span>'.$Category.'</span></div>'.
                    '</div>'.
                    '<div class="row">'.
                            '<div class="col"><span class="mt-2" style="font-weight:bold">État : </span> <span>'.$state.'</span></div>'.
                    '</div>'.
                    /*foreach ($ads->criteriasValues as $criteria){
                    //On fait appelle à la fonction qui va prendre le nom du critère par rapport à son id
                    $criteriaName = ECategoriesHelper::getCriteriaNameByCode($criteria->id);
                    .'<div class="row">'.
                        '<div class="col"><span class="mt-2" style="font-weight:bold">'.echo $criteriaName.'</span></div>'.
                        '<div class="col">'. $criteria->value.'</div>'.
                    '</div>'.
                    }*/
                '<div class="row pb-3 pl-3 pr-3 pt-3">'.
                    '<span><span style="font-weight:bold">Description</span> : '.$ads->Description.'</span>'.
                '</div>'.
                '</div>'.
                '</div>'.
            '</div>'.
            '</body>'.
        '</html>';
    

    $retValue = true;

    foreach ($admin as $user) {
        $result = sendEmail($user->email, $subject, $body);
        if ($result !== true)
            $retValue = $result;
    }
    // Retourne true ou l'exception en cas d'erreur
    return $retValue;
}

/**
 * Fonction envoyer un mail a l'utilisateur lorsque il a été bloqué
 * 
 * @param [EUser] $users, l'utilisateur nouvellement bloqué
 * @return bool true si ok, Swift_TransportException si il y a un problème lors de l'envoi de l'email.
 */
function sendEmailUserStatusChanged($users){
    $email = $users->email;
    $status = $users->bann;
    
    
    if($status == 1){
        $status = "Vous avez enfreint les règles d'utilisation du site. Suite à votre comportement sur le site GEEC, votre compte a été banni par l'administrateur.";
    }else{
        $status = "Vous avez été débanni du site GEEC.";
    }
    $subject = "Statut de votre compte GEEC.";
    $body = 
    '<html>' .
    ' <head></head>' .
    ' <body>'.
    '  <h1>'.$subject.'</h1>' .
    '  <p>'.$status.'</p>' .
    '<p>Veuillez ne pas répondre a ce mail.</p>' .
    ' </body>' .
    '</html>';
    return sendEmail($email, $subject, $body);
}

/**
 * Fonction activer annonce depuis un mail
 * 
 * @param $id, l'id de l'annonce a activer
 * @return true si bon, false sinon
 */
function updateActivated($id){
    $sql = 'UPDATE ads SET `actived` = 1 WHERE `id` = :i';
    try {
        // Préparation de la requête
        $stmt = EDatabase::prepare($sql);
        // Execution de la requête
        $stmt->execute(array(
            ':i' => $id,
        ));
    } catch (PDOException  $e) {
        echo "updateActivated Error: " . $e->getMessage();
        return false;
    }
    return true;
}

/**
 * Fonction envoyer un mail après la création d'un nouvel utilisateur
 * 
 * @param [EUser] $users
 * @return bool true si ok, Swift_TransportException si il y a un problème lors de l'envoi de l'email.
 */
function sendEmailNewUser($users){

    $newUser = ESession::getEmail();
    $admin = EUserHelper::GetAllAdmin();
    $email = array();
    $subject = "Nouvelle utilisateur sur la plateforme GEEC";
    $body = 
    '<html>' .
    ' <head></head>' .
    ' <body>'.
    '  <h1>'.$subject.'</h1>' .
    '  <p>Le nouvel utilisateur nommé '.$newUser.' vient de s\'inscrire sur la plateforme d\'échange GEEC.</p>' .
    ' </body>' .
    '</html>';


    $retValue = true;
    foreach ($admin as $user) {
        $result = sendEmail($user->email, $subject, $body);
        if ($result !== true)
            $retValue = $result;
    }
    // Retourne true ou l'exception en cas d'erreur
    return $retValue;
}

/**
 * Met a jour les préférences du mode vacances
 *
 * @param [int] $flag
 * @param [int] $days
 * @return true si la mise a jour n'a pas de probème
 */
function updatePreferences($flag, $days){
    $sql = 'UPDATE preferences SET FLAGHOLIDAY=:f, DAYFORACTIVATION=:d';
    try {
        $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
        $stmt->execute(array(':f'=>$flag, ':d'=>$days, ));

        return true;
    } catch (PDOException  $e) {
        echo "UpdatePreferences Error: " . $e->getMessage();
        return false;
    }
    // fail
    return false;
}

/**
* lit les préférences pour savoir si le mode vacances est activé
* @return $nb, 1 si le mode est activé, 0 sinon
*/
function ReadPreferences(){
    // Le tableau qui va contenir les préfèrences
    $arr = array();

    $p = false;

    $sql = "SELECT * FROM preferences";
    try {
        $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if ($row !== null)
            $p = new EPreference($row['FLAGHOLIDAY'], $row['DAYFORACTIVATION']);
        else
            $p = new EPreference();
        array_push($arr, $p);
        } 
        catch (PDOException  $e) 
        {
            echo "RaedPreference Error: " . $e->getMessage();
            return false;
        }
    return $p;
}

function blockAds($id){
    $sql = 'UPDATE ads SET `actived` = 0,  ACTIVATIONDATE = null WHERE `id` = :i';
    try {
        // Préparation de la requête
        $stmt = EDatabase::prepare($sql);
        // Execution de la requête
        $stmt->execute(array(
            ':i' => $id,
        ));
    } catch (PDOException  $e) {
        echo "updateActivated Error: " . $e->getMessage();
        return false;
    }
    return true;
}