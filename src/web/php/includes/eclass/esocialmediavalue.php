<?php

/**
 * La classe ESocialmediaValue contient les informations 
 * pour un réseau socialdonné :
 * Code du reseau social @see ESocialmediaValue
 * Défaut (Yes/No)
 * La valeur
 */
class ESocialmediaValue{

     /**
     * @brief   Class Constructor avec paramètres par défaut pour construire l'objet
     */
    public function __construct($InEduMail = '', $InCode = -1, $InValue = '', $InDefault = false){
        $this->edumail = $InEduMail;
        $this->code = $InCode;
        $this->value = $InValue;
        $this->default = $InDefault;
    }
    /**
     * Est-ce que l'objet est valide ?
     *
     * @return boolean True si valide, sinon false.
     */
    public function IsValid()
    {
        return ($this->code !== -1);
    }

    /** @var [int] Le mail de l'utilisateur */
    public $edumail;

    /** @var [int] Code unique du réseau social */
    public $code;

    /** @var [string] La valeur */
    public $value;

    /** @var [boolean] Par défaut visible? */
    public $default;


}
