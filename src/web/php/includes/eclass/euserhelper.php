<?php

/**
 * La classe EUserHelper contient des fonctions statiques 
 * pour gérer les utilisateurs
 */
class EUserHelper{

    /**
     * Crée un utilisateur dans la base de données
     *
     * @param [EUser] $u L'objet utilisateur
     * @return boolean True si créé, autrement False
     */
    public static function CreateUser($u){
        /** On met 1 pour value sur 'ROLES_CODE' car les utilisateurs authentifiés sont des élèves. */
        $sql = 'INSERT INTO users (EDU_MAIL, EDU_NAME, URL_PROFIL_PIC, ROLES_CODE, ACCEPT_CONDITIONS, BANN) VALUES(:email, :edu_name, :url_pic, ' . ERL_STUDENT . ', 0, 0)';

        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));

            // Réservation des champs de l'objet Euser pour les attributs en BDD  
            $stmt->bindParam(':email', $u->email, PDO::PARAM_STR);
            $stmt->bindParam(':edu_name', $u->name, PDO::PARAM_STR);
            $stmt->bindParam(':url_pic', $u->imageurl, PDO::PARAM_STR);

            // Execution de la requête
            $stmt->execute();
            return true;
        } catch (PDOException  $e) {
            echo "CreateUser Error: " . $e->getMessage();
            return false;
        }

        // Problème de création
        return false;
    }

    /**
     * Récupère tous les utilisateurs de la base de données
     *
     * @return array Un tableau de EUser
     */
    public static function GetAllUsers(){
        // Le tableau qui va contenir les EUser
        $arr = array();

        $sql = 'SELECT EDU_MAIL, EDU_NAME, URL_PROFIL_PIC, PRIVATE_MAIL, PHONE_NUMBER, PRIVATE_MAIL_DEFAULT, PHONE_NUMBER_DEFAULT, ROLES_CODE, BANN FROM users';
        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {                
                // Création du user avec les données provenant de la base de données
                $u = new EUser($row['EDU_MAIL'], $row['EDU_NAME'], $row['URL_PROFIL_PIC'], $row['PRIVATE_MAIL'], $row['PHONE_NUMBER'], $row['PRIVATE_MAIL_DEFAULT'], $row['PHONE_NUMBER_DEFAULT'], $row['ROLES_CODE'], $row['BANN']);
                // Assigner le tableau dans l'objet user qui contient un tableau des valeurs des 
                // meédia sociaux.
                $u->socialmedias = EUserHelper::GetSocialMediasForUser($u->email);
                // Si on a une erreur, alors on remet à null 
                if ($u->socialmedias === false)
                    return false;
                // On ajoute l'objet utilisateur dans le tableau qu'on va retourner.
                array_push($arr, $u);
            } #end while

        } catch (PDOException  $e) {
            echo "EUserHelper::GetAllUsers Error: " . $e->getMessage();
            return false;
        }
        // Ok je retourne le tableau des EUser
        return $arr;
    }

    /**
     * Récupère un utilisateur donné en paramètre de la base de données
     * @param [string] $email
     * @return EUser Un objet EUser
     */
    public static function GetUserByEmail($email){
        $sql = 'SELECT EDU_MAIL, EDU_NAME, URL_PROFIL_PIC, PRIVATE_MAIL, PHONE_NUMBER, PRIVATE_MAIL_DEFAULT, PHONE_NUMBER_DEFAULT, ROLES_CODE, `BANN` FROM users WHERE EDU_MAIL = :e';
        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute(array(':e' => $email));

            $row = $stmt->fetchAll();
            if (count($row) > 0) {
                // Création du user avec les données provenant de la base de données
                $u = new EUser($row[0]['EDU_MAIL'], $row[0]['EDU_NAME'], $row[0]['URL_PROFIL_PIC'], $row[0]['PRIVATE_MAIL'], $row[0]['PHONE_NUMBER'], $row[0]['PRIVATE_MAIL_DEFAULT'], $row[0]['PHONE_NUMBER_DEFAULT'], $row[0]['ROLES_CODE'], $row[0]['BANN']);
                // Assigner le tableau dans l'objet user qui contient un tableau des valeurs des 
                // meédia sociaux.
                $u->socialmedias = EUserHelper::GetSocialMediasForUser($u->email);
                // Si on a une erreur, alors on remet à null 
                if ($u->socialmedias === false)
                    return false;
                // On retourne l'objet utilisateur
                return $u;
            } #end count
        } catch (PDOException  $e) {
            echo "EUserHelper::GetUserByEmail Error: " . $e->getMessage();
            return false;
        }
        // fail
        return false;
    }

    /**
     * Fonction permettant de mettre à jour le profil d'un utilisateur (PRIVATEMAIL, TEL)
     *
     * @param [EUser] $u L'objet utilisateur
     * @return bool ture si ok, false si problème
     */
    public static function updateUser($u){
        // On commence le processus de transaction
        EDatabase::beginTransaction();

        /** On prépare la request permettant de mettre à jour les attributs en BDD */
        $sql = 'UPDATE users SET PRIVATE_MAIL=:privatemail, PHONE_NUMBER=:privatephone, PRIVATE_MAIL_DEFAULT=:ped, PHONE_NUMBER_DEFAULT=:ppd WHERE EDU_MAIL = :e';
        try {
            // Préparation de la requête
            $req = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));

            // Execution de la requête
            $req->execute(array(
                ':e' => $u->email,
                ':privatemail' => $u->privatemail,
                ':privatephone' => $u->privatephone,
                ':ppd' => $u->privatephonedefault,
                ':ped' => $u->privatemaildefault
            ));
        } catch (PDOException  $e) {
            echo "EUserHelper::updateUser Error: " . $e->getMessage();
            // Une erreur, on roll back
            EDatabase::rollBack();
            return false;
        }
        // On va traiter la liste des valeurs des média sociaux
        // 1) on supprimer tous les enregistrement pour l'utilisateur
        $sql = 'DELETE FROM user_socialmedias WHERE USERS_EDU_MAIL = :e';
        try {
            // Préparation de la requête
            $req = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            // Execution de la requête
            $req->execute(array(':e' => $u->email));
        } catch (PDOException  $e) {
            echo "EUserHelper::updateUser Error: " . $e->getMessage();
            // Une erreur, on roll back
            EDatabase::rollBack();
            return false;
        }
        // 2) on insère tous les enregistrements
        if (count($u->socialmedias) == 0) {
                EDatabase::commit();
                return true;
            }

        $sql = 'INSERT INTO user_socialmedias (USERS_EDU_MAIL, SOCIALMEDIAS_CODE, `DEFAULT`, SCVALUE) VALUES(:email, :sc, :d, :v)';
        try {
            // Préparation de la requête
            $req = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
        } catch (PDOException  $e) {
            echo "EUserHelper::updateUser Error: " . $e->getMessage();
            // Une erreur, on roll back
            EDatabase::rollBack();
            return false;
        }

        foreach ($u->socialmedias as $sc) {
            try {
                // Execution de la requête
                $req->execute(array(
                    ':email' => $sc->edumail,
                    ':sc' => $sc->code,
                    ':d' => $sc->default,
                    ':v' => $sc->value
                ));
            } catch (PDOException  $e) {
                echo "EUserHelper::updateUser Error: " . $e->getMessage();
                // Une erreur, on roll back
                EDatabase::rollBack();
                return false;
            }
        }
        // Ok, on commit
        EDatabase::commit();
        return true;
    }

    /**
     * Récupère l'url de la photo de l'utilisateur
     * @param [string] $email l'email de l'utilisateur
     * @return string l'url de la photo ou false si une erreur est survenue.
     */
    public static function getUserPicture($email){
        // Image de profil stocké en base de l'utilisateur
        $sql = 'SELECT URL_PROFIL_PIC FROM users WHERE EDU_MAIL = :e';
        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute(array(':e' => $email));

            $row = $stmt->fetchAll();
            if (count($row) > 0) {
                return $row[0]['URL_PROFIL_PIC'];
            } #end count
        } catch (PDOException  $e) {
            echo "EUserHelper::GetUserPicture Error: " . $e->getMessage();
            return false;
        }
        // fail
        return false;
    }    
    /**
     * Mettre à jour la photo de profil d'un utilisateur
     * 
     * @param [string] $email L'email utilisateur
     * @param [string] $pictureURL L'email utilisateur
     * @return void
     */
    public static function updateUserPic($email, $pictureURL){

        /** On prépare la request permettant de mettre à jour les attributs en BDD */
        $sql = 'UPDATE users SET URL_PROFIL_PIC = :imageurl WHERE EDU_MAIL = :e';

        try {
            // Préparation de la requête
            $req = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));

            // Execution de la requête
            $req->execute([
                ':e' => $email,
                ':imageurl' => $pictureURL
            ]);
        } catch (PDOException  $e) {
            echo "EUserHelper::updateUserPic Error: " . $e->getMessage();
            // Une erreur
            return false;
        }
        return true;
    }

    /**
     * Fonction permettant de mettre à jour le profil d'un utilisateur (PRIVATEMAIL, TEL)
     *
     * @param [EUser] $u L'objet utilisateur
     * @return void
     */
    public static function updateUserBann($u){
        /** On prépare la request permettant de mettre à jour les attributs en BDD */
        $sql = 'UPDATE users SET bann=:bann WHERE EDU_MAIL = :e';
        try {
            // Préparation de la requête
            $req = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            // Execution de la requête
            $req->execute([
                ':e' => $u->email,
                ':bann' => $u->bann
            ]);
        } catch (PDOException  $e) {
            echo "EUserHelper::updateUserBann Error: " . $e->getMessage();
            // Une erreur, on roll back
            return false;
        }
        return true;
    }

    /**
     * Retourne le tableau de ESocialmediaValue pour un utilisateur donné
     * @param [string] $mail L'email de l'utilisateur
     * @return [array] Tableau d'objets ESocialmediaValue.
     *                 FALSE si un problème survient
     */
    public static function GetSocialMediasForUser($email){
        $arr = array();
        // Request permettant de récupérer les valeurs des réseaux sociaux pour cet utilisateur
        $sql = "SELECT user_socialmedias.USERS_EDU_MAIL, user_socialmedias.SOCIALMEDIAS_CODE, user_socialmedias.DEFAULT, user_socialmedias.SCVALUE FROM user_socialmedias, socialmedias where user_socialmedias.SOCIALMEDIAS_CODE = socialmedias.CODE AND user_socialmedias.USERS_EDU_MAIL=:e";

        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute(array(':e' => $email));

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                // Création du réseau social avec les données provenant de la base de données
                $sm = new ESocialmediaValue($row['USERS_EDU_MAIL'], intval($row['SOCIALMEDIAS_CODE']), utf8_decode($row['SCVALUE']), boolval($row['DEFAULT']));
                array_push($arr, $sm);
            } #end while

            // Si l'on a une exception, qu'on ne retrouve pas de lien vers la BDD / autre,
            // on affiche le message d'erreur correspondant et on on retourne -1 pour l'id afin de dire que la fonction n'est pas valide (Qu'elle n'a pas trouvé les valeurs des réseaux sociaux associés à l'id du réseau social)
        } catch (PDOException  $e) {
            echo "GetSocialMediasForUser Error: " . $e->getMessage();
            return false;
        }
        // Done
        return $arr;
    }

    /**
     * Fonction pour vérifier si l'utilisateur a accepter les conditions d'utilisations
     *
     * @param [string] $email
     * @return boolean True s'il a accepté, autrement False
     */
    public static function UserConditions($email){
        $sql = 'SELECT ACCEPT_CONDITIONS FROM users WHERE EDU_MAIL = :e';
        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute(array(':e' => $email));

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) > 0) {
                    return intval($result[0]["ACCEPT_CONDITIONS"]);
                }
        } catch (PDOException  $e) {
            return false;
        }

        // Par défaut on retourne false
        /* @remark Ici on devrait pouvoir traiter les cas d'erreur
        *         Si on a une erreur, on va retourner false
        *         mais c'est la même chose que si l'utilisateur
        *         n'a pas encore valider les conditions.
        */
        return false;
    }

    /**
     * Fonction qui change la valeur des conditions d'utilisations
     *
     * @param [string] $email Email de l'utilisateur
     * @return true si l'update a fonctionné, false si un problème
     */
    public static function updateAccept($email){
        /** On met 1 pour value sur 'ACCEPT_CONDITIONS' car les utilisateurs ont accepter les conditions si ils arrivent sur cette fonction */
        $sql = 'UPDATE users SET ACCEPT_CONDITIONS = 1 WHERE EDU_MAIL = :e';

        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));

            // Réservation des champs de l'objet Euser pour les attributs en BDD  
            $stmt->bindParam(':e', $email, PDO::PARAM_STR);

            // Execution de la requête
            $stmt->execute();
            return true;
        } catch (PDOException  $e) {
            return false;
        }
        // Problème d'update
        return false;
    }

    /**
     * Test si l'email de l'utilisateur existe 
     *
     * @param [string] $email Contient l'email à tester
     * @return boolean True s'il existe, autrement False
     */
    public static function UserExists($email){
        $sql = 'SELECT EDU_MAIL FROM users WHERE EDU_MAIL = :e';
        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute(array(':e' => $email));

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return (count($result) > 0) ? true : false;
        } catch (PDOException  $e) {
            return false;
        }
        // Not found
        return false;
    }

    public static function GetAllAdmin()
    {
        // Le tableau qui va contenir les EUser
        $arr = array();

        $sql = 'SELECT EDU_MAIL, EDU_NAME, URL_PROFIL_PIC, PRIVATE_MAIL, PHONE_NUMBER, PRIVATE_MAIL_DEFAULT, PHONE_NUMBER_DEFAULT, ROLES_CODE, BANN FROM users WHERE ROLES_CODE = 99';
        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {                
                // Création du user avec les données provenant de la base de données
                $u = new EUser($row['EDU_MAIL'], $row['EDU_NAME'], $row['URL_PROFIL_PIC'], $row['PRIVATE_MAIL'], $row['PHONE_NUMBER'], $row['PRIVATE_MAIL_DEFAULT'], $row['PHONE_NUMBER_DEFAULT'], $row['ROLES_CODE'], $row['BANN']);
                // Assigner le tableau dans l'objet user qui contient un tableau des valeurs des 
                // meédia sociaux.
                $u->socialmedias = EUserHelper::GetSocialMediasForUser($u->email);
                // Si on a une erreur, alors on remet à null 
                if ($u->socialmedias === false)
                    return false;
                // On ajoute l'objet utilisateur dans le tableau qu'on va retourner.
                array_push($arr, $u);
            } #end while

        } catch (PDOException  $e) {
            echo "GetAllAdmin Error: " . $e->getMessage();
            return false;
        }
        // Ok je retourne le tableau des EUser
        return $arr;
    }
}

