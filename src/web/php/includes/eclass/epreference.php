<?php

/**
 * La classe EPreference contient les informations complémentaire à une préférence
 * Ex: Boolean pour vacances et nombre de jours pour validation
 */
class EPreference{

    /**
     * @brief   Class Constructor avec paramètres par défaut pour construire l'objet
     */
    public function __construct($InFlagHoliday = 0, $InDayForActivation = 1){
        $this->FlagHoliday = $InFlagHoliday;
        $this->DayForActivation = $InDayForActivation;

    }    
    /** @var [bool] Mode Vacances activé ou pas */
    public $FlagHoliday;

    /** @var [int] Nombre de jours pour l'activation d'un annonce */
    public $DayForActivation;


}

?>