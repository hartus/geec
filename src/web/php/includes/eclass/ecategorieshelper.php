<?php

/**
 * La classe ECategoriesHelper contient des fonctions statiques 
 * pour gérer les catégories et les critères
 */
class ECategoriesHelper{
    /**
     * Récupère la liste des catégories
     * 
     * @return array Un tableau de ECategory
     */
    public static function GetCategories()
    {
        // Le tableau qui va contenir les ECategory
        $arr = array();

        // Request permettant de sélectionner l'ID d'une catégorie
        $sql = 'SELECT ID, `NAME` FROM categories';
        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                // Création des catégories avec les données provenant de la base de données
                $c = new ECategory(intval($row['ID']), $row['NAME']);
                // Dans 'c' on mets les catégories, dans 'arr' on va mettre
                // le tableau de catégorie que nous retournera la request situé ci-dessus (ID,NAME)
                array_push($arr, $c);
            } // End while

            // Si l'on a une exception, qu'on ne retrouve pas de lien vers la BDD / autre,
            // on affiche le message d'erreur correspondant et on ne continue pas
        } catch (PDOException  $e) {
            echo "GetCategories Error: " . $e->getMessage();
            return false;
        }
        // Une fois lue toutes les catégories,
        // on demande à chaque objet de charger ses critères
        foreach ($arr as $item) {
            // On charge les critères pour l'objet categorie. Si la fonction retourne -1, c'est qu'on a une erreur.
            // On décide de ne pas continuer et de sortir en erreur. (return false)
            if ($item->LoadAllCriterias() < 0) {
                return false;
            }
        }
        // Ok je retourne le tableau des EUser qu'on aura push au préalable
        return $arr;
    }

    /**
     * Retourne la valeur de a catégorie
     * @param [int] $id l'id d'ads
     * @return [string] La catégorie de l'ads
     *                  FALSE si un problème survient
     */
    public static function GetCategorieForAds($id)
    {
        $sql = "SELECT categories.ID, categories.NAME FROM categories, ads where categories.ID = ads.CATEGORIES_ID AND ads.ID=:id";

        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute(array(':id' => $id));

            $c = $stmt->fetchAll();

            // Si l'on a une exception, qu'on ne retrouve pas de lien vers la BDD / autre,
        } catch (PDOException  $e) {
            echo "GetCategorieForAds Error: " . $e->getMessage();
            return false;
        }
        // Done
        return $c[0]['NAME'];
    }

    /**
     * Récupérer le libellé de la catégorie en fonction de son id
     *
     * @param [tableau de ECategory] $arr   Le tableau des ECategory
     * @param [int] $id     Le nom recherché pour cet id de catégorie
     * @return [string]     Le libellé de la catégorie.
     *                      False si pas trouvé.
     */
    public static function GetCategoryNameFromId($arr, $id)
    {
        foreach ($arr as $item) {
            if ($item->id == $id) {
                return $item->name;
            }
        }
        // Not found
        return false;
    }

    /**
     * Retourne l'id a partir du nom de la categorie
     *
     * @param [string] $name
     * @return integer $result l'id
     */
    public static function GetIdFromCategorie($name)
    {
        $sql = 'SELECT ID FROM geec.categories where `NAME` like :n';
        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute(array(':n' => $name));

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) > 0){
                return intval($result[0]['ID']);
            }
        } catch (PDOException  $e) {
            echo "GetIdFromCategorie Error: " . $e->getMessage();
            return false;
        }
        // fail
        return false;
    }

    /**
     * Récupère la liste des critères
     * @return array Un tableau de ECriteria
     */
    public static function GetAllCriterias()
    {
        $criterias = array();
        // Request permettant de prendre l'intégralité des critères qui sont propre à l'id d'une catégorie (ex : ID_CATEGORIE = 1, NAME = Livre | Liste des critères : ISBN, Année de parution, Editeur)
        $sql = "SELECT criterias.ID, criterias.NAME FROM criterias";

        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                // Création du critère avec les données provenant de la base de données
                $c = new ECriteria(intval($row['ID']), $row['NAME']);
                $criterias[] = $c;
            } #end while

            // Si l'on a une exception, qu'on ne retrouve pas de lien vers la BDD / autre,
            // on affiche le message d'erreur correspondant et on on retourne -1 pour l'id afin de dire que la fonction n'est pas valide (Qu'elle n'a pas trouvé les critères associés à la catégorie sélectionné)
        } catch (PDOException  $e) {
            echo "GetAllCriterias Error: " . $e->getMessage();
            return false;
        }
        return $criterias;
    }




    /**
     * Retourne le nom a partir de l'id de la categorie
     *
     * @param [string] $name
     * @return integer $result l'id
     */
    public static function GetNameFromCategorie($id)
    {
        $sql = 'SELECT `NAME` FROM categories where ID like :i';
        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute(array(':i' => $id));

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result[0]['NAME'];
        } catch (PDOException  $e) {
            echo "GetNameFromCategorie Error: " . $e->getMessage();
            return false;
        }
        // fail
        return false;
    }


    /**
     * Retourne l'id a partir du nom de la categorie
     *
     * @param [string] $name
     * @return integer $result l'id
     */
    public static function GetIdFromCriteria($name)
    {
        $sql = 'SELECT ID FROM geec.criterias where `NAME` like :n';
        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute(array(':n' => $name));

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) > 0){
                return intval($result[0]['ID']);
            }
        } catch (PDOException  $e) {
            echo "GetIdFromCriteria Error: " . $e->getMessage();
            return false;
        }
        // fail
        return false;
    }

    /**
     * Récupérer l'id en fonction du nom d'un critère
     *
     * @param [array] $arr   Le tableau des Ecriteria
     * @param [string] $name  Le nom recherché
     * @return [integer] -1 si pas trouvé.
     */
    public static function GetIdFromCriteriaName($arr, $name){
        foreach ($arr as $item) {
            if ($item->name == $name) {
                return $item->id;
            }
        }
        // Not found
        return -1;
    }

    /**
     * Récupérer le nom d'un critère en fonction de son id
     *
     * @param [type] $arr   Le tableau des Ecriteria
     * @param [type] $id    L'id recherché
     * @return string Le nom correspondant à l'id ou "" si pas trouvé
     */
    public static function GetNameFromCriteriaId($arr, $id){
        foreach ($arr as $item) {
            if ($item->id == $id) {
                return $item->name;
            }
        }
        // Not found
        return "";
    }

    /**
     * Récupère la liste des valeurs d'un critètre en fonction de l'id de ce critère
     * @param @TODO
     * @return array Un tableau de @TODO
     */
    public static function GetValueByCriteriaId($idAd){
        // Le tableau qui va contenir les ECategory
        $arr = array();

        // Request permettant de sélectionner la valeur d'un critère par rapport un ID d'annonce donné
        $sql = 'SELECT `NAME`, CRITERIAS_ID, CRITVAL FROM ads_criterias_values LEFT JOIN criterias ON criterias.ID = ads_criterias_values.CRITERIAS_ID WHERE ADS_ID = :id;';

        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute(array(':id' => $idAd));

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                $valueCriteria = new ECategory($row['CRITERIAS_ID'], $row['CRITVAL']);
                array_push($arr, $valueCriteria);
            } // End while

            // Si l'on a une exception, qu'on ne retrouve pas de lien vers la BDD / autre,
            // on affiche le message d'erreur correspondant et on ne continue pas
        } catch (PDOException  $e) {
            echo "GetValueByCriteriaId Error: " . $e->getMessage();
            return false;
        }
        // Une fois lue toutes les catégories,
        // on demande à chaque objet de charger ses critères
        foreach ($arr as $item) {
            // On charge les critères pour l'objet categorie. Si la fonction retourne -1, c'est qu'on a une erreur.
            // On décide de ne pas continuer et de sortir en erreur. (return false)
            if ($item->LoadAllCriterias() < 0) {
                return false;
            }
        }
        // Ok je retourne le tableau des EUser qu'on aura push au préalable
        return $arr;
    }

    /**
     * Permet de tester si la catégorie est utilisée par une annonce
     * @param [ECategory] $cat
     * @return  
     */
    public static function isCategoryUsed($cat){
        if (isset($cat))
            return ECategoriesHelper::isCategoryIdUsed($cat->id);
        // fail
        return false;
    }

    /**
     * Permet de tester si une catégorie est utilisée par une annonce
     *
     * @param [int] $catId l'id de l'annonce
     * @return  
     */
    public static function isCategoryIdUsed($catId){
        $isUsed = false;
        $sql = 'SELECT count(*) AS total FROM ads where CATEGORIES_ID = :id';

        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);

            // Execution de la requête
            $stmt->execute(array(
                ':id' => $catId
            ));
            $value = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        } catch (PDOException $e) {
            echo "isCategoryIdUsed Error: " . $e->getMessage();
            return false;
        }
        if (intval($value['total']) > 0){
            return true;
        }
        // Pas trouvé
        return false;
    }

    /**
    * Permet d'ajouter une catégorie
    * @param [ECategory] $cat Objet catégorie
    * @return bool true si ok, false si il y a un problème lors de la création.
    */
    public static function addCategory($cat){
        return ECategoriesHelper::addCategoryName($cat->name);
    }

    /**
     * Permet d'ajouter une catégorie
     *
     * @param [string] $name Le nom de la catégorie à ajouter
     * @return bool true si ok, false si il y a un problème lors de la création.
     */
    public static function addCategoryName($name, $id){
        $sql = 'INSERT INTO categories (ID, `NAME`) VALUES(:i, :n)';
        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);
            // Execution de la requête
            $stmt->execute(array(
                                ':n' => $name,
                                ':i' => $id));
        } catch (PDOException  $e) {
            echo "addCat Error: " . $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Permet de reommer une catégorie
     *
     * @param [type] $oldName
     * @param [type] $newName
     * @return true si pas de problème, false si erreure
     */
    public static function renameCategory($oldName, $newName){
        $sql = 'UPDATE categories SET `name` = :v WHERE `name` = :n';
        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);
            // Execution de la requête
            $stmt->execute(array(
                ':n' => $oldName,
                ':v' => $newName
            ));
        } catch (PDOException  $e) {
            echo "renameCategory Error: " . $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Permet de suprimmer une catégorie
     *
     * @param [int] $catId l'id de l'annonce
     * @return bool true si ok, false si il y a un problème lors de la supression.
     */
    public static function deleteCategory($catId){
        if (ECategoriesHelper::isCategoryIdUsed($catId) == false){
            $sql = 'DELETE FROM categories WHERE ID = :catId';
            try {
                // Préparation de la requête
                $stmt = EDatabase::prepare($sql);
                // Execution de la requête
                $stmt->execute(array(
                    ':catId' => $catId
                ));
            } catch (PDOException  $e) {
                echo "deleteCat Error: " . $e->getMessage();
                return false;
            }
            return true;
        }
    }

    /**
     * Permet d'ajouter un critère
     *
     * @param [ECategory] $cat Objet critère
     * @return bool true si ok, false si il y a un problème lors de la création.
     */
    public static function addCriteria($crit){
        return ECategoriesHelper::addCriteriaName($crit->name);
    }

    /**
     * Permet d'ajouter un critère
     *
     * @param [integer] $catId L'identifiant de la catégorie
     * @param [string] $name Le nom du critère à ajouter
     * @return bool L'id du critère créé, autrement false si il y a un problème lors de la création.
     */
    public static function addCriteriaName($catId, $name){
        // On commence le processus de transaction
        EDatabase::beginTransaction();
        $critID = -1;
        $sql = 'INSERT INTO criterias (`NAME`) VALUES(:n)';
        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);
            // Execution de la requête
            $stmt->execute(array(':n' => $name));
            $critID = intval(EDatabase::lastInsertId());
        } catch (PDOException  $e) {
            echo "addCriteriaName Error: " . $e->getMessage();
            // Une erreur, on roll back
            EDatabase::rollBack();
            return false;
        }
        $sql = 'INSERT INTO categories_criterias (`CATEGORIES_ID`, `CRITERIAS_ID`) VALUES(:ci,:id)';
        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);
            // Execution de la requête
            $stmt->execute(array(
                            ':ci' => $catId, 
                            ':id' => $critID));
        } catch (PDOException  $e) {
            echo "addCriteriaName Error: " . $e->getMessage();
            // Une erreur, on roll back
            EDatabase::rollBack();
            return false;
        }
        // Ok, on commit
        EDatabase::commit();
        // On retourne l'id du critère créé
        return $critID;
    }

    /**
     * Permet de reommer un critère
     *
     * @param [integer] $catId L'identifiant de la catégorie
     * @param [string] $oldName
     * @param [string] $newName
     * @return true si bon
     */
    public static function renameCriteria($catId, $oldName, $newName){
        $sql = 'UPDATE criterias SET `name` = :v WHERE `name` = :n AND ID IN(SELECT CRITERIAS_ID FROM categories_criterias where CATEGORIES_ID=:ci)';
        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);
            // Execution de la requête
            $stmt->execute(array(
                ':ci' => $catId,
                ':n' => $oldName,
                ':v' => $newName
            ));
        } catch (PDOException  $e) {
            echo "renameCriteria Error: " . $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Permet de suprimmer une catégorie
     *
     * @param [int] $critId l'id du critère
     * @return bool true si ok, false si il y a un problème lors de la supression.
     */
    public static function deleteCriteria($catId, $critId ,$name){
        EDatabase::beginTransaction();

        $sql = 'DELETE FROM categories_criterias WHERE CRITERIAS_ID = :critId AND CATEGORIES_ID = :catId';
        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);
            // Execution de la requête
            $stmt->execute(array(
                ':catId' => $catId,
                ':critId' => $critId,
            ));
        } catch (PDOException  $e) {
            echo "deleteCriteria Error: " . $e->getMessage();
            EDatabase::rollBack();
            return false;
        }

        $sql = 'DELETE FROM criterias WHERE ID = :critId';
        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);
            // Execution de la requête
            $stmt->execute(array(
                ':critId' => $critId,
            ));
        } catch (PDOException  $e) {
            echo "deleteCriteria Error: " . $e->getMessage();
            EDatabase::rollBack();
            return false;
        }

        EDatabase::commit();
        return true;
    }
    /**
    * Fonction permettant de retourner un NAME pour un ID de critère correspondant
    * @return string
    */
    public static function getCriteriaNameByCode($id){
        $sql = 'SELECT `NAME` FROM criterias WHERE ID = :id';
        try {
            // Préparation de la requête
            $req = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $req->execute([':id' => $id]);
            $criteria = $req->fetch(PDO::FETCH_ASSOC);
            return $criteria['NAME'];
            } catch (PDOException  $e) {
            echo "getCriteriaNameByCode Error: " . $e->getMessage();
            return false;
        }
    }
}