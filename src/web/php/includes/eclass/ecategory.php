<?php

/**
 * La classe ECategory contient les informations sur les catégories
 * d'annonce. Ex: Livre, Calculatrice, ..
 */
class ECategory {

     /**
     * @brief   Class Constructor avec paramètres par défaut pour construire l'objet
     */
    public function __construct($InId = -1, $InName = ""){
        $this->id = $InId;
        $this->name = $InName;
        $this->criterias = array();
    }
    /**
     * Est-ce que l'objet est valide ?
     *
     * @return boolean True si valide, sinon false.
     */
    public function IsValid()
    {
        return ($this->id !== -1);
    }

    /**
     * Charge tous les critères de la catégorie
     *
     * @return int Le nombre de critère chargés pour une catégorie définie. Si erreur, on retourne -1
     * @abstract On fait appel à la fonction 'LoadAllCriterias' afin de faire la relation entre les critères et les catégories,
     * plutôt que de le faire sur la page de fonction, dans ce cas de figure, nous le faisons directement sur l'objet.
     */
    public function LoadAllCriterias()
    {   
        // Si le test est valide, on prend les critères d'une catégorie, on push les critères dans l'array
        // Généralement, pour appeler des fonctions de la classe courante, mettre 'self::' en tant que prefix avant d'appeler une fonction
        if (self::IsValid())
        {
            // Request permettant de prendre l'intégralité des critères qui sont propre à l'id d'une catégorie (ex : ID_CATEGORIE = 1, NAME = Livre | Liste des critères : ISBN Année de parution, Editeur)
            $sql = "SELECT criterias.ID, criterias.NAME FROM categories_criterias, criterias where CATEGORIES_ID=:id and categories_criterias.CRITERIAS_ID = criterias.ID";

            try{
                $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $stmt->execute( array( ':id' => $this->id ) );
                
                while($row=$stmt->fetch(PDO::FETCH_ASSOC,PDO::FETCH_ORI_NEXT)){
                    // Création du critère avec les données provenant de la base de données
                    $c = new ECriteria($row['ID'],$row['NAME']);
                    array_push($this->criterias,$c);
                } #end while
            
            // Si l'on a une exception, qu'on ne retrouve pas de lien vers la BDD / autre,
            // on affiche le message d'erreur correspondant et on on retourne -1 pour l'id afin de dire que la fonction n'est pas valide (Qu'elle n'a pas trouvé les critères associés à la catégorie sélectionné)
            }catch(PDOException  $e ){
                echo "LoadAllCriterias Error: ".$e->getMessage();
                return -1;
            }            
        }
        // On retourne combien de critères comprend la catégorie sélectionné pour l'ID dans l'objet ECategory
        return count($this->criterias);
    }
    /** @var [int] L'identifiant unique de la catégorie */
    public $id;

    /** @var [string] Le nom de la catégorie */
    public $name;

    /** @var [array of ECriteria] Le tableau contenant les critères associés à cette catégorie */
    public $criterias;
}
?>