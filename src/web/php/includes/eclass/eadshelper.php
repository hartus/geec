<?php

/**
 * La classe EAdsHelper contient des fonctions statiques 
 * pour gérer les annonces
 */
class EAdsHelper
{

    /**
     * Ajoute une annonce dans la base
     *
     * @param EAds $ads
     * @return boolean True ajouté, False erreur
     */
    public static function addAds($ads)
    {
        // On commence le processus de transaction
        EDatabase::beginTransaction();
        /** On met 1 pour value sur 'ROLES_CODE' car les utilisateurs authentifiés sont des élèves. */
        $sql = 'INSERT INTO ads (USERS_EDU_MAIL, `NAME`, PRICE, `DESCRIPTION`, PUBLICATIONDATE, SALEDATE, RAWIMAGE, STATES_CODE, CATEGORIES_ID,PRIVATE_MAIL_SHOW,PHONE_NUMBER_SHOW, ACTIVATIONDATE) VALUES(:mail, :name, :price, :description, :publicationdate, :saledate, :rawimage, :states_code, :categories_id, :showMail, :showPhone, :activationDate)';
        
        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);

            // Réservation des champs de l'objet EAds pour les attributs en BDD
            $datetoday = date("Y-m-d H:i:s");
            $p = ReadPreferences();
            $nbForActication = $p->DayForActivation;
            $dateActivation = date("Y-m-d H:i:s", strtotime('+'.$nbForActication.'day'));
            $shem = ($ads->showPrivateEmail == true) ? 1 : 0;
            $shph = ($ads->showPrivatePhone == true) ? 1 : 0;
            // Execution de la requête
            $stmt->execute(array(
                ':mail' => $ads->Email,
                ':name' => $ads->Name,
                ':price' => $ads->Price,
                ':description' => $ads->Description,
                ':publicationdate' => $datetoday,
                ':saledate' => null,
                ':rawimage' => $ads->rawImage,
                ':categories_id' => $ads->Categorie,
                ':states_code' => $ads->State,
                ':showMail' => $shem,
                ':showPhone' => $shph,
                ':activationDate'=> $dateActivation
            ));
        } catch (PDOException  $e) {
            echo "EAdsHelper::addAds Error: " . $e->getMessage();
            // Une erreur, on roll back
            EDatabase::rollBack();
            return false;
        }
        $adID = intval(EDatabase::lastInsertId());

        // On va insérer les valeurs des critères dans le tableau associatif : ads_criterias_values
        if (count($ads->criteriasValues) > 0) {
            $sql = 'INSERT INTO ads_criterias_values (ADS_ID, CRITERIAS_ID, CRITVAL) VALUES(:idAd, :code, :val)';
            try {
                // Préparation de la requête
                $stmt = EDatabase::prepare($sql);
            } catch (PDOException  $e) {
                echo "EAdsHelper::addAds Error: " . $e->getMessage();
                // Une erreur, on roll back
                EDatabase::rollBack();
                return false;
            }
            foreach ($ads->criteriasValues as $cv) {
                try {
                    // Execution de la requête
                    $stmt->execute(array(
                        ':idAd' => $adID,
                        ':code' => $cv->id,
                        ':val' => utf8_encode($cv->value)
                    ));
                } catch (PDOException  $e) {
                    echo "EAdsHelper::addAds Error: " . $e->getMessage();
                    // Une erreur, on roll back
                    EDatabase::rollBack();
                    return false;
                }
            }
        }

        // On va insérer les default des réseaux sociaux dans le tableau associatif : ads_socialmedias_default
        if (count($ads->socialMediaDefaults) > 0) {
            $sql = 'INSERT INTO ads_socialmedias_default (ADS_ID, SOCIALMEDIAS_CODE, `DEFAULT`) VALUES(:idAd, :code, :default)';
            try {
                // Préparation de la requête
                $stmt = EDatabase::prepare($sql);
            } catch (PDOException  $e) {
                echo "EAdsHelper::addAds Error: " . $e->getMessage();
                // Une erreur, on roll back
                EDatabase::rollBack();
                // Fail
                return false;
            }
            foreach ($ads->socialMediaDefaults as $smd) {
                try {
                    // Execution de la requête
                    $stmt->execute(array(
                        ':idAd' => $adID,
                        ':code' => $smd->code,
                        ':default' => $smd->default
                    ));
                } catch (PDOException  $e) {
                    echo "EAdsHelper::addAds Error: " . $e->getMessage();
                    // Une erreur, on roll back
                    EDatabase::rollBack();
                    return false;
                }
            }
        }
        // Ok, on commit
        EDatabase::commit();
        // @remark Ne sert à rien d'assigner à l'objet,
        // il n'est pas retourné.
        //$ads->Id = $adID;

        return true;
    }

    /**
     * Récupère une annonce stocké en base de donnée par rapport à son ID
     *
     * @param integer $id L'identifiant unique de l'annonce
     * @param integer $statut $statut Si l'on veut toutes les annonces ADS_STATUS_ALL.
     *                        Si l'on veut toutes les annonces désactivées ADS_STATUS_DEACTIVATED
     *                        et ADS_STATUS_ACTIVATED pour toutes les annonces activées
     * @return [EAds] L'annonce EAds ou FALSE si pas trouvé
     */
    public static function getAdById($id, $statut = ADS_STATUS_ALL)
    {
        $arr = EAdsHelper::getAds($id, -1, $statut);
        if ($arr !== false && count($arr) > 0) {
            return $arr[0];
        }
        // Fail
        return false;
    }

    /**
     * Retourne les anonnces en fonction de paramètres
     *
     * @param integer $id L'identifiant unique de l'annonce
     * @param integer $max la limite de caractères pour les commentaires
     * @param integer $statut $statut Si l'on veut toutes les annonces ADS_STATUS_ALL.
     *                        Si l'on veut toutes les annonces désactivées ADS_STATUS_DEACTIVATED
     *                        et ADS_STATUS_ACTIVATED pour toutes les annonces activées
     * @return [arr] un tableau de EAds
     */
    public static function getAds($id, $max, $statut)
    {
        $arr = array();
        $sql = 'SELECT ID, USERS_EDU_MAIL, `NAME`, PRICE, `DESCRIPTION`, PUBLICATIONDATE, CATEGORIES_ID, STATES_CODE, PUBLICATIONDATE, SALEDATE, RAWIMAGE, PRIVATE_MAIL_SHOW, PHONE_NUMBER_SHOW, ACTIVED, ACTIVATIONDATE FROM ads';
        // Le tableau des critères ajoutés dynamiquement en fonction des critères de recherche
        $critarr = array();
        $whereExist = false;

        if ($id > 0) {
            $whereExist = true;
            $sql .= " WHERE ID = :id";
            $critarr = array(':id' => $id);
        }
        if ($whereExist == true && $statut == 0 || $whereExist == true && $statut == 1) {
            $sql .= " AND ACTIVED = :actived";
            $critarr[':actived'] = $statut;
        } else {
            if (($statut == 0 || $statut == 1)) {
                $sql .= " WHERE ACTIVED = :actived";
                $critarr[':actived'] = $statut;
            }
        }
        $sql .= " ORDER BY PUBLICATIONDATE DESC";

        if ($max > 0) {
            $sql .= " LIMIT " . $max;
        }
        // Je passe le sql construit ainsi que le tableau associatif des paramètres contenu dans la requête
        return EAdsHelper::getAdsBySQL($sql, $critarr);
    }

    /**
     * Affichage des annonces contenant le text recherché
     *
     * @param ESearchCriterias $sc La structure contenant les paramètres pour la rechercher
     * @return [arr] un tableau de EAds
     */
    public static function getAdsByCriterias($sc)
    {
        // Le tableau des critères ajoutés dynamiquement en fonction des critères de recherche
        $critarr = array();

        $sql = 'SELECT DISTINCT ID, USERS_EDU_MAIL, `NAME`, PRICE, `DESCRIPTION`, PUBLICATIONDATE, SALEDATE, CATEGORIES_ID, STATES_CODE, PUBLICATIONDATE, SALEDATE, RAWIMAGE, PRIVATE_MAIL_SHOW, PHONE_NUMBER_SHOW, ACTIVED, ACTIVATIONDATE FROM ads LEFT JOIN ads_criterias_values ON ads.ID = ads_criterias_values.ADS_ID';
        // Critères sql construit dynamiquement
        $crit = "";
        
        // Est-ce qu'on a un texte à rechercher ?
        if (strlen($sc->Text2Find) > 0) {
            $crit = " (`NAME` LIKE :t OR `DESCRIPTION` LIKE :t)";
            $critarr[':t'] = "%" . $sc->Text2Find . "%";
        }

        // Est-ce qu'on a une catégorie?
        if ($sc->Category > 0) {
            if (strlen($crit) > 0) {
                $crit .= " AND ";
            }
            $crit .= " (CATEGORIES_ID = :c)";
            $critarr[':c'] = $sc->Category;
        }

        // Est-ce qu'on a un state?
        if ($sc->State > 0) {
            if (strlen($crit) > 0) {
                $crit .= " AND ";
            }
            $crit .= " (STATES_CODE = :s)";
            $critarr[':s'] = $sc->State;
        }

        // Est-ce qu'on doit filtrer par le prix?
        // Une rangée de prix
        if ($sc->PriceMin > 0 && $sc->PriceMax > 0) {
            if (strlen($crit) > 0) {
                $crit .= " AND ";
            }
            $crit .= " (PRICE BETWEEN :pMin AND :pMax)";
            $critarr[':pMin'] = $sc->PriceMin;
            $critarr[':pMax'] = $sc->PriceMax;
        } elseif // Le prix min
            ($sc->PriceMin > 0) {
            if (strlen($crit) > 0) {
                $crit .= " AND ";
            }
            $crit .= " (PRICE >= :pMin)";
            $critarr[':pMin'] = $sc->PriceMin;
        } elseif // Le prix max
            ($sc->PriceMax > 0) {
            if (strlen($crit) > 0) {
                $crit .= " AND ";
            }
            $crit .= " (PRICE <= :pMax)";
            $critarr[':pMax'] = $sc->PriceMax;
        }
        // Est-ce que j'ai des critères ?
        if (!empty($sc->criteriasValues)) {
            if (strlen($crit) > 0) {
                $crit .= ' t2.CRITERIAS_ID IN (';
                foreach ($sc->criteriasValues as $cv) {
                    $crit .= $cv->value . ",";
                }
                $crit = substr($crit, 0, -1);
                $crit .= ')';
            }
        }

        if (strlen($crit) > 0) {
            $sql .= " WHERE " . $crit . "AND ACTIVED = 1";
        }
        // Est-ce qu'on a des critères dynamique
        foreach ($sc->criteriasValues as $cv) {
            $id = $cv->id;
            $value = $cv->value;
        }
        // J'ajoute le tri à la fin
        $sql .= ' ORDER BY PUBLICATIONDATE DESC';
        // Je passe le sql construit ainsi que le tableau associatif des paramètres contenu dans la requête
        return EAdsHelper::getAdsBySQL($sql, $critarr);
    }

    /**
     * Toutes les annonces de la base
     *
     * @param integer $max la limite de caractères pour les commentaires
     * @param integer $statut Si l'on veut toutes les annonces ADS_STATUS_ALL.
     *                        Si l'on veut toutes les annonces désactivées ADS_STATUS_DEACTIVATED
     *                        et ADS_STATUS_ACTIVATED pour toutes les annonces activées
     * @return [arr] un tableau de EAds
     *
     */
    public static function getAllAds($max = -1, $statut = ADS_STATUS_ALL)
    {
        return EAdsHelper::getAds(-1, $max, $statut);
    }

    /**
     * Toutes les annonces de la base pour un email donné
     *
     * @param string $email l'email du propriétaire des annonces
     * @param integer $max la limite de caractères pour les commentaires
     * @param integer $statut Si l'on veut toutes les annonces ADS_STATUS_ALL.
     *                        Si l'on veut toutes les annonces désactivées ADS_STATUS_DEACTIVATED
     *                        et ADS_STATUS_ACTIVATED pour toutes les annonces activées
     * @return [arr] un tableau de EAds
     *
     */
    public static function getAdsByEmail($mail, $max = -1, $statut = ADS_STATUS_ALL)
    {
        $arr = array();
        $sql = 'SELECT ID, USERS_EDU_MAIL, `NAME`, PRICE, `DESCRIPTION`, PUBLICATIONDATE, SALEDATE, CATEGORIES_ID, STATES_CODE, PUBLICATIONDATE, SALEDATE, RAWIMAGE, PRIVATE_MAIL_SHOW, PHONE_NUMBER_SHOW, ACTIVED, ACTIVATIONDATE FROM ads';
        // Le tableau des critères ajoutés dynamiquement en fonction des critères de recherche
        $critarr = array();
        if ($mail != "") {
            $sql .= " WHERE USERS_EDU_MAIL = :mail";
            $critarr = array( ':mail' => $mail);
        }
        switch ($statut) {
            case ADS_STATUS_ACTIVATED:
            case ADS_STATUS_DEACTIVATED:
                $sql .= " AND ACTIVED = :actived";
                $critarr[':actived'] = $statut;
                break;
            default:
            break;
        }
        $sql .= " ORDER BY PUBLICATIONDATE DESC";
        if ($max > 0) {
            $sql .= " LIMIT ".$max;
        }
        // Je passe le sql construit ainsi que le tableau associatif des paramètres contenu dans la requête
        return EAdsHelper::getAdsBySQL($sql, $critarr);
    }

    /**
     * Mets à jour une annonce dans la base
     *
     * @param EAds $ads
     * @return boolean True ok
     */
    public static function updateAds($ads)
    {
        // On commence le processus de transaction
        EDatabase::beginTransaction();

        /** On met 1 pour value sur 'ROLES_CODE' car les utilisateurs authentifiés sont des élèves. */
        $sql = 'UPDATE ads SET `NAME`=:name, PRICE=:price, `DESCRIPTION`=:description, RAWIMAGE=:rawimage, STATES_CODE=:states_code, CATEGORIES_ID=:categories_id, PRIVATE_MAIL_SHOW=:showMail,PHONE_NUMBER_SHOW=:showPhone, ACTIVATIONDATE=:activationDate WHERE ID=:id';

        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);

            // Réservation des champs de l'objet EAds pour les attributs en BDD
            $date = date("Y-m-d H:i:s");
            $p = ReadPreferences();
            $nbForActication = $p->DayForActivation;
            $dateActivation = date("Y-m-d H:i:s", strtotime('+'.$nbForActication.'day'));
            
            $shem = ($ads->showPrivateEmail == true) ? 1 : 0;
            $shph = ($ads->showPrivatePhone == true) ? 1 : 0;
            // Execution de la requête
            $stmt->execute(array(
                ':id' => $ads->Id,
                ':name' => $ads->Name,
                ':price' => $ads->Price,
                ':description' => $ads->Description,
                ':rawimage' => $ads->rawImage,
                ':categories_id' => $ads->Categorie,
                ':states_code' => $ads->State,
                ':showMail' => $shem,
                ':showPhone' => $shph,
                ':activationDate' =>$dateActivation
            ));
        } catch (PDOException  $e) {
            echo "EAdsHelper::updateAds Error: " . $e->getMessage();
            // Une erreur, on roll back
            EDatabase::rollBack();
            return false;
        }
        // On va insérer les valeurs des critères dans le tableau associatif : ads_criterias_values
        $sql = 'DELETE FROM ads_criterias_values WHERE ADS_ID = :idAd';
        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);
            // Execution de la requête
            $stmt->execute(array(':idAd' => $ads->Id));
        } catch (PDOException  $e) {
            echo "EAdsHelper::updateAds Error: " . $e->getMessage();
            // Une erreur, on roll back
            EDatabase::rollBack();
            return false;
        }

        if (count($ads->criteriasValues) > 0) {
            $sql = 'INSERT INTO ads_criterias_values (ADS_ID, CRITERIAS_ID, CRITVAL) VALUES(:idAd, :code, :val)';
            try {
                // Préparation de la requête
                $stmt = EDatabase::prepare($sql);
            } catch (PDOException  $e) {
                echo "EAdsHelper::updateAds Error: " . $e->getMessage();
                // Une erreur, on roll back
                EDatabase::rollBack();
                return false;
            }
            foreach ($ads->criteriasValues as $cv) {
                try {
                    // Execution de la requête
                    $stmt->execute(array(
                        ':idAd' => $ads->Id,
                        ':code' => $cv->id,
                        ':val' => utf8_encode($cv->value)
                    ));
                } catch (PDOException  $e) {
                    echo "EAdsHelper::updateAds Error: " . $e->getMessage();
                    // Une erreur, on roll back
                    EDatabase::rollBack();
                    return false;
                }
            }
        }

        // On va insérer les default des réseaux sociaux dans le tableau associatif : ads_socialmedias_default

        $sql = 'DELETE FROM ads_socialmedias_default WHERE ADS_ID = :idAd';

        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);
            // Execution de la requête
            $stmt->execute(array(':idAd' => $ads->Id));
        } catch (PDOException  $e) {
            echo "EAdsHelper::updateAds Error: " . $e->getMessage();
            // Une erreur, on roll back
            EDatabase::rollBack();
            return false;
        }

        if (count($ads->socialMediaDefaults) > 0) {
            $sql = 'INSERT INTO ads_socialmedias_default (ADS_ID, SOCIALMEDIAS_CODE, `DEFAULT`) VALUES(:idAd, :code, :default)';
            try {
                // Préparation de la requête
                $stmt = EDatabase::prepare($sql);
            } catch (PDOException  $e) {
                echo "EAdsHelper::updateAds Error: " . $e->getMessage();
                // Une erreur, on roll back
                EDatabase::rollBack();
                // Fail
                return false;
            }
            foreach ($ads->socialMediaDefaults as $smd) {
                try {
                    // Execution de la requête
                    $stmt->execute(array(
                        ':idAd' => $ads->Id,
                        ':code' => $smd->code,
                        ':default' => $smd->default
                    ));
                } catch (PDOException  $e) {
                    echo "EAdsHelper::updateAds Error: " . $e->getMessage();
                    // Une erreur, on roll back
                    EDatabase::rollBack();
                    return false;
                }
            }
        }
        // Ok, on commit
        EDatabase::commit();
        return true;
    }

    /**
     * Affichage des annonces contenant le text recherché
     *
     * @param ESearchCriterias $sc La structure contenant les paramètres pour la rechercher
     * @param integer $max le nombre maximum d'enregistrement à retourner. Default est -1 pour tous.
     * @return array un tableau de EAds uniquement avec les annonces contenant le text recherché
     */
    public static function getAllAdsSearchByCriterias($sc, $max = -1)
    {
        $arr = array();
        $sql = 'SELECT ID, USERS_EDU_MAIL, `NAME`, PRICE, `DESCRIPTION`, PUBLICATIONDATE, SALEDATE, CATEGORIES_ID, STATES_CODE, PUBLICATIONDATE, RAWIMAGE, ACTIVED, ACTIVATIONDATE FROM ads WHERE (`NAME` LIKE :t OR `DESCRIPTION` LIKE :t) AND (STATES_CODE = :s) AND (PRICE BETWEEN :pMin AND :pMax)  ORDER BY PUBLICATIONDATE DESC';
        
        if ($max > 0) {
            $sql .= " LIMIT " . $max;
        }

        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute(array(
                ':t' => "%" . $sc->Text2Find . "%",
                ':c' => $sc->Category,
                ':s' => $sc->State,
                ':pMin' => $sc->PriceMin,
                ':pMax' => $sc->PriceMax
            ));

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                // Création des ads avec les données provenant de la base de données
                $a = new EAds(intVal($row['ID']), $row['USERS_EDU_MAIL'], $row['NAME'], intVal($row['PRICE']), $row['DESCRIPTION'], $row['PUBLICATIONDATE'], intVal($row['STATES_CODE']), intVal($row['CATEGORIES_ID']), $row['RAWIMAGE'], $row['SALEDATE'], $row['ACTIVED'], $row['ACTIVATIONDATE']);

                $a->categorie = ECategoriesHelper::GetCategorieForAds($row['ID']);
                // Si on a une erreur, alors on remet à null
                if ($a->categorie === false) {
                    return false;
                }

                $a->state = GetStateForAds($row['ID']);
                // Si on a une erreur, alors on remet à null
                if ($a->state === false) {
                    return false;
                }

                $a->showPrivateEmail = EUserHelper::GetUserByEmail($a->Email)->privatemaildefault;
                // Si on a une erreur, alors on remet à null
                if ($a->showPrivateEmail === false) {
                    return false;
                }

                $a->showPrivatePhone = EUserHelper::GetUserByEmail($a->Email)->privatephonedefault;
                // Si on a une erreur, alors on remet à null
                if ($a->showPrivatePhone === false) {
                    return false;
                }

                $a->rawImage = $row['RAWIMAGE'];

                // On retourne l'objet ads
                array_push($arr, $a);
            } #end while
        } catch (PDOException $e) {
            echo "EAdsHelper::getAllAds Error: " . $e->getMessage();
            return false;
        }
        // Parcourir toutes les annonces, on va lire les valeurs des critères pour l'annonce
        foreach ($arr as $ad) {
            $sql = 'SELECT CRITERIAS_ID, CRITVAL FROM ads_criterias_values WHERE ADS_ID = :id';
            try {
                // Préparation de la requête
                $req = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            } catch (PDOException  $e) {
                echo "EAdsHelper::getAllAds Error: " . $e->getMessage();
                return false;
            }
        }
        return $arr;
    }

    /**
     * Retourne les anonnces en fonction d'une chaîne sql et ses paramètres associés
     *
     * @param integer $sql La requête sql à exécuter pour récupérer les annonces
     * @param [arr] $sqlcritarr Les paramètres associés à la requête
     * @return [arr] un tableau de EAds
     */
    public static function getAdsBySQL($sql, $sqlcritarr)
    {
        $arr = array();
        try {
            $stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            if (count($sqlcritarr) > 0) {
                $stmt->execute($sqlcritarr);
            } else {
                $stmt->execute();
            }
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                // Création des ads avec les données provenant de la base de données
                $a = new EAds(intVal($row['ID']), $row['USERS_EDU_MAIL'], $row['NAME'], intVal($row['PRICE']), $row['DESCRIPTION'], $row['PUBLICATIONDATE'], intVal($row['STATES_CODE']), intVal($row['CATEGORIES_ID']), $row['RAWIMAGE']);
                $a->actived =  intVal($row['ACTIVED']);
                $a->saleDate = $row['SALEDATE'];
                $a->activationDate = $row['ACTIVATIONDATE'];
                $a->categorie = intval($row['CATEGORIES_ID']);
                $a->state = intval($row['STATES_CODE']);
                /* @todo lire l'info dans la base */
                $a->showPrivateEmail = intval($row['PRIVATE_MAIL_SHOW']);
                /* @todo lire l'info dans la base */
                $a->showPrivatePhone = intval($row['PHONE_NUMBER_SHOW']);
                $a->rawImage = $row['RAWIMAGE'];
                // On retourne l'objet ads
                array_push($arr, $a);
            } #end while
        } catch (PDOException $e) {
            echo "EAdsHelper::getAdsBySQL Error: " . $e->getMessage();
            return false;
        }
        // Parcourir toutes les annonces, on va lire les valeurs des critères pour l'annonce
        foreach ($arr as $ads) {
            $sql = 'SELECT CRITERIAS_ID, CRITVAL FROM ads_criterias_values WHERE ADS_ID = :id';
            try {
                // Préparation de la requête
                $req = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            } catch (PDOException  $e) {
                echo "EAdsHelper::getAdsBySQL Error: " . $e->getMessage();
                return false;
            }
            try {
                // Execution de la requête
                $req->execute(array(':id' => $ads->Id));
                while ($row = $req->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                    // Création des objets 'ECriteriaValue' avec les données provenant de la base de données
                    $cv = new ECriteriaValue(intval($row['CRITERIAS_ID']), $row['CRITVAL']);
                    // In ajoute l'élément dans le tableau
                    array_push($ads->criteriasValues, $cv);
                } // End while
            } catch (PDOException  $e) {
                echo "EAdsHelper::getAdsBySQL Error: " . $e->getMessage();
                return false;
            }
        }
        
        // Parcourir toutes les annonces, on va lire les valeurs des défauts pour les médias sociaux pour l'annonce
        foreach ($arr as $ads) {
            $sql = 'SELECT SOCIALMEDIAS_CODE, `DEFAULT` FROM ads_socialmedias_default WHERE ADS_ID = :id';
            try {
                // Préparation de la requête
                $req = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            } catch (PDOException  $e) {
                echo "EAdsHelper::getAdsBySQL Error: " . $e->getMessage();
                return false;
            }
            try {
                // Execution de la requête
                $req->execute(array(':id' => $ads->Id));
                while ($row = $req->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                    // Création des objets 'EAdsRSValue' avec les données provenant de la base de données
                    $smv = new EAdsRSValue($ads->Id, intval($row['SOCIALMEDIAS_CODE']), intval($row['DEFAULT']));
                    // In ajoute l'élément dans le tableau
                    array_push($ads->socialmediasValues, $smv);
                } // End while
            } catch (PDOException  $e) {
                echo "EAdsHelper::getAdsBySQL Error: " . $e->getMessage();
                return false;
            }
        }
        return $arr;
    }

    /**
     * Fonction permettent de vendre une annonce
     *
     * @param [int] $id (L'id de l'annonce)
     * @param [int] $statut $statut Si l'on veut  désactivée l'annonce : ADS_STATUS_DEACTIVATED
         *                          et si l'on veut activée l'annonce :ADS_STATUS_ACTIVATED
     */
    public static function changedActivated($id, $statut)
    {
        // On commence le processus de transaction
        EDatabase::beginTransaction();
        if ($statut == 0 || $statut == 1) {
            $sql = 'UPDATE ads SET ACTIVED=:actived WHERE ID=:id';
            try {
                // Préparation de la requête
                $stmt = EDatabase::prepare($sql);
                // Execution de la requête
                //On met la valeur d'actived à $statut (0 ou 1) pour désactiver ou activer l'annonce.
                $stmt->execute(array(
                ':id' => $id,
                ':actived' => $statut
            ));
            } catch (PDOException  $e) {
                echo "EAdsHelper::changedActivated Error: " . $e->getMessage();
                // Une erreur, on roll back
                EDatabase::rollBack();
                return false;
            }
            EDatabase::commit();
            return true;
        }
    }

    /**
     * Met a jour le champs de la date de vente pour une annonce donnée
     *
     * @param [integer] $id
     * @param [dateTime] $saleDate
     * @return true si tout est bon
     */
    public static function updateSaleDate($id, $saleDate)
    {
        // On commence le processus de transaction
        $sql = 'UPDATE ads SET SALEDATE=:saledate, ACTIVED=0 WHERE ID=:id';
        try {
            // Préparation de la requête
            $stmt = EDatabase::prepare($sql);
            // Réservation des champs de l'objet EAds pour les attributs en BDD
            $saleDate = date("Y-m-d H:i:s");
            // Execution de la requête
            //On met la valeur d'actived à $statut (0 ou 1) pour désactiver ou activer l'annonce.
            $stmt->execute(array(
            ':id' => $id,
            ':saledate' => $saleDate
        ));
        } catch (PDOException  $e) {
            echo "EAdsHelper::addAds Error: " . $e->getMessage();
            // Une erreur, on roll back
            return false;
        }
        return true;
    }

    /**
      * Fonction permettant de récupérer les informations complémentaire au profil d'un utilisateur selon le propriétaire d'une annonce créé
      *
      * @return void
      */
    public static function getUserInfos()
    {
        // Le tableau qui va contenir les informations des utilisateurs pour la page d'accueil (index.php)
        $arr = array();

        $query = 'SELECT EDU_MAIL, EDU_NAME, URL_PROFIL_PIC FROM users, ads WHERE EDU_MAIL = USERS_EDU_MAIL GROUP BY EDU_MAIL;';

        try {
            $stmt = EDatabase::prepare($query, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                array_push($arr, $row);
            }
               
            // Si l'on a une exception, qu'on ne retrouve pas de lien vers la BDD / autre,
            // on affiche le message d'erreur correspondant et on on retourne -1 pour l'id afin de dire que la fonction n'est pas valide
        } catch (PDOException  $e) {
            echo "getUserInfos Error: " . $e->getMessage();
            return false;
        }
        // Done
        return $arr;
    }
    /**
     * Check la date des annonces non activées, si la date est plus ancienne que la date du server, passe l'annonce a activée et enlève la date d'activation, sinon ne fait rien
     *
     * @return true si la mise a jour est ok
     */
    public static function CheckAdsActivations()
    {
        $sql = 'UPDATE ads SET ACTIVATIONDATE=NULL, ACTIVED=1 WHERE ACTIVATIONDATE < NOW()';
        try {
            // Préparation de la requête
            $req = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $req->execute();
            return true;
        } catch (PDOException  $e) {
            echo "EAdsHelper::CheckAdsActivations Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * Check the social media's value array of the ads against the social media defined within the user's profile
     *
     * @param array $AdSocialMediaValues Array of EAdsRSValue for the Ads
     * @param array $userSocialMedias Array of EAdsRSValue for the Ads
     * @return array The final array of EAdsRSValue for the Ads
     */
    public static function checkSocialMediaADS($AdSocialMediaValues, $userSocialMedias)
    {
        $socialMedias = array();
        // Now we have to be sure that social media values contained within the Ads
        // are still defined within the user's profile
        foreach($AdSocialMediaValues as $sc){
            $obj = EAdsHelper::findSocialMediaByCode($sc->code, $userSocialMedias);
            // If found, we can add it to the final array
            if ($obj !== false)
                array_push($socialMedias, $sc);
        }

        // Check if the all social media are present within the ads
        foreach($userSocialMedias as $sc){
            $obj = EAdsHelper::findSocialMediaByCode($sc->code, $socialMedias);
            // If not found, add the social media of the user's profile
            if ($obj === false)
                array_push($socialMedias, $sc);
        }
        // Done
        return $socialMedias;
    }
    
    /**
     * Find the social media code within the array of EAdsRSValue
     *
     * @param int $code The social media code to look at
     * @param array $arr The array of EAdsRSValue
     * @return mixed|bool The EAdsRSValue object if exists, False if not.
     */
    private static function findSocialMediaByCode($code, $arr){
        // Check if the social media is present within the array of EAdsRSValue
        foreach($arr as $sc){
            if ($sc->code === $code)
                return $sc;
        }
        // Not found
        return false;
    }
}