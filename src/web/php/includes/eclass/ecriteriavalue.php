<?php

/**
 * La classe ECriteria contient les informations sur un critère.
 * Le critère appartient à une catégorie
 * @see ECategory
 */
class ECriteriaValue {

     /**
     * @brief   Class Constructor avec paramètres par défaut pour construire l'objet
     */
    public function __construct($InId = -1, $InValue = ""){
        $this->id = $InId;
        $this->value = $InValue;
    }
    /**
     * Est-ce que l'objet est valide ?
     *
     * @return boolean True si valide, sinon false.
     */
    public function IsValid()
    {
        return ($this->id !== -1);
    }
    /** @var [int] L'identifiant unique du critère */
    public $id;

    /** @var [string] Le valeur du critère */
    public $value;
}