<?php
/**
 * @brief	classe ESession 
 * @author 	Cuhbert Sébastien, Bytyçi Qendrim
 * @remark	
 */
class ESession {
	/**
	 * @brief	Class Constructor - Create a new database connection if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new KDatabase();'
	 */
	private function __construct() {}
	/**
	 * @brief	Like the constructor, we make __clone private so nobody can clone the instance
	 */
    private function __clone() {}
        
	/**
	 * @brief	Si l'utilisateur s'est connecté, retourne en session son email
	 * @return false si pas connecté
	 */
	public static function getEmail() {
        if (isset($_SESSION['useremail']))
            return $_SESSION['useremail'];
		return false;
	} # end method


	/**
	 * @brief	set l'email dans la session
	 * @return string l'email de l'utilisateur
	 */
	public static function setEmail($email) {
        $_SESSION['useremail'] = $email;
	} # end method

	/**
	 * @brief	Si l'utilisateur s'est connecté, retourne en session son image de profil
	 * @return false si pas connecté
	 */
	public static function getUrlImage() {
        if (isset($_SESSION['userurlimage']))
            return $_SESSION['userurlimage'];
		return false;
	} # end method

	/**
	 * @brief 	set l'image de profil de la session
	 * @return string l'url de l'image de profil de l'utilisateur
	 */
	public static function setUrlImage($image) {
        $_SESSION['userurlimage'] = $image;
	} # end method


	/**
	 * @brief Detruit la session
	 *
	 * @remark Attention, ne déconnecte pas de EEL
	 */
	public static function Destroy(){
		// Unset all of the session variables.
		$_SESSION = array();

		// If it's desired to kill the session, also delete the session cookie.
		// Note: This will destroy the session, and not just the session data!
		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);
		}

		// Finally, destroy the session.
		session_destroy();		
	}
}


