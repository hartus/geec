<?php

/**
 * La classe EState contient les informations sur un état
 * @remark Les variables membres sont en public car on veut 
 *         utiliser l'objet uniquement comme container et non
 *         comme objet à part entière.
 */
class EState{

     /**
     * @brief   Class Constructor avec paramètres par défaut pour construire l'objet
     */
    public function __construct($InCode = -1, $InName = ""){
        $this->code = $InCode;
        $this->name = $InName;
    }
    /**
     * Est-ce que l'objet est valide ?
     *
     * @return boolean True si valide, sinon false.
     */
    public function IsValid()
    {
        return ($this->code !== -1);
        // Nous avons -1 sur le 'code' si notre objet 'EState'
        // ne trouve pas de 'code' à mettre.
        
        // S'il ne trouve pas de code par l'intérmediaire d'une request,
        // le constructeur va se charger de mettre -1 pour le 'code' et chaine vide pour le 'name' de l'état par défaut
    }
    /** @var [integer] Le code de l'état */
    public $code;

    /** @var [string] Le nom de l'état' */
    public $name;
}
?>