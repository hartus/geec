<?php

/**
 * La classe ESocialmedia contient les informations sur les différents réseaux sociaux
 * d'annonce. Ex: Facebook, Twitter, etc.
 */
class ESocialmedia{

     /**
     * @brief   Class Constructor avec paramètres par défaut pour construire l'objet
     */
    public function __construct($InCode = -1, $InName = ""){
        $this->code = $InCode;
        $this->name = $InName;
    }
    /**
     * Est-ce que l'objet est valide ?
     *
     * @return boolean True si valide, sinon false.
     */
    public function IsValid()
    {
        return ($this->code !== -1);
    }

    /** @var [int] Code unique du réseau social*/
    public $code;

    /** @var [string] Le nom du réseau social */
    public $name;

}

?>