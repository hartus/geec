<?php

/**
 * La classe EAdsRSValue contient l'id de l'annonce, le code du réseau social et le default
 * d'annonce. Ex : ADS_ID : 1, SOCIALMEDIAS_CODE : 1 (Facebook), DEFAULT : 1 (True)
 * Cette exemple veut dire que pour l'annonce numéro 1 ayant pour réseau social Facebook, nous voulons afficher le réseau social Facebook pour l'utilisateur publiant l'annonce
 */
class EAdsRSValue{

     /**
     * @brief   Class Constructor avec paramètres par défaut pour construire l'objet
     */
    public function __construct($InIdAd = -1, $InCode = -1, $inDefault = 0){
        
        $this->idAd = $InIdAd;
        $this->code = $InCode;
        $this->default = $inDefault;
    }
    /**
     * Est-ce que l'objet est valide ?
     *
     * @return boolean True si valide, sinon false.
     */
    public function IsValid()
    {
        return ($this->idAd !== -1);
    }

    /** @var [int] Id de l'annonce */
    public $idAd;

    /** @var [int] Code unique du réseau social*/
    public $code;

    /** @var [tinyint] Le default du réseau social*/
    public $default;

}

?>