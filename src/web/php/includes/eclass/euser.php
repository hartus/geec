<?php

/**
 * La classe EUser contient les informations sur un utilisateur
 * @remark Les variables membres sont en public car on veut 
 *         utiliser l'objet uniquement comme container et non
 *         comme objet à part entière.
 */
class EUser{

     /**
     * @brief   Class Constructor avec paramètres par défaut pour construire l'objet
     */
    public function __construct($inEmail = "", $inName = "", $inUrl = "", $inPrivatemail = "", $inPrivatephone = "", $inPrivatemailDefault = 0, $inPrivatephoneDefault = 0, $InRolesCode = 0, $inBann = 0){
           
        $this->email = $inEmail;
        $this->name = $inName;
        $this->imageurl = $inUrl;
        $this->socialmedias = NULL;
        $this->privatemail = $inPrivatemail;
        $this->privatephone = $inPrivatephone;
        $this->privatemaildefault = (int)$inPrivatemailDefault;
        $this->privatephonedefault = (int)$inPrivatephoneDefault;
        $this->rolescode = (int)$InRolesCode;

        $this->bann = (int)$inBann;        
    }
    /**
     * Est-ce que l'objet est valide ?
     *
     * @return boolean True si valide, sinon false.
     */
    public function IsValid()
    {
        return (strlen($this->email) > 0) ? true : false;
    }

 
    /** @var [string] L'email eduge.ch de l'utilisateur */
    public $email;

    /** @var [string] Le nom de l'utilisateur eduge.ch */
    public $name;

    /** @var [string] L'image url de l'utilisateur eduge.ch */
    public $imageurl;

    /** @var [array of ESocialmediaValue] Le tableau des valeurs correspondantes pour
     *  les réseaux sociaux de l'utilisateur. */
    public $socialmedias;

    /**
     * @var [string] L'email privé de l'utilisateur
     */
    public $privatemail;

    /**
     * @var [string] Le numéro de téléphone de l'utilisateur
     */
    public $privatephone;

    /**
     * @var [boolean] L'afichage par défault du téléphone privé
     */
    public $privatephonedefault;

    /**
     * @var [boolean] L'afichage par défault du mail privé
     */
    public $privatemaildefault;
    
    /**
     * @var [string] Le role de l'utilisateur (eleve, admin)
     */
    public $rolescode;

    /**
     * @var [boolean] le bann de l'utilisateur (actif / bani)
     */
    public $bann;
}
?>