<?php

/**
 * La classe EAds contient les informations complémentaire à une annonce
 * d'annonce. Ex: Nom d'article, prix, description, etc.
 */
class EAds{

     /**
     * @brief   Class Constructor avec paramètres par défaut pour construire l'objet
     */
    public function __construct($InArticleId = -1, $InEduMail = "", $InArticleName = "", $InArticlePrice = "", $InArticleDescription = "", $InPublicationDate = "", $InArticleState = -1, $InArticleCategorie = -1, $InImage = "", $InShowPrivateEmail = false, $InShowPrivatePhone = false, $InActived = 0, $InSaleDate = null, $InActivationDate = ""){
        $this->Id = $InArticleId;
        $this->Email = $InEduMail;
        $this->Name = $InArticleName;
        $this->Price = $InArticlePrice;
        $this->Description = $InArticleDescription;
        $this->publicationDate = $InPublicationDate;
        $this->State = $InArticleState;
        $this->Categorie = $InArticleCategorie;
        $this->rawImage = $InImage;
        $this->showPrivateEmail = $InShowPrivateEmail;
        $this->showPrivatePhone = $InShowPrivatePhone;
        $this->criteriasValues = array();
        $this->socialmediasValues = array();
        $this->actived = $InActived; 
        $this->saleDate = $InSaleDate;
        $this->activationDate = $InActivationDate;
    }
    /**
     * Est-ce que l'objet est valide ?
     *
     * @return boolean True si valide, sinon false.
     */
    public function IsValid()
    {
        return ($this->id !== -1);
    }
    
    /** @var [int] Id unique de l'article */
    public $Id;

    /** @var [string] Email unique du propriétaire de l'article */
    public $Email;

    /** @var [string] Nom de l'article */
    public $Name;

    /** @var [int] Prix de l'article */
    public $Price;

    /** @var [string] Description de l'article */
    public $Description;

    /** @var [string] Date de création de l'annonce*/
    public $publicationDate;

    /** @var [int] Id de l'État de l'article */
    public $State;

    /** @var [string] Nom de la catégorie */
    public $Categorie;

    /** @var [longtext] Nom sous format RAW en base64 de l'image */
    public $rawImage;

    /** @var [bool] Montrer ou non l'email privé */
    public $showPrivateEmail;
    
    /** @var [bool] Montrer ou non le téléphone privé */
    public $showPrivatePhone;

    /** @var [ECriteriaValue] Array[] comprenant des critères avec leurs ID et leurs valeur associés */
    public $criteriasValues;

    /** @var [ESocialmediaValue] Array[] comprenant des réseaux sociaux avec leurs CODE et leurs valeur associés */
    public $socialmediasValues;

    /** @var [bool] Permet de savoir si l'annonce est activé ou non */
    public $actived;

    /** @var [string] Date de vente de l'article */
     public $saleDate;
    
     /** @var [string] Date de création de l'annonce*/
    public $activationDate;
}

?>