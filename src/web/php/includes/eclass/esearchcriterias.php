<?php
/**
 * La classe ESearchCriterias contient les éléments de recherche avancée
 * 
 */
class ESearchCriterias{
    /**
     * @brief   Class Constructor avec paramètres par défaut pour construire l'objet
     */
    public function __construct($InText = "", $InCategory = -1, $InState = -1, $InPriceMin = -1, $InPriceMax = -1){
        $this->Text2Find = $InText;
        $this->Category = $InCategory;
        $this->State = $InState;
        $this->PriceMin = $InPriceMin;
        $this->PriceMax = $InPriceMax;
        $this->criteriasValues = array();
    }

    /** @var [string] Le texte à rechercher */
    public $Text2Find;

    /** @var [int] catégorie des articles que l'on recherche */
    public $Category;

    /** @var [int] état des articles que l'on recherche */
    public $State;

    /** @var [int] prix minimum des articles que l'on recherche */
    public $PriceMin;

    /** @var [int] prix maximum des articles que l'on recherche */
    public $PriceMax;

    /** @var [ECriteriaValue] Array[] comprenant les valeurs à rechercher pour les critères */
    public $criteriasValues;
}
?>