<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';
/**
 * @brief : Page de contrôl de la validité de la session
 * @version : 1.0.0
 * @since : 01.02.19
 * @author : Bytyçi
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */

// Est-ce qu'on a un user connecté
if (ESession::getEmail() === false)
{
   header("Location: ./connect.html");
   exit();
}
?>