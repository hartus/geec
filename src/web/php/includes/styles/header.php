<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

$userAccount = EUserHelper::GetUserByEmail($email);
$role = $userAccount->rolescode;

$url = ESession::getUrlImage();
?>

<nav class="navbar navbar-expand-lg navbar-light" style="position:fixed;left:0px;top:0px;width:100%;">
  <a class="navbar-brand <?php echo (basename($_SERVER['PHP_SELF']) == "index.php")?"active":"";?>"
    href="index.php">Bourse aux livres</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == "consultationAds.php")?"active":"";?>"
          href="consultationAds.php" id="consult">Consulter</a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == "ads.php")?"active":"";?>" href="ads.php"
          id="create">Créer</a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == "myads.php")?"active":"";?>" href="myads.php"
          id="modify"><?php echo ($role == ERL_ADMIN) ? "Modifier les annonces Ecole" : "Modifier mes annonces"; ?></a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == "adsManagement.php")?"active":"";?>"
          href="adsManagement.php" id="modify">Modifier les annonces</a>
      </li>
      <!--
        Faire un test pour voir le role de l'utilisateur, s'il est à 99, on affiche ce menu
        (Pour l'instant il reste ici pour le développement)
      -->
      <?php if ($role == ERL_ADMIN) : ?>
      <li class="nav-item">
        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == "users_management.php")?"active":"";?>"
          href="users_management.php" id="management">Gestion des utilisateurs</a>
      </li>

       <li class="nav-item">
          <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == "categoryManagment.php")?"active":"";?>" href="categoryManagment.php" id="categoryManagment">Gestion des catégories</a>
        </li>
      <?php elseif ($role == ERL_STUDENT) : ?>
      <li class="nav-item">
        <!--<a class="nav-link <?php //echo (basename($_SERVER['PHP_SELF']) == "users_management.php")?"active":"";?>" href="users_management.php" id="management">Gestion des utilisateurs</a>-->
      </li>
      <?php endif; ?>
      <li class="nav-item">
        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == "aboutUs.php")?"active":"";?>" href="aboutUs.php">À propos</a>
      </li>
    </ul>
    <?php 
    if (isset($SHOW_SEARCH_BAR) && $SHOW_SEARCH_BAR == true) {
        // On récupère le contexte pour la recherche avancée
        $ctx = SB_CONTEXT_NONE;
        if (isset($SEARCH_BAR_ADVANCED)) 
            $ctx = $SEARCH_BAR_ADVANCED;
    ?>
    <form class="form-inline my-2 my-lg-0" method="POST" name="searchForm">
      <div id="searchBarContent">
        <?php 
        switch ($ctx)
        {
            case SB_CONTEXT_ADS:
            case SB_CONTEXT_USER:
            ?>
        <img id="ShowAdvancedCriteriaSearch">
        <?php
              break;

            case SB_CONTEXT_NONE: 
            default :
              break;
        }
    ?>
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="searchValue">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit" id="SearchButton">Rechercher</button>

        <?php 
        switch ($ctx)
        {
            case SB_CONTEXT_ADS:
                include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/header_search_crit_ads.php';
                break;
            case SB_CONTEXT_USER:
                include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/header_search_crit_user.php';
              break;
              

            case SB_CONTEXT_NONE: 
            default :
              break;

        }
    ?>
      </div>
    </form>
    <?php
    }
    ?>
    <form class="form-inline my-2 my-lg-0">
      <img height="40" width="40" style="object-fit: contain;margin-left:10px;border-radius:25px 25px 25px 25px;"
        src="<?=$url != '' ? $url : '' ?>">
      <li class="nav-item dropdown" style="text-decoration:none;list-style: none;">
        <a class="nav-link dropdown-toggle" style="color:rgba(0,0,0,.5);" href="#" id="navbarDropdownMenuLink"
          role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php echo $name ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="profile.php">Modifier mon profil</a>
          <a class="dropdown-item" name="logout" href="./disconnect.php">Déconnexion</a>
        </div>
      </li>
    </form>
  </div>
</nav>

<script type="text/javascript">
  $("#ShowAdvancedCriteriaSearch").click(function () {
    $("#AdvancedSearchCriterias").toggle("slow");
  });

  $("#searchCategorie").change(function (event) {
    var categoryId = $(this).find("option:selected").val();
    // Si tous sélectioner
    if (categoryId == -1) {
      // On va montrer tous les spans
      $("span.SearchCatFilter").show();
    } else {
      // On va parcourir tous les spans
      $("span.SearchCatFilter").each(function () {
        var catid = $(this).data("catid");
        if (categoryId == catid)
          $(this).show();
        else
          $(this).hide();
      });
    }
    // Cacher tous les spans qui ne sont pas de cette catégorie
  });
</script>