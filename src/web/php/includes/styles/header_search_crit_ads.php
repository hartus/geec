<div id="AdvancedSearchCriterias" style="display:none">
      <form action="src\web\consultationAds.php" method="POST">
            <select name="searchCategorie" id="searchCategorie" class="form-control" onchange="searchAds()">
                  <?php 
                  echo '<option value="-1">Toutes catégories</option>';
                  foreach ($varCategories as $c) {
                        echo '<option value="'. $c->id .'">' . $c->name . '</option>';
                  }
                  ?>
            </select>
            <select name="searchState" id="searchState" class="form-control" onchange="searchAds()">
                  <?php 
                  echo '<option value="-1">Tout état</option>';
                  foreach ($varStates as $c) {
                        echo '<option value="'. $c->code .'">' . $c->name . '</option>';
                  }
                  ?>
            </select>
            <input type="number" class="form-control" name="priceMin" placeholder="Prix Minimum">
            <input type="number" class="form-control" name="priceMax" placeholder="Prix Maximum">
            <br>
            <?php
            $categories = ECategoriesHelper::GetCategories();
            foreach ($categories as $cat) {
                  echo '<span data-catid="'.$cat->id.'" class="SearchCatFilter">';
                  echo '<div >'.$cat->name;
                  foreach ($cat->criterias as $c) {
                        echo '<input type="text" class="form-control" name="'.$c->name.'" data-critid="'.$c->id.'" placeholder="'.$c->name.'">';
                  }
                  echo '</div>';
                  echo '</span>';
            }

            ?>
      </form>
</div>
