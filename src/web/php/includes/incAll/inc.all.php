<?php
session_start();
/**
 * INCLUDE DE LA CONNEXION A LA BDD
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/php/server/database.php';

/**
 * INCLUDE DES FONCTIONS
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/functions.php';

/**
 * INCLUDE DES CONSTANTES
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/constants.php';

/**
 * INCLUDE DES CLASSES
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/estate.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/euser.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/eads.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/epreference.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/ecriteria.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/ecriteriavalue.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/ecategory.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/esocialmedia.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/esocialmediavalue.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/esession.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/esearchcriterias.php';


require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/eadsrsvalue.php';

require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/euserhelper.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/eadshelper.php';

//paramètres envoie d'email
/**
 * @remark Mettre le bon chemin d'accès à votre fichier contenant les constantes
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/config/email.param.php';
// Inclure le fichier swift_required.php localisé dans le répertoire swiftmailer5
require_once $_SERVER['DOCUMENT_ROOT'].'/swiftmailer5/lib/swift_required.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/eclass/ecategorieshelper.php';
