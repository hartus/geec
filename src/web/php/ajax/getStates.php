<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// $data prends les données de la fonction 'GetStates'
$data = GetStates();
// Si nous avons pas de donnée dans $data
if ($data === false) {
    echo '{ "ReturnCode": 2, "Message": "Un problème de récupération de données de GetStates()" }';
    exit();
}

// Si l'on a des données dans data, on encode les données afin de pouvoir les traiter en JS pour faire du Jquery (sélection dynamique)
$jsn = json_encode($data);

// $jsn = data que l'on récupère convertit en json
// S'il y a une erreur dans la conversion de PHP à JSON
if ($jsn === false) {
    $msg = json_last_error_msg();
    echo '{ "ReturnCode": 3, "Message": "Un problème d\'encodage json (' . $msg . ') }';
    exit();
}

// On echo les données dans $jsn qui prends les données récupérés de 'PHP' TO JSON
echo '{ "ReturnCode": 0, "Data": ' . utf8_encode($jsn) . ' }';
exit;
?>