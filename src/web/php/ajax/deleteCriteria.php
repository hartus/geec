<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$critName = filter_input(INPUT_POST, 'critName', FILTER_SANITIZE_STRING);
$catName = filter_input(INPUT_POST, 'catName', FILTER_SANITIZE_STRING);
if (strlen($catName) <= 0)
{
    echo '{ "ReturnCode": 1, "Message" : "Nom de catégorie requis. Contactez le support" }';
    exit;
}

if (strlen($critName) <= 0)
{
    echo '{ "ReturnCode": 1, "Message" : "Nom du critère requis. Contactez le support" }';
    exit;
}

$catId = ECategoriesHelper::GetIdFromCategorie($catName);
$critId = ECategoriesHelper::GetIdFromCriteria($critName);

if (ECategoriesHelper::deleteCriteria($catId, $critId , $critName) == false)
{
    echo '{ "ReturnCode": 2, "Message" : "Problème de supression d\'un critère existant. Contactez le support" }';
    exit;
}

echo '{ "ReturnCode": 0, "Message" : "Supression ok"}';
exit;