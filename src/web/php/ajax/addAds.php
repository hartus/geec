<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Est-ce qu'on modifie une annonce existante
$adsID = intval(filter_input(INPUT_POST, 'adsID', FILTER_SANITIZE_NUMBER_INT));

// Test pour vérifier si l'on a bien une rentrée pour une email
$email = ESession::getEmail();

// Initialisation des variables prenant la valeurs des champs du formulaire sur la page "create_ads.php" - OK
$articleName = filter_input(INPUT_POST,"articleName",FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
$description = filter_input(INPUT_POST,"description",FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);

$s = filter_input(INPUT_POST,"showPrivateEmail",FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
if ($s !== FALSE || $s === "on")
    $showPrivateEmail = true;
else
    $showPrivateEmail = false;

$s = filter_input(INPUT_POST,"showPrivatePhone",FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
if ($s !== FALSE || $s === "on")
    $showPrivatePhone = true;
else
    $showPrivatePhone = false;


$categorieSelect = intval(filter_input(INPUT_POST,"categorieSelect",FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE));
$articlePrice = intval(filter_input(INPUT_POST,"articlePrice",FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE));

// A REVOIR !!!
$state = intval(filter_input(INPUT_POST,"selectState",FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE)); // NOT OK

// No image checked ?
$noImage = filter_input(INPUT_POST,"imageNA",FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
if ($noImage !== FALSE && $noImage === "on")
    $adImage = "";    
else{
    $adImage = filter_input(INPUT_POST,"imageSrc",FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE); // OK
    if($adImage === FALSE || $adImage === "../img/default-img-ads.jpg"){
        echo '{ "ReturnCode": 1, "Message" : "Veuillez mettre une image. Contactez le support" }';
        exit;
    }
}

// Valeurs des critères à false par default - OK
$critValues = array();
$criterias = ECategoriesHelper::GetAllCriterias();

// On parcourt de 1 au dernier index existant
// Un critere est de la forme crit_x ou x est incrémenté de 1 à n
for ($i=1; true; $i++) { 
    if (!isset($_POST['crit_' . strval($i)]))
        break;
    
        $criteriaName = 'crit_' . strval($i);
        $fieldvalue = filter_input(INPUT_POST, $criteriaName, FILTER_SANITIZE_STRING);
        $cv = new ECriteriaValue($i, $fieldvalue);
        array_push($critValues, $cv);
}

// Variable permettant de traiter chaques input pour les traiter 1 par 1
$nbInput = intval(filter_input(INPUT_POST, 'inputCount', FILTER_SANITIZE_NUMBER_INT));
// Array permettant de récupérer le code du réseau social + le default (s'il est checked ou pas)
$arrDefaultValues = array();

// Pour chaque input,  on prend le code du réseau social avec son default
for ($i=1; $i <= $nbInput; $i++) { 
    if (isset($_POST['rsCode' . strval($i)]))
    {
        $code = filter_input(INPUT_POST, 'rsCode' . strval($i), FILTER_SANITIZE_NUMBER_INT);

        if (isset($_POST['rsShow' . strval($i)])){
            // checked (on l'affiche)
            $default = 1;
        }
        else{
            // uncheck (on l'affiche pas)
            $default = 0;
        }
        
        // Si l'on a au moin 1 code à pousser
        if (strlen($code) > 0)
        {
            // On pousse dans la classe les données pour les defaults d'une annonce
            $ev = new EAdsRSValue(0, intval($code),$default);
            array_push($arrDefaultValues, $ev);
        }
    }
}

$ads = new EAds();
$ads->Email = $email;
$ads->Name = $articleName;
$ads->Price = $articlePrice;
$ads->Description = $description;
$ads->State = $state;
$ads->Categorie = $categorieSelect;
$ads->showPrivateEmail = $showPrivateEmail;
$ads->showPrivatePhone = $showPrivatePhone;
$ads->rawImage = $adImage;
$ads->criteriasValues = $critValues;
$ads->socialMediaDefaults = $arrDefaultValues;
$ads->saleDate = "";
$ads->actived = FALSE;



// Modifier une annonce existante?
if ($adsID > 0)
{
    $ads->Id = $adsID;
    if (eadshelper::updateAds($ads) === false)
    {
        // Nous avons pas de donnée pour une annonce,
        // On retourne un message d'erreur parce que nous avons pas de données
        echo '{ "ReturnCode": 2, "Message" : Problème de mise à jour. Contactez le support" }';
        exit();
    }
    
}
else
if (eadshelper::addAds($ads) === false)
{
    // Nous avons pas de donnée pour une annonce,
    // On retourne un message d'erreur parce que nous avons pas de données
    echo '{ "ReturnCode": 3, "Message" : Problème d\'insertion. Contactez le support" }';
    exit();
}

// On echo les données dans $jsn qui prends les données récupérés de 'PHP' TO JSON
if(sendEmailAddAds($ads) === false){
    echo '{ "ReturnCode": 4, "Message" : Problème d\'envoi de mail. Contactez le support" }';
    exit();
    
}
if(sendEmailAddAdsAdmin($ads) === false){
    echo '{ "ReturnCode": 5, "Message" : Problème d\'envoi de mail a l\'admin. Contactez le support }';
    exit();
    
}

echo '{ "ReturnCode": 0, "Message" : "Anonce ajoutée"}';
exit();
?>