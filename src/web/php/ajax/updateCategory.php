<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$oldName = filter_input(INPUT_POST, 'oldCatName', FILTER_SANITIZE_STRING);
$newName = filter_input(INPUT_POST, 'newCatName', FILTER_SANITIZE_STRING);
$catId = intval(filter_input(INPUT_POST, 'catId', FILTER_SANITIZE_STRING));

if (strlen($newName) == 0)
{
    echo '{ "ReturnCode": 1, "Message" : "Nom de catégorie invalide. Contactez le support" }';
    exit;
}

// New category?
if ($catId == 0)
{
    if (ECategoriesHelper::addCategoryName($newName, $catId) === false)
    {
        echo '{ "ReturnCode": 3, "Message" : "Problème d\'ajout de catégorie. Contactez le support" }';
        exit;
    }
    echo '{ "ReturnCode": 0, "Message" : "La catégorie a bien été ajoutée" }';
    exit;
}

// Existing category
if (strlen($oldName) == 0)
{
    echo '{ "ReturnCode": 1, "Message" : "Paramètre ancienne catégorie invalide. Contactez le support" }';
    exit;
}

if (ECategoriesHelper::renameCategory($oldName, $newName) === false)
{
    echo '{ "ReturnCode": 2, "Message" : "Problème de modification d\'une catégorie existante. Contactez le support" }';
    exit;
}
echo '{ "ReturnCode": 0, "Message" : "La catégorie a bien été mise à jour" }';
exit;


