<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
if (strlen($email) <= 0)
{
    echo '{ "ReturnCode": 2, "Message" : "Paramètre email invalide. Contactez le support" }';
    exit;

}
// Mettre à jour un user
$user = new EUser();
$user->email = $email;
$user->privatemail = filter_input(INPUT_POST, 'showPrivateEmail', FILTER_SANITIZE_EMAIL);

$s = filter_input(INPUT_POST, 'emailSwitch', FILTER_SANITIZE_STRING);
if ($s === "on")
    $user->privatemaildefault = 1;

$user->privatephone = filter_input(INPUT_POST, 'showPrivatePhone', FILTER_SANITIZE_NUMBER_INT);

$s = filter_input(INPUT_POST, 'phoneSwitch', FILTER_SANITIZE_STRING);
if ($s === "on")
    $user->privatephonedefault = 1;

$nbInput = filter_input(INPUT_POST, 'inputCount', FILTER_SANITIZE_NUMBER_INT);
$user->socialmedias = array();

for ($i=1; $i <= $nbInput; $i++) {
    $value = filter_input(INPUT_POST, 'rsValue' . strval($i), FILTER_SANITIZE_SPECIAL_CHARS);
    $code = filter_input(INPUT_POST, 'rsSelectValue' . strval($i), FILTER_SANITIZE_SPECIAL_CHARS);

    if(isset($_POST['rsShow' . strval($i)])){
        $default = 1;
    }
    else{
        $default = 0;
    }
    
    if (strlen($value) > 0)
    {
        // Créer la liste des valeurs des réseaux sociaux
        $ev = new ESocialmediaValue($email, intval($code), $value, $default);
        array_push($user->socialmedias, $ev);
    }
}

if (EUserHelper::updateUser($user) === false)
{
    echo '{ "ReturnCode": 1, "Message" : "Problème mise à jour. Contactez le support" }';
    exit;
}

echo '{ "ReturnCode": 0  }';
exit;

