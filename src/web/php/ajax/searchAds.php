<?php
/**
 * @description : fichier retourne les données de la function 
 * @version : 1.0.0
 * @since : 13.03.2019
 * @author : Cuthbert Sébastien, Bytyci Qendrim
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Flag pour savoir si on doit rechercher selon des critères
$bFind = false;

// Les champs fixes du formulaire de recherche
$searchText = filter_input(INPUT_POST, 'searchValue', FILTER_SANITIZE_SPECIAL_CHARS);
if (strlen($searchText) > 0)
    $bFind = true;

$searchCategory = filter_input(INPUT_POST, 'searchCategorie', FILTER_SANITIZE_NUMBER_INT);
if (strlen($searchCategory) > 0 && intval($searchCategory) > 0)
    $bFind = true;

$stateCode = filter_input(INPUT_POST, 'searchState', FILTER_SANITIZE_NUMBER_INT);
if (strlen($stateCode) > 0 && intval($stateCode) > 0)
    $bFind = true;

$searchPriceMin = filter_input(INPUT_POST, 'priceMin', FILTER_SANITIZE_NUMBER_INT);
if (strlen($searchPriceMin) > 0)
    $bFind = true;

$searchPriceMax = filter_input(INPUT_POST, 'priceMax', FILTER_SANITIZE_NUMBER_INT);
if (strlen($searchPriceMax) > 0)
    $bFind = true;

if($searchPriceMax > $searchPriceMin){
    $temp = $searchPriceMax;
    $searchPriceMax = $searchPriceMin;
    $searchPriceMin = $temp;
}

// Tableau des valeurs des critères    
$arr = array();
// Tous les champs variables des critères
$crits = ECategoriesHelper::GetAllCriterias();
foreach ($crits as $c) {
    $fieldname = $c->name;
    $fieldid = ECategoriesHelper::GetIdFromCriteriaName($crits, $fieldname);
    // Pour retrouver dans le post, les espaces sont remplacé par des _
    $value = filter_input(INPUT_POST, str_replace(' ', '_', $fieldname), FILTER_SANITIZE_SPECIAL_CHARS);
    if (strlen($value) > 0)
    {
        $b = new ECriteriaValue($fieldid, $value);
        array_push($arr, $b);
        $bFind = true;
    }
}


$data = false;
if ($bFind)
{
    $searchCriterias = new ESearchCriterias();
    $searchCriterias->Text2Find = $searchText;
    $searchCriterias->Category = intval($searchCategory);
    $searchCriterias->State = intval($stateCode);
    $searchCriterias->PriceMin = intval($searchPriceMin);
    $searchCriterias->PriceMax = intval($searchPriceMax);
    $searchCriterias->criteriasValues = $arr;
    // $data prends les données de la fonction 'EAdsHelper::getAllAdsSearchByCriterias'
    //$data = EAdsHelper::getAllAdsSearchByCriterias($searchCriterias);
    $data = EAdsHelper::getAdsByCriterias($searchCriterias);
}
else
    $data = EAdsHelper::getAllAds(-1, 1);
// Si nous avons pas de donnée dans $data
if ($data === false) {
    echo '{ "ReturnCode": 2, "Message": "Un problème de récupération de données de EAdsHelper::getAllAdsSearchByCriterias()" }';
    exit();
}

// Si l'on a des données dans data, on encode les données afin de pouvoir les traiter en JS pour faire du Jquery (sélection dynamique)
$jsn = json_encode($data);

// $jsn = data que l'on récupère convertit en json
// S'il y a une erreur dans la conversion de PHP à JSON
if ($jsn === false) {
    $msg = json_last_error_msg();
    echo '{ "ReturnCode": 3, "Message": "Un problème d\'encodage json (' . $msg . ') }';
    exit;
}

// On echo les données dans $jsn qui prends les données récupérés de 'PHP' TO JSON
echo '{ "ReturnCode": 0, "Data": ' . utf8_encode($jsn) . ' }';
exit;
?>