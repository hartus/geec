<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';


// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$actived = filter_input(INPUT_POST, 'actived', FILTER_SANITIZE_SPECIAL_CHARS);
$actived = (int)$actived;
$id = (int)$id;
$ads = EAdshelper::getAdById($id);
if ($id <= -1) {
    echo '{ "ReturnCode": 2, "Message": "Un problème de récupération de données" }';
    exit();
}


if (EAdshelper::changedActivated($id, $actived) === false) {
    echo '{ "ReturnCode": 3, "Message": "Un problème de fonction" }';
    exit();
}

// On echo les données dans $jsn qui prends les données récupérés de 'PHP' TO JSON
$result = sendEmailChangedActivated($ads);
if ($result !== true) {
    $s1 = str_replace(array('"', "\r\n"), '', $result->getMessage());
    $s = "Un problème de fonction email. Code erreur <".$result->getCode(). ">  information: <".$s1.">";
    echo '{ "ReturnCode": 3, "Message": "'.$s.'" }';
    exit();
}

echo '{ "ReturnCode": 0, "Message": "ça marche"}';
?>