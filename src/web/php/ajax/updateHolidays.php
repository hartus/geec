<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';


// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$flag = filter_input(INPUT_POST, 'flag', FILTER_SANITIZE_SPECIAL_CHARS);
$days = filter_input(INPUT_POST, 'days', FILTER_SANITIZE_SPECIAL_CHARS);
$isActived = 0;
if($flag == "on")
    $isActived = 1;

if($isActived == 0){
    exit();
}

if ($days == ""){
    if (updatePreferences(0, $days) === false) {
        echo '{ "ReturnCode": 2, "Message": "Un problème de fonction" }';
        exit();
    }
}
else {
    if (updatePreferences($isActived, intval($days)) === false) {
        echo '{ "ReturnCode": 1, "Message": "Un problème de fonction" }';
        exit();
    }
}

// On echo les données dans $jsn qui prends les données récupérés de 'PHP' TO JSON
echo '{ "ReturnCode": 0, "Message": "ça marche"}';
exit();
?>