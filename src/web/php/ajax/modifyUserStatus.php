<?php
/**
 * @description : Appel ajax permettant de récupérer les données d'un utilisateur + encodage en JSON pour traiter ça dynamiquement
 * @version : 1.0.0
 * @since : 05.04.19
 * @author : Bytyci Qendrim
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
$bann = filter_input(INPUT_POST, 'bann', FILTER_VALIDATE_BOOLEAN);

if (strlen($email) <= 0)
{
    echo '{ "ReturnCode": 1, "Message" : "Paramètre email invalide. Contactez le support" }';
    exit;
}

$user = new EUser();
$user->email = $email;
$user->bann = (int)$bann;

if (EUserHelper::updateUserBann($user) === false)
{
    echo '{ "ReturnCode": 2, "Message" : "Problème mise à jour. Contactez le support" }';
    exit;
}

if(sendEmailUserStatusChanged($user) == false){
    echo '{ "ReturnCode": 3, "Message" : Problème de mail. Contactez le support" }';
    exit();
}

echo '{ "ReturnCode": 0, "Message" : a été banni  }';
exit;
