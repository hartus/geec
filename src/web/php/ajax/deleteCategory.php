<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$catId = intval(filter_input(INPUT_POST, 'catId', FILTER_SANITIZE_STRING)); // id de l'annonce que l'on veut suprimmer, -1 en attendant d'avoir la variable dans le post

if (strlen($catId) <= 0)
{
    echo '{ "ReturnCode": 1, "Message" : "L\'id n\'est pas valide" }';
    exit;
}

if (ECategoriesHelper::deleteCategory($catId) === false)
{
    echo '{ "ReturnCode": 2, "Message" : "Problème de supression de catégorie. Contactez le support" }';
    exit;
}

echo '{ "ReturnCode": 0, "Message" : "Supression ok"}';
exit;