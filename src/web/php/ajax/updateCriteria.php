<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');


$oldName = filter_input(INPUT_POST, 'oldCritName', FILTER_SANITIZE_STRING);
$newName = filter_input(INPUT_POST, 'critName', FILTER_SANITIZE_STRING);
$catId = intval(filter_input(INPUT_POST, 'categoryId', FILTER_SANITIZE_NUMBER_INT));

if (strlen($newName) == 0)
{
    echo '{ "ReturnCode": 1, "Message" : "Paramètre nouveau critère invalide. Contactez le support" }';
    exit;
}

if ($catId == 0)
{
    echo '{ "ReturnCode": 1, "Message" : "ID de la catégorie requis. Contactez le support" }';
    exit;
}

if (strlen($oldName) > 0)
{
    if (ECategoriesHelper::renameCriteria($catId, $oldName, $newName) == false)
    {
        echo '{ "ReturnCode": 3, "Message" : "Problème de modification d\'un critère existant. Contactez le support" }';
        exit;
    }
}
else
{
    $id = ECategoriesHelper::addCriteriaName($catId, $newName);
    if ($id === false)
    {
        echo '{ "ReturnCode": 4, "Message" : "Problème d\'ajout de critère. Contactez le support" }';
        exit;
    }

    echo '{ "ReturnCode": 10, "CreatedCritID": '.$id.', "CategoryID": '.$catId.', "CriteriaName": "'.$newName.'"  }';
    exit;
}


echo '{ "ReturnCode": 0 }';
