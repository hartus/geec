<?php
/**
 * @description : fichier check si le user existe.
 * @version : 1.0.0
 * @since : 13.12.18
 * @author : Cuthbert Sébastien, Bytyci Qendrim
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';


// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère les paramètres
$email = "";
if (isset($_POST['useremail'])){
    $email = filter_input(INPUT_POST, 'useremail', FILTER_SANITIZE_EMAIL);
    
    // Récupération de l'email de l'utilisateur connecté pour le mettre en session
    ESession::setEmail($email);
}

$name = "";
if (isset($_POST['username']))
    $name = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

$userPictureURL = "";
if (isset($_POST['userurlimage'])){
    $userPictureURL = filter_input(INPUT_POST, 'userurlimage', FILTER_SANITIZE_STRING);

    // Récupération de l'image de profil de l'utilisateur connecté pour le mettre en session
    ESession::setUrlImage($userPictureURL);
}


if (strlen($email) > 0 && strlen($name) > 0 && strlen($userPictureURL) > 0)
{
    $bUserExists = EUserHelper::UserExists($email);
    if ($bUserExists == false)
    {
        $usr = new EUser($email,$name,$userPictureURL);
        if (EUserHelper::CreateUser($usr) === false)
        {
            // Si j'arrive ici, c'est pas bon
            echo '{ "ReturnCode": 2, "Message": "Impossible de créer l\'utilisateur."}';
            exit();    
        }
    }

    $dbPictureURL = EUserHelper::getUserPicture($email);
    if ($dbPictureURL !== false)
    {
        if ($userPictureURL != $dbPictureURL)
        {
            if (EUserHelper::updateUserPic($email, $userPictureURL) == false)
            {
                /** @todo ajouter un code d'erreur*/
            }
        }
    }
    // @brief Controle si les conditions d'utilisation sont validées
    if (EUserHelper::UserConditions($email) == false)
    {
        echo '{ "ReturnCode": 3, "Message": ""}'; 
        exit();
    }
    // Si les conditions sont accéptés, on traite la situation si un utilisateur est banni ou pas
    else
    {
        $user = EUserHelper::GetUserByEmail($email);
        if ($user === FALSE)
        {
            echo '{ "ReturnCode": 6, "Message": "Une erreur s\'est produite."}';     
            exit();
        }
        switch ($user->bann)
         {
            case 0: // OK, pas banni
            {
                break;
            }
            case 1: // Banni
            {
                echo '{ "ReturnCode": 4, "Message": "Votre compte a été banni"}';     
                exit();
            }
            default: // Invalid
            {
                echo '{ "ReturnCode": 5, "Message": "Impossible de récupérer le statut"}'; 
                exit();
    
            }
         }
    }
    if ($bUserExists == false && $users != null)
    {
        // Envoie d'email à un administrateur pour le mettre au courant d'une nouvelle inscription
        if($usersendEmailNewUser($users) === false){
            echo '{ "ReturnCode": 1, "Message" : Problème de mail. Contactez le support" }';
            exit();
        }
    }
    // OK
    echo '{ "ReturnCode": 0, "Message": ""}';
    exit();
}
else
{
    // Si j'arrive ici, c'est pas bon
    echo '{ "ReturnCode": 1, "Message": "Il manque des paramètres"}';
}
?>