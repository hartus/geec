<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';


// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$saleDate = filter_input(INPUT_POST, 'saleDate', FILTER_SANITIZE_SPECIAL_CHARS);
$id = (int)$id;

$saleDate = date("Y-m-d H:i:s");



if ($id <= -1) {
    echo '{ "ReturnCode": 2, "Message": "Un problème de récupération de données" }';
    exit();
}


if (EAdshelper::updateSaleDate($id, $saleDate) === false) {
    echo '{ "ReturnCode": 3, "Message": "Un problème de fonction" }';
    exit();
}

// On echo les données dans $jsn qui prends les données récupérés de 'PHP' TO JSON
echo '{ "ReturnCode": 0, "Message": "ça marche"}';
exit();
?>