<?php
/**
 * @description : Erreur survenue.
 * @version : 1.0.0
 * @since : 05.04.19
 * @author : DOA
 * @copyright : Entreprise Ecole CFPT-I © 2021
 */

include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/check_session.php';

$varCategories = ECategoriesHelper::GetCategories();
$varStates = GetStates();

$email = ESession::getEmail();

$userAccount = EUserHelper::GetUserByEmail($email);
$name = $userAccount->name;
$edumail = $userAccount->email;
$privateEmail = $userAccount->privatemail;
$privatePhone = $userAccount->privatephone;

if($userAccount->bann == 0)
{
    header("Location: ./index.php");
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/style.css">
    <title>Bourse aux livres</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript" src="./js/eelauth.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>

<body>

    <div class="container-fluid" style="width: 20rem;margin: auto;">
        <div class="card mt-5">
            <div class="card-body text-center">
                <h5 class="card-title">Erreur</h5>
                <p>Une erreur est survenue.</p>
                <p>Contacter le support</p>
            </div>
        </div>
    </div>

    <?php include './php/includes/styles/footer.php'; ?>
    <!--CDN POUR LE JQUERY-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>
</body>

</html>