<?php
include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/check_session.php';
/**
 * @brief : Page de consultation de catégorie et de critères
 * @version : 1.0.0
 * @since : 08.05.19
 * @author : Cuthbert
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */

$varCategories = ECategoriesHelper::GetCategories();
$varStates = GetStates();

$email = ESession::getEmail();

$userAccount = EUserHelper::GetUserByEmail($email);
$name = $userAccount->name;
$edumail = $userAccount->email;
// Pour afficher la search bar
$SHOW_SEARCH_BAR = false;
// Quel contexte pour les paramètres avancés de la search bar
$SEARCH_BAR_ADVANCED = SB_CONTEXT_GESTION;

$roleUser = $userAccount->rolescode;

if($roleUser == ERL_STUDENT)
{
    header("Location: ./index.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- ============= LINKS ============= -->
    <link rel="stylesheet" href="./css/uikit.min.css">

    <title>Bourse aux livres</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script src="./js/uikit.min.js"></script>
    <script type="text/javascript" src="./js/eelauth.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <script src="./js/functions.js"></script>

    <script src="./js/inc.all.constants.js"></script>

    <!-- ============= CUSTOM ============= -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- ============= END ============= -->
</head>

<body style="background-color:transparent;">
<style>
    .background-image {
    position: absolute;
    background-image: url(../img/test.png);
    background-repeat: no-repeat;
    background-size: cover;
    height: 980px;
    width: 45%;
    top: 0;
    right: 0;
    z-index: -1;
    }

    .edit-field{
        width: 80%;
    }
    </style>
    <?php include './php/includes/styles/header.php'; ?>
    <div class="background-image">
        <img src="./img/delimiter3.png" alt="">
    </div>
    <div class="container-fluid" style="margin-top:80px !important;">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-4 shadow-lg uk-animation-slide-bottom-small" style="width: 65rem;margin: auto;border:none;border-radius:20px;">
                    <table class="table table-striped table-bordered table-sm mb-0" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="th-sm" style="border-top:1px solid transparent;border-left:1px solid transparent;">Catégories</th>
                                <th class="th-sm" style="border-top:1px solid transparent;border-right:1px solid transparent;" title="Vous pouvez spécifiez ** devant le critère afin de le rendre obligatoire à la saisie">Critères</th>
                            </tr>
                        </thead>

                        <tbody id="tableContent">
                            <?php

                            foreach($varCategories as $cat) {
                                echo '<tr><td id="'.$cat->id.'"><label id="lbl-' . $cat->id . '">' . $cat->name . '</label>';
                                echo '<input type="text" class="edit-field" id="edt-'.$cat->id.'" value="' . $cat->name .'" style="display:none">';
                                echo '<button id="btnmod-' . $cat->id .'" class="btn btn-info float-right" onclick="modifyCat('.$cat->id.')" data-toggle="tooltip" data-placement="top" title="✎ la catégorie">✎</button> <button id="btn-'.$cat->id.'" onclick="deleteCat('. $cat->id .')" class="btn btn-danger float-right mr-2" data-toggle="tooltip" data-placement="top" title="Supprimer la catégorie"> X </button>';
                                echo '</td><td>';
                                echo '<ul class="list-group" id="crit'.$cat->id.'">';
                                foreach($cat->criterias as $crit) {
                                    echo '<li id="critli-'.$cat->id.'-'.$crit->id.' "class="list-group-item">';
                                    echo '<span><label id="critlbl-'.$cat->id.'-'.$crit->id.'">' . $crit->name . '</label>';
                                    echo '<input type="text" class="edit-field" id="critedt-'.$cat->id.'-'.$crit->id.'" value="' . $crit->name .'" style="display:none">';
                                    echo '<button id="critbtnmod-' . $cat->id . '-' . $crit->id . '" class="btn btn-info float-right btn-edit" onclick="modifyCrit('.$cat->id.', '.$crit->id.')" data-toggle="tooltip" data-placement="top" title="✎ le critère">✎</button> <button id="critbtn-'.$cat->id.'-'.$crit->id.'" onclick="deleteCrit('. $cat->id .', '.$crit->id.')" class="btn btn-danger float-right mr-2" data-toggle="tooltip" data-placement="top" title="Supprimer le critère"> X </button>';
                                    echo '</span>';
                                    echo '</li>';
                                }// #end foreach crit                                echo '<span class="w-100 float-right text-right">';

                                echo '</ul><button class="mt-4 mb-4 mr-4 btn btn-success w-25 float-right" id="AddCriteria-'.$cat->id.'" style="font-weight:bold;" onclick="startAddCriteria4Category('. $cat->id .')"> + </button></td</tr>';
                                echo '</span>';
                            }
                            ?>
                        </tbody>
                    </table>
                    <button type="button" class="btn btn-success w-25 mb-4 mt-4 ml-4" id="AddCategory" data-toggle="tooltip" data-placement="top" title="Ajouter une nouvelle catégorie">+</button>
                </div>
                <div class="col text-left" id="infoMessage"></div>
            </div>
        </div>
    </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous">
    </script>
    <script>
        /**
         * The cat ID for a new category not created yet
         */
        var newCatID = 0;
        /**
         * The criteria ID for a new criteria not created yet
         */
        var newCritID = 0;
        /**
         * Contient les libellés des catégories changé au momnet de la modification d'une catégorie
         */
        var arrLabels = [];
        /**
         * Contient les libellés des critères changé au momnet de la modification d'un critère
         */
        var arrCritLabels = [];
        /**
         * Contient un flag permettant de savoir si ce qui est saisit dans l'input est valide
         */
        var inputValidated = false;
        /**
         * Le nom du critère au départ de la modification du critère
         */
        var ModifiedOldCriterianame = "";
        /**
         * Le tableau qui contient tous les critères au moment de la création ou de la modification d'un critère
         */
        var arrCritLabels = [];


        $(document).ready(function () {
            $("#AddCategory").click(addEmptyCategoryLine);
        });

        /**
         * Retourne la un tableau de critère
         */
        function getCat(event) {
            if (event != undefined)
                event.preventDefault();
            $.ajax({
                method: 'POST',
                url: "./php/ajax/getCategories.php",
                dataType: 'json',
                success: function (data) {
                    var msg = '';
                    switch (data.ReturnCode) {
                        case 0: // tout bon
                            showCat(data.Data);
                            break;
                        case 1: // problème de récupération
                        case 2: // problème d'encodage
                        default:
                            msg = data.Message;
                            break;
                    }
                    if (msg.length > 0) {
                        displayMessageById("#infoMessage", msg, 3);
                    }
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }

        /**
         * Ajoute une nouvelle ligne de categorie
         */
        function addEmptyCategoryLine() {
            /* @brief Previously the id for an empty category was the last id + 1
             *        This wont work since we don't know what will be the next id
             *        on the database.
             */
            //var el = $("#tableContent tr").last().find("td");
            //var catId = parseInt(el.attr('id')) + 1;
            var row = '<tr><td id="' + newCatID + '"><label id="lbl-' + newCatID + '">' + "" + '</label>';
            row = row + '<input type="text" class="edit-field" id="edt-' + newCatID + '" value="' + "" + '" style="display:none;width=100%" placeholder="Enter pour valider. Esc pour annuler">';
            //row = row + '<button id="btnmod-' + newCatID + '" class="btn btn-info float-right" onclick="modifyCat(' + newCatID + ')" >✎</button> <button id="btn-' + newCatID + '" onclick="deleteCat(' + newCatID + ')" class="btn btn-danger float-right mr-2"> X </button>';
            row = row + '</td></tr>';
            $("#tableContent").append(row);
            modifyCat(newCatID);
        }

        /**
         * Supprime la section de la nouvelle categorie
         */
        function removeEmptyCategoryLine() {
            $("#tableContent tr").last().remove();
        }

        /**
         * Supprime la section du nouveau critère
         * @param catId L'id de la catégorie pour laquelle on veut supprimer le critère 
         */
        function removeEmptyCriteriaLine(catId){
            $("#critli-" + catId + "-" + newCritID).remove();
        }
        /**
         * Ajoute une nouvelle ligne de critère
         * @param catId L'id de la catégorie pour laquelle on veut ajouter un critère
         * @remark Le nouveau critère est créé avec un id à 0 @see newCritID
         *         Une fois la confirmation en ajax envoyée au serveur pour créer
         *         le critère dans la base de données, on va remplacer cet id
         *         par l'id stocké dans la base de données
         */
        function addHTMLCriteriaLine(catId, critId, critValue="") {
            $('#buttonAddCrit-' + catId).attr('disabled','disabled');
            var li = '<li id="critli-'+ catId +'-'+critId+'" class="list-group-item"><span><label id="critlbl-' + catId + '-'+critId+'">' + critValue + '</label>';
            li = li + '<input type="text" class="edit-field" id="critedt-' + catId + '-'+critId+'" value="' + "" + '" style="display:none" placeholder="Enter pour valider. Esc pour annuler">';
            if (critId != newCritID)
                li = li + '<button id="critbtnmod-' + catId + '-'+critId+'" class="btn btn-info float-right btn-edit" onclick="modifyCrit(' + catId + ','+critId+')" >✎</button> <button id="critbtn-' + catId + '-'+critId+'" onclick="deleteCrit(' + catId + ','+critId+')" class="btn btn-danger float-right mr-2"> X </button>';
            li = li + '</span></li>';
            $("#crit" + catId).append(li);
        }
        /**
         * Démarre le processus d'ajout d'un nouveau critère
         * @param catId L'id de la catégorie pour laquelle on veut ajouter un critère
         */
        function startAddCriteria4Category(catId) {
            addHTMLCriteriaLine(catId, newCritID);
            modifyCrit(catId, newCritID);
        }

        /**
         * Récupère les libellés des catégories 
         * @return un tableau de string
         */
        function getLabelCategories() {
            var arr = [];
            var idx = 0;
            // To avoid infinite loop, we fixed a maximum to 100 categories
            while (++idx < 100) {
                var lbl = $("#lbl-" + idx).text();
                if (lbl.length == 0)
                    break;
                arr.push(lbl);
            }
            return arr;
        }

        /**
         * Récupère les libellés des critères de la catégorie
         * @return un tableau de string
         */
        function getLabelCriterias(id) {
            var arr = [];
            var catId = 1;
            while (true) {
                var lbl = $("#critlbl-" + id + "-" + catId).text();
                if (lbl.length == 0)
                    break;
                arr.push(lbl);
                catId++;
            }
            return arr;
        }

        /**
         * fonction d'envoi des informations de modification d'une catégorie
         * @param [string] oldCatName l'ancien nom de la catégorie
         * @param [string] catName le nom acctuel de la catégorie
         * @param [integer] catId l'id de la catégorie concernée
         */
        function updateCategory(oldCatName, catName, catId) {
            $.ajax({
                method: 'POST',
                url: "./php/ajax/updateCategory.php",
                data: {
                    'oldCatName': oldCatName,
                    'newCatName': catName,
                    'catId': catId
                },
                dataType: 'json',
                success: function (data) {
                    var msg = '';
                    switch (data.ReturnCode) {
                        case 0: // tout bon
                            break;
                        case 1: // problème de catégorie
                        case 2: // problème de modification
                        case 3: // problème d'ajout
                        default:
                            msg = data.Message;
                            break;
                    }
                    if (msg.length > 0) {
                        displayMessageById("#infoMessage", msg, 3);
                    }
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }

        function deleteCat(catId) {
            UIkit.modal.confirm('Confirmez la suppression définitive de la catégorie!').then(function() {
                $('#lbl-' + catId).hide();
                $('#btn-' + catId).hide();
                $('#btnmod-' + catId).hide();
                delCat(catId);
            }, function () {
                // no delete
            });
        }


        /**
         * fonction de supression d'une catégorie
         * @param [integer] catId l'id de la catégorie concernée
         */
        function delCat(catId) {
            $.ajax({
                method: 'POST',
                url: "./php/ajax/deleteCategory.php",
                data: {
                    'catId': catId
                },
                dataType: 'json',
                success: function (data) {
                    var msg = '';
                    switch (data.ReturnCode) {
                        case 0: // tout bon
                            break;
                        case 1: // problème d'id
                        case 2: // problème de supression
                        default:
                            msg = data.Message;
                            break;
                    }
                    if (msg.length > 0) {
                        displayMessageById("infoMessage", msg, 3);
                    }
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }

        /**
         * Call-back lorsqu'on clicke sur le bouton modifier de la catégorie
         * @param [integer] id l'identifiant de la catégorie
         */
        function modifyCat(id) {
            // On cache le libellé et les boutons
            $("#AddCategory").hide();
            $("#lbl-" + id).hide();
            $("#btn-" + id).hide();
            $("#btnmod-" + id).hide();
            // On affiche l'edit
            $("#edt-" + id).show();
            $("#edt-" + id).focus();
            arrLabels = getLabelCategories();
            var oldname = $("#lbl-" + id).html();
            // On veut capturer Enter et Esc
            $("#edt-" + id).keyup(function (event) {
                var catName = $("#edt-" + id).val();
                switch (event.which){
                    case 13: //Enter
                    {
                        if (catName.length > 0) {
                            if (oldname != catName) {
                                $("#lbl-" + id).html(catName);
                                updateCategory(oldname, catName, id);
                            }
                            $("#AddCategory").show();
                            $("#lbl-" + id).show();
                            $("#edt-" + id).hide();
                            $("#btn-" + id).show();
                            $("#btnmod-" + id).show();

                        } else {
                            $("#edt-" + id).css("color", "red");
                            //$("#edt-" + id).val(oldname);
                        }
                        break;
                    }
                    case 27: //Esc
                    {
                        $("#AddCategory").show();
                        // Are we editing a new empty category?
                        if (id == newCatID){
                            removeEmptyCategoryLine();
                            return;
                        }
                        $("#edt-" + id).val(oldname);
                        $("#lbl-" + id).show();
                        $("#edt-" + id).hide();
                        $("#btn-" + id).show();
                        $("#btnmod-" + id).show();

                        break;
                    }
                    default: 
                    {
                        if (catName.length > 0) {
                            $("#edt-" + id).css("background-color", "");
                            // Est-ce que la chaîne saisie existe déjà dans les catégories
                            // mais pas comme nom précédent
                            if (arrLabels.indexOf(catName) >= 0 && oldname != catName) {
                                $("#edt-" + id).css("color", "red");
                            } else {
                                $("#edt-" + id).css("color", "");
                            }
                        } else {
                            $("#edt-" + id).css("background-color", "red");
                        }

                        break;
                    }
                }
            });
        } //#end modifyCat


        /**
         * Call-back lorsqu'on clicke sur le bouton modifier d'un critère
         * @param integer catid l'identifiant de la catégorie
         * @param integer critid l'identifiant du critère
         */
        function modifyCrit(catid, critid) {
            $("#AddCriteria-" + catid).hide();
            // On cache le libellé
            $("#critlbl-" + catid + "-" + critid).hide();
            $("#critbtn-" + catid + "-" + critid).hide();
            $("#critbtnmod-" + catid + "-" + critid).hide();

            // On affiche l'edit
            $("#critedt-" + catid + "-" + critid).show();
            $("#critedt-" + catid + "-" + critid).focus();
            arrCritLabels = getLabelCriterias(catid);
            oldCritname = $("#critedt-" + catid + "-" + critid).val();
            // On veut capturer Enter et Esc
            $("#critedt-" + catid + "-" + critid).keyup(function (event) {
                // On récupère la valeur saisie
                var critValue = $(this).val();
                switch (event.which){
                    case 13: //Enter
                    {
                        if (critValue.length > 0) {
                            if (oldCritname != critValue) {
                                $("#critlbl-" + catid + "-" + critid).html(critValue);
                                updateCriteria(catid, oldCritname, critValue);
                            }
                        } else {
                            $("#critedt-" + catid + "-" + critid).css("color", "red");
                        }
                        $("#AddCriteria-" + catid).show();
                        $("#critlbl-" + catid + "-" + critid).show();
                        $("#critedt-" + catid + "-" + critid).hide();
                        $("#critbtn-" + catid + "-" + critid).show();
                        $("#critbtnmod-" + catid + "-" + critid).show();

                        // On débloque l'accès au bouton d'ajout
                        $("#buttonAddCrit-"+catid).removeAttr('disabled');

                        break;
                    }
                    case 27: //Esc
                    {
                        $("#AddCriteria-" + catid).show();
                        if (critid == newCritID){
                            removeEmptyCriteriaLine(catid);
                            return;
                        }

                        $("#critedt-" + catid + "-" + critid).val(oldCritname);

                        $("#critlbl-" + catid + "-" + critid).show();
                        $("#critedt-" + catid + "-" + critid).hide();
                        $("#critbtn-" + catid + "-" + critid).show();
                        $("#critbtnmod-" + catid + "-" + critid).show();
                        $("#buttonAddCrit-"+catid).removeAttr('disabled');

                        break;
                    }
                    default: 
                    {    
                        if (critValue.length > 0) {
                            $("#critedt-" + catid + "-" + critid).css("background-color", "");
                            if (arrCritLabels.indexOf(critValue) < 0 && oldCritname != critValue){
                                $("#critedt-" + catid + "-" + critid).css("color", "");
                            } else {
                                $("#critedt-" + catid + "-" + critid).css("color", "red");
                            }
                        } else {
                            $("#critedt-" + catid + "-" + critid).css("background-color", "red");
                        }
                        break;
                    }
                }
            });
        }

        /**
         * Call-back lorsqu'un nouveau critère a été ajouté
         * @param [integer] catId l'identifiant de la catégorie
         * @param [integer] critId l'identifiant du critère
         * @param [string] critValue La valeur du critère
         */
        function OnCreatedCriteria(catId, critId, critValue){
            addHTMLCriteriaLine(catId, critId, critValue);
            // On va supprimer la zone li utilisée pour l'edit
            removeEmptyCriteriaLine(catId);

        }

        /**
         * fonction d'envoi des informations de modification d'une catégorie
         * @param [integer] catId l'identifiant de la catégorie liée
         * @param [string] oldCritName l'ancien nom du critère
         * @param [string] critName le nouveau nom du critère
         */
        function updateCriteria(catId, oldCritName, critName) {
            $.ajax({
                method: 'POST',
                url: "./php/ajax/updateCriteria.php",
                data: {
                    'categoryId': catId,
                    'oldCritName': oldCritName,
                    'critName': critName,
                },
                dataType: 'json',
                success: function (data) {
                    var msg = '';
                    switch (data.ReturnCode) {
                        case 0: // tout bon
                            break;
                        case 10: //Ajout du nouveau critère, réussit
                            OnCreatedCriteria(data.CategoryID, data.CreatedCritID, data.CriteriaName);
                            break;
                        case 1: // pas de catégorie ou problème de nouveau critère
                            msg = data.Message;
                            break;
                        case 3: //problème de modification de critère
                        case 4: //problème d'ajout
                        default:
                            msg = data.Message;
                            break;
                    }
                    if (msg.length > 0) {
                        displayMessageById("#infoMessage", msg, 3);
                    }
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }

        /**
         * Call-back lorsqu'on clicke sur le bouton modifier d'un critère
         * @param [integer] catId l'identifiant de la catégorie
         * @param [integer] critId l'identifiant du critère
         */
        function deleteCrit(catId, critId) {
            UIkit.modal.confirm('Confirmez la suppression définitive du critère!').then(function() {
                $("#critlbl-" + catId + "-" + critId).remove();
                $("#critbtn-" + catId + "-" + critId).remove();
                $("#critbtnmod-" + catId + "-" + critId).remove();
                $("#critli-" + catId + "-" + critId).remove();
                var critName = $("#critedt-" + catId + "-" + critId).val();
                var catname = $("#lbl-" + catId).html();
                delCrit(catname, critName);
            }, function () {
                // no delete
            });
        }

        /**
         * Call-back lorsqu'on clicke sur le bouton supprier un critère
         * @param integer catid l'identifiant de la catégorie
         * @param integer id l'identifiant du critère
         */
        function delCrit(catName, critName) {
            $.ajax({
                method: 'POST',
                url: "./php/ajax/deleteCriteria.php",
                data: {
                    'catName': catName,
                    'critName': critName
                },
                dataType: 'json',
                success: function (data) {
                    var msg = '';
                    switch (data.ReturnCode) {
                        case 0: // tout bon
                            break;
                        case 1: // problème de nom de catégorie
                        case 2: // problème de supression
                        default:
                            msg = data.Message;
                            break;
                    }
                    if (msg.length > 0) {
                        displayMessageById("infoMessage", msg, 3);
                    }
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }
    </script>
</body>
<?php include './php/includes/styles/footer.php'; ?>

</html>