<?php
/**
 * @brief : Page de gestion des annonces
 * @version : 1.0.0
 * @since : 02.05.19
 * @author : Güner
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */

include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/check_session.php';


$email = ESession::getEmail();
$userAccount = EUserHelper::GetUserByEmail($email);
if($userAccount->bann == 1)
{
    header("Location: ./banned.php");
}
$roleUser = $userAccount->rolescode;
$p = ReadPreferences();
$adsarr = EAdsHelper::getAllAds();

if($roleUser == ERL_ADMIN)
    $adsarr = EAdsHelper::getAllAds();
else if($p->FlagHoliday == 1)
    $adsarr = EAdsHelper::getAdsByEmail($email, -1, ADS_STATUS_ACTIVATED);

$catarr = ECategoriesHelper::GetCategories();
$name = $userAccount->name;

$pref = ReadPreferences();
$flag = intval($pref->FlagHoliday);
$days = intval($pref->DayForActivation);

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- ============= LINKS ============= -->
    <link rel="stylesheet" href="./css/uikit.min.css">
    <title>Bourse aux livres</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript" src="./js/eelauth.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="./js/functions.js"></script>

    <!-- ============= CUSTOM ============= -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- ============= END ============= -->

</head>

<body style="background-color:transparent;">
    <style>
        .background-image {
            position: absolute;
            background-image: url(../img/test.png);
            background-repeat: no-repeat;
            background-size: cover;
            height: 980px;
            width: 45%;
            top: 0;
            right: 0;
            z-index: -1;
        }
    </style>
    <?php include './php/includes/styles/header.php'; ?>
    <div class="background-image">
        <img src="./img/delimiter3.png" alt="">
    </div>

    <div class="container-fluid" style="margin-top:60px !important;">
        <div class="row">
            <div class="col-md-12">
                <div class="mt-4 uk-animation-slide-bottom-small"
                    style="width: 65rem;margin: auto;border:none;border-radius:20px;">
                    <div class="row mx-auto" style="width: 65rem;">
                        <button class="btn btn-outline-success my-2 my-sm-0 ml-auto" type="submit" id="showAll">Tout
                            afficher</button>
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit" id="showUnsoldAds">Annonces
                            non vendus</button>
                        <button class="btn btn-outline-success my-2 my-sm-0 mr-auto" type="submit"
                            id="showSoldAds">Annonces vendues</button>
                        <?php 
                    if($roleUser == ERL_ADMIN)
                    {
                    echo '<button class="btn btn-danger my-2 my-sm-0" data-toggle="modal" data-target="#holidayModal" >Préférences</button>';
                    } ?>
                    </div>
                    <div style="display:none;" class="alert col-md-5 alert-danger mx-auto text-center mt-4" id="message"></div>
                </div>
                <div class="card mt-4 shadow-lg uk-animation-slide-bottom-small"
                    style="width: 65rem;margin: auto;border:none;border-radius:20px;border:transparent;">
                    <table class="table table-striped table-bordered table-sm" style="border:none;" cellspacing="0"
                        width="100%">
                        <thead>
                            <tr>
                                <th style="width:120px;border-left:transparent;border-top:transparent;"
                                    class="th-sm text-center"></th>
                                <th class="th-sm" style="border-top:transparent;">Titre</th>
                                <th class="th-sm text-center" style="border-top:transparent;">Catégories</th>
                                <th class="th-sm text-center" style="border-top:transparent;">Activé</th>
                                <th class="th-sm text-center" style="border-right:none;border-top:none;">Vendu</th>
                            
                            </tr>
                        </thead>

                        <tbody id="tableContent">
                            <?php
                            foreach($adsarr as $ads) {
                                $Category = ECategoriesHelper::GetCategoryNameFromId($catarr, $ads->categorie);
                                if ($Category === false)
                                    $Category = "#err#";

                                echo '<tr class="';
                                if ($ads->saleDate != null && strlen($ads->saleDate) > 0){
                                    echo " unSold ";}
                                else{echo " sold ";}
                                echo '">';
                                echo '<td><img src="'.$ads->rawImage.'" width="128" height="128" style="object-fit: contain; text-align:center;"></td>';
                                echo '<td style="min-width: 165px; min-height: 165px; overflow-y: auto;"><p class="p-0"><a style="text-decoration:none;" href="post_page.php?id='.$ads->Id.'"><b>'.$ads->Name.'</a></b></p><p class="p-0">'.limitString($ads->Description, 300).'</p></td>';
                                echo '<td style="text-align:center;min-width:150px;transform: translate3D(0%, 35%, 0);">'.$Category.'</td>';

                                echo '<td style="min-width:120px;transform: translate3D(0%, 35%, 0);"> <div class="custom-control custom-switch w-100 text-center" style="padding-left:45px;">';
                                echo '<input type="checkbox" class="custom-control-input" name="activatedSwitch" id="activatedSwitch'.$ads->Id.'"';
                                if($ads->actived == 1)
                                    echo ' checked ';
                                else if($ads->saleDate != null)
                                    echo ' disabled ';
                                echo ' onchange="enabledAds('.$ads->Id.')">';
                                echo '<label class="custom-control-label" for="activatedSwitch'.$ads->Id.'"></label></div></td>';

                                echo '<td style="min-width:120px;text-align:center;border-right:none;transform: translate3D(0%, 35%, 0);"><div class="custom-control custom-switch w-100 text-center">';
                                echo '<input type="checkbox" class="custom-control-input" name="soldSwitch" id="soldSwitch'.$ads->Id.'"';
                                if($ads->saleDate != null){
                                    echo ' checked ';
                                    echo ' disabled ';
                                }
                                echo  'onchange="confirmSold('.$ads->Id.')">';
                                echo '<label class="custom-control-label" for="soldSwitch'.$ads->Id.'"></label>';

                                echo '</div></td>';
                                echo '</tr>';
                            } //#end foreach

                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="holidayModal" tabindex="-1" role="dialog" aria-labelledby="holidayModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="holidayModalLabel">Préférences</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Activer le mode vacances ! : <input type="checkbox" name="flagHoliday" id="flagHoliday" <?php if($flag == 1) 
                    echo ("checked");
                    else
                    echo("");
                    ?>>
                    <br>
                    Nombre de jours : <input type="number" name="dayNumber" value="<?= $days ?>" min="0" max="150" id="dayNumber" <?php if($flag == 1) 
                    echo ("");
                    else
                    echo("disabled");
                    ?>>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="button" onclick="getPreferences()" data-dismiss="modal" class="btn btn-success">Sauvegarder</button>
                </div>
            </div>
        </div>
    </div>

    <!--CDN POUR LE JQUERY-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous">
    </script>
    <script language="JavaScript" type="text/JavaScript">
        var ads = <?php echo json_encode($adsarr); ?> ;
        var cats = <?php echo json_encode($catarr); ?> ;

        $(document).ready(function () {
            // Au chargement de la page on recherche toutes les annonces.
            //searchAds();

            $("#showUnsoldAds").click(function (event) {
                $("#tableContent tr").show();
                $("#tableContent tr.unSold").toggle();
            });
            $("#showSoldAds").click(function (event) {
                $("#tableContent tr").show();
                $("#tableContent tr.sold").toggle();
            });

            $('#showAll').click(function (event) {
                $("#tableContent tr").show();
            });

        });

        function confirmSold(id) {

            if (document.getElementById("soldSwitch" + id).checked = false) {
                return true;
            } else {
                var box = confirm("Si vous passez votre annonce comme \"vendue\" vous ne pourrez plus la modifier. Si vous voulez temporairement la désactiver du site il vous suffit de cliquer sur \"Désactiver.\"  Appuyer sur ok pour confirmer la vente .");
                if (box == true) {
                    var ad = ads.find(a => {
                        return a.id === id;
                    });
                    // Appel ajax
                    $.post('./php/ajax/updateSaleDate.php', {
                        id: id,
                        saleDate: ads.saleDate
                    }).done(function (data) {
                        console.log(data);
                    });

                    document.getElementById("activatedSwitch" + id).disabled = true;
                    document.getElementById("activatedSwitch" + id).checked = false;
                    document.getElementById("soldSwitch" + id).checked = true;
                    document.getElementById("soldSwitch" + id).disabled = true;

                    
                    return true;
                } else {
                    document.getElementById("soldSwitch" + id).checked = false;
                }
            }
        }

        function enabledAds(id) {
            var ad = ads.find(a => {
                return a.Id === id;
            });

            // Swap activated status.
            ad.actived = ad.actived == 0 ? 1 : 0;

            /* @todo à supprimer
            Appel ajax
            $.post('./php/ajax/changeActived.php', {
                id: id,
                actived: ad.actived
            }).done(function (data) {
                console.log(data);
            });
            */

            $.ajax({
                method: 'POST',
                url: "./php/ajax/changeActived.php",
                // .serialize permet de placer dans le post toutes les
                // données des input de la forme
                data:{
                    'id': id,
                    'actived': ad.actived
                },
                dataType: 'json',
                success: function (data) {
                    var msg = '';

                    switch (data.ReturnCode) {
                        case 0: // tout bon
                        break;
                        case 2: // problème de mise a jour
                        case 3: // problème d'insertion'
                            msg = data.Message;
                            break;
                        default:
                            msg = data.Message;
                    }
                    if (msg.length > 0) {
                        console.log(msg);
                    }
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });


        }

        function getPreferences(){
            var days = $('#dayNumber').val();
            var flag = $('#flagHoliday').val();
            updatePreference(flag, days);
        }

        function updatePreference(flag, days){
            if(days > 150){
                displayMessageById('message', 'Valeure de jours trop grande');
                showElementById("message", 5);

            }else{
            $.post('./php/ajax/updateHolidays.php', {
                days: days,
                flag: flag
            }).done(function (data) {
                console.log(data);
            });
                }
            }
    </script>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
    integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
    integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous">
</script>

</html>