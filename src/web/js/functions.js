/**
 * Effectue un appel ajax et exécute une fonction en cas de succès
 * @param string url        Le chemin vers le fichier qui va récupérer nos données
 * @param string callback   Le nom de la fonction à exécuter après avoir reçu les données. Il ne faut pas mettre de parenthèses
 * @param object params     Object qui contient tout les paramètres à envoyer
 * @author GENGA Dario      dario.gng@eduge.ch
 */
function get_data(url, callback, params = {}) {
    // Copier l'objet params dans une variable locale
    // qui sera simplement utilisées pour l'appel Ajax
    var dp = params;
    // On utilise le paramètre de la fonction qui est stocké sur le stack
    // pour créer un tableau qui contient les paramètres additionnels qui se
    // trouvent après params.
    // Si on stocke ces paramètres dans un tableau créé dans cette function,
    // il sera détruit après l'appel Ajax et on aura plus rien lorsqu'on sera
    // rappelé de manière asynchrone.
    params = Array();
    for (var i = 3; i < arguments.length; i++){
        params.push(arguments[i]);
    }
    $.ajax({
        method: 'POST',
        url: url,
        data: dp,
        dataType: 'json',
        success: function (data) {
            var msg = '';
 
            switch (data.ReturnCode){
                case 0 : // tout bon
                    // On récupère par params, notre tableau des arguments.
                    params.unshift(data.Data);
                    callback.apply(this, params);
                    break;
                case 2 : // problème récup données
                case 3 : // problème encodage sur serveur
                default:
                    msg = data.Message;
                break;
            }
        },
        error: function(jqXHR){
            console.log(jqXHR);
        }
    });
 
}


/**
 * Display message
 * @param {tr.fn.init} el Tableau d'élément retourné par $('')
 * @param {string} msg Le message à intégrer
 * @param {int} tmo Timeout en seconds. Default 5.
 */
function displayMessage(el, msg, tmo = 5){
    el.html(msg);
    setTimeout(function(e){
        e.html("");
    }, // Fonction à appeler après le timeout
    tmo*1000, // Le nombre de milisecondes
    el); // Les paramètre passé à la fonction
}


/**
 * Display message en utilisant l'id de l'élément
 * @param {string} id L'id de l'élément 
 * @param {string} msg Le message à intégrer
 * @param {int} tmo Timeout en seconds. Default 5.
 */
function displayMessageById(id, msg, tmo = 5) {
    displayMessage($('#'+id), msg, tmo);
}

/**
 * Display message en utilisant la classe des éléments
 * @param {string} cl Classe des éléments
 * @param {string} msg Le message à intégrer
 * @param {int} tmo Timeout en seconds. Default 5.
 */
function displayMessageByClass(cl, msg, tmo = 5) {
    displayMessage($('.' + cl), msg, tmo);
}



/**
 * Montrer un élément html
 * @param {tr.fn.init} el Tableau d'élément retourné par $('')
 * @param {int} tmo Timeout en seconds. Default 5.
 */
function showElement(el, tmo = 5) {
    el.show();
    setTimeout(function (e) {
            e.hide();
        }, // Fonction à appeler après le timeout
        tmo * 1000, // Le nombre de milisecondes
        el); // Les paramètre passé à la fonction
}


/**
 * Montrer un élément en utilisant sont id
 * @param {string} id L'id de l'élément 
 * @param {int} tmo Timeout en seconds. Default 5.
 */
function showElementById(id, tmo = 5) {
    showElement($('#' + id), tmo);
    
}

/**
 * Montrer un élément en utilisant sont id
 * @param {string} id L'id de l'élément 
 * @param {int} tmo Timeout en seconds. Default 5.
 */
function showElementByClass(cl, tmo = 5) {
    showElement($('.' + cl), tmo);

}

/**
 * L'email est-il valide
 * @param {string} email l'email à valider
 * @returns bool True si l'email est valide autrement False.
 */
function validateEmail(email) {
    return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+([^<>()\.,;:\s@\"]{2,}|[\d\.]+))$/.test(email);
  }