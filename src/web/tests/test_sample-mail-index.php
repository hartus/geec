<!DOCTYPE html>
<?php
/**
 * @remark Mettre le bon chemin d'accès à votre fichier contenant les constantes
 */


include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/check_session.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/config/email.param.php';
// Inclure le fichier swift_required.php localisé dans le répertoire swiftmailer5
require_once $_SERVER['DOCUMENT_ROOT'].'/swiftmailer5/lib/swift_required.php';
?>
<html>
<!-- Cet exemple démontre comment envoyer un email avec SwiftMailer 5

    Le fichier contenant les paramètres d'envoi d'email doit être 
    mis à jour avec les données du serveur et du compte utilisé
    pour envoyer des emails.

    Il faut impérativement activer ssl dans le fichier php.ini
    Retrouver la ligne 
    ;extension=openssl
    et décommenter
    extension=openssl

    Redémarrer le serveur apache
-->     
<head>
<meta charset="utf-8">
<title>Mailing - Sample</title>
</head>
<body>
<?php
$email = ESession::getEmail();
$idAd = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING,FILTER_NULL_ON_FAILURE);

$userAccount = EUserHelper::GetUserByEmail($email);


$name = $userAccount->name;
$edumail = $userAccount->email;
$privateEmail = $userAccount->privatemail;
$privatePhone = $userAccount->privatephone;


$ads = EAdsHelper::getAdById($idAd); // $idAd va prendre en GET l'ID de l'ads pour récupérer les données après

//$message = sendEMail("theo.hrlmn@eduge.ch", "Voici mon message", $body);
$body = sendEmailChangedActivated($ads);
echo $body;

?>
</body>
</html>