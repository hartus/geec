<?php

/**
 * Ce fichier sert à tester les fonctions liés aux utilisateurs
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

/*************************************************************************************************************/
/**
 * On va charger la liste des utilisateurs.
 */
$email = ESession::getEmail();
$arr = EUserHelper::GetAllUsers();
/*************************************************************************************************************/
if ($arr !== FALSE){
    echo 'Affichage de la liste des utilisateurs';
    echo '<pre>';
    var_dump($arr); // Affichage des données du tableau des utilsateurs
    echo '</pre>';
}
else{
    echo 'EUserHelper::GetAllUsers() ERREUR';
}

/**
 * On va charger un utilisateur en fonction de l'email.
 */
$u = EUserHelper::GetUserByEmail("sebastien.cthbr@eduge.ch");
/*************************************************************************************************************/
if ($u !== FALSE){
    echo 'Affichage de la liste des utilisateurs';
    echo '<pre>';
    var_dump($u); // Affichage des données du tableau des utilsateurs
    echo '</pre>';
}
else{
    echo 'EUserHelper::GetUserByEmail() ERREUR';
}


/**
 * Mise a jour des informations de l'utilisateur
 */
$user = new EUser();
$user->email = $email;
$user->privateemail = "email@email";
$user->privatephone = "000";
$user->privatemaildefault = 1;
$user->privatephonedefault = 1;



if (EUserHelper::updateUser($user) === false){
    echo '!ok';
    exit;
}

echo 'ok';
exit;

?>