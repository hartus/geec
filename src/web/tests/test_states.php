<?php

/**
 * Ce fichier sert à tester les fonctions de states
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

/*************************************************************************************************************/
/**
 * On va tester ce que nous retourne le tableau 'arr' de la fonction 'GetStateCodes'
 */
$arr = GetStateCodes();
echo 'Affichage de la liste des états';
echo '<pre>';
var_dump($arr); // Affichage des données du tableau qui sont dans la fonction 'GetStateCodes' (ID,NAME)
echo '</pre>';
/*************************************************************************************************************/

?>