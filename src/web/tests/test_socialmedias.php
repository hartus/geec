<?php

/**
 * Ce fichier sert à tester les fonctions liés aux réseaux sociaux des utilisateurs
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/php/includes/incAll/inc.all.php';

/*************************************************************************************************************/
/**
 * On va charger la liste des utilisateurs.
 */
$arr = EUserHelper::GetAllUsers();
/*************************************************************************************************************/
    echo 'Affichage des utilisateurs avec leurs réseaux sociaux correspondant';
    foreach($arr as $usr){
        $scs = $usr->GetSocialMedias();
        if ($scs === false)
            echo 'Error GetSocialMedias() for user <'.$usr->email.'>';
        else{
            echo '<pre>';
            var_dump($scs);
            echo '</pre>';
        }
    }

?>