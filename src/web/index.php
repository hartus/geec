<?php
include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/check_session.php';
    
$varCategories = ECategoriesHelper::GetCategories();
$varStates = GetStates();

$email = ESession::getEmail();

$userAccount = EUserHelper::GetUserByEmail($email);
// The user does not exist
if ($userAccount === false)
{
    header("Location: ./connect.html");
    exit();
}
$name = $userAccount->name;
$edumail = $userAccount->email;
$privateEmail = $userAccount->privatemail;
$privatePhone = $userAccount->privatephone;

$adsarr = EAdsHelper::getAllAds(4, ADS_STATUS_ACTIVATED);

$test = EAdsHelper::getUserInfos();
$p = ReadPreferences();
// si 1 on est en vacances et on ne met pas a jour les annonces, sinon on peut les mettre a jour automatiquement
if($p->FlagHoliday == 0)
    EAdsHelper::CheckAdsActivations();

if($userAccount->bann == 1)
    {
        header("Location: ./banned.php");
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- ============= LINKS ============= -->
    <link rel="stylesheet" href="./css/uikit.min.css">
    
    <link rel="stylesheet" href="../css/style.css">
    <title>Bourse aux livres</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript" src="./js/eelauth.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
        crossorigin="anonymous">

    <!-- ============= CUSTOM ============= -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- ============= END ============= -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">


</head>

<body style="background-color:transparent;">
    <style>
    .background-image {
    position: absolute;
    background-image: url(../img/test.png);
    background-repeat: no-repeat;
    background-size: cover;
    height: 980px;
    width: 45%;
    top: 0;
    right: 0;
    z-index: -1;
    }
    </style>
    <?php include './php/includes/styles/header.php'; ?>

    <div class="background-image">
        <img src="./img/delimiter3.png" alt="">
    </div>

    <!--Les annonces-->
    <div class="container-fluid" style="margin:auto;text-align:center;margin-top:120px !important;">

        <div style="margin: auto;text-align:left;">
            <h1 class="display-5">L'équipe GEEC vous souhaite la bienvenue !</h1>
            <p class="lead">Nouvelle plateforme d'échanges entre élèves en formation professionnelle !</p>
        </div>

        <div class="row" id="consultation">
            <?php if ($adsarr !== false): ?>
                <?php foreach ($adsarr as $ads): ?>
                <div class="col-md-4 col-lg-3 mt-5 pl-5 pr-5">
                    <div onclick="window.location.replace('post_page.php?id=<?php echo($ads->Id) ?>')" class="card shadow-lg uk-animation-slide-bottom-small" style="width: 100%;border:none;border-radius:20px;height:550px;">
<?php
    $imageString = $ads->rawImage;
    if (strlen($ads->rawImage) == 0)
        $imageString = "../img/default-img-ads.jpg";
?>        
                        <img src="<?php echo $imageString; ?>" class="card-img-top" alt="..." style="border-radius:20px 20px 0px 0px;height:200px;object-fit:cover;">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col text-left">
                                        <h5 class="card-title">
                                        <?php 
                                            echo '<p class="mb-2 text-left"><b>'.$ads->Name.'</b></p>';
                                        ?>
                                        </h5>
                                    </div>
                                    <div class="col text-right">
                                        <h5 class="card-title"><?php $date = new DateTime($ads->publicationDate); echo $date->format('d.m.Y'); ?></h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col text-left" style="font-size:20pt;">
                                        <span><?php
                                            if ($ads->Price == 0) {
                                                echo "Gratuit";
                                            }
                                            else {
                                                echo "CHF ".$ads->Price;
                                            }
                                        ?></span>
                                    </div>
                                </div>
                                <p class="card-text" style="max-height: 96px; text-align: left; overflow-y: auto">
                                    <?= limitString($ads->Description, 150) ?>
                                </p>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 user-img">
                                        <?php foreach ($test as $item): ?>
                                            <?php if ($ads->Email == $item['EDU_MAIL']): ?>
                                                <img style="object-fit: contain;border-radius:30px 30px 30px 30px;" width="60rem;" src="<?=$item['URL_PROFIL_PIC'] != '' ? $item['URL_PROFIL_PIC'] : '' ?>">
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                    
                                    <div class="col-md-9 col-sm-9 text-left pt-1">
                                        <?php foreach ($test as $item): ?>
                                            <?php if ($ads->Email == $item['EDU_MAIL']): ?> <!-- SI l'email de l'annonce et égal à l'email dans l'array -->
                                                <?= $item['EDU_NAME'] // Nom de l'utilisateur ?>
                                                <br>
                                                <?= $ads->Email; // Mail de l'utilisateur ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <?php include './php/includes/styles/footer.php'; ?>
    <!--CDN POUR LE JQUERY-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>
</body>

</html>