<?php
/**
 * @description : Page permettant de gérer le statut des utilisateurs
 * @version : 1.0.0
 * @since : 05.04.19
 * @author : Bytyci Qendrim
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */

include $_SERVER['DOCUMENT_ROOT'] . '/php/includes/styles/check_session.php';

$varCategories = ECategoriesHelper::GetCategories();
$varStates = GetStates();

$email = ESession::getEmail();

$userAccount = EUserHelper::GetUserByEmail($email);
$name = $userAccount->name;
$edumail = $userAccount->email;
$privateEmail = $userAccount->privatemail;
$privatePhone = $userAccount->privatephone;

$roleUser = $userAccount->rolescode;

if($roleUser == ERL_STUDENT)
{
    header("Location: ./index.php");
}

// Les utilisateurs
$users = EUserHelper::GetAllUsers();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- ============= LINKS ============= -->
    <link rel="stylesheet" href="./css/uikit.min.css">

    <link rel="stylesheet" href="../css/style.css">
    <title>Bourse aux livres</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript" src="./js/eelauth.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <!-- ============= CUSTOM ============= -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- ============= END ============= -->

</head>

<body style="background-color:transparent;">
<style>
    .background-image {
    position: absolute;
    background-image: url(../img/test.png);
    background-repeat: no-repeat;
    background-size: cover;
    height: 980px;
    width: 45%;
    top: 0;
    right: 0;
    z-index: -1;
    }
    </style>
    <?php include './php/includes/styles/header.php'; ?>
    <div class="background-image">
        <img src="./img/delimiter3.png" alt="">
    </div>
    <div class="container-fluid" style="width: 70rem;margin: auto;margin-top:100px !important;">
        <!-- Contenu du tableau -->
        <div class="card mb-4 mt-4 shadow-lg uk-animation-slide-bottom-small" style="width: 55rem;margin: auto;border:none;border-radius:20px;">
            <table class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" style="border:none;">
                <thead>
                    <tr>
                        <th class="th-sm text-center pt-2 pb-2" style="border-left:none;border-top:none;">IMG</th>
                        <th class="th-sm pt-2 pb-2" style="border-top:none;">Nom</th>
                        <th class="th-sm pt-2 pb-2" style="border-top:none;">Adresse mail</th>
                        <th class="th-sm text-center pt-2 pb-2" style="border-top:none;">Actions</th>
                        <th class="th-sm text-center pt-2 pb-2" style="border-right:none;border-top:none;">Statut</th>
                    </tr>
                </thead>
                <tbody id="tableContent">
                    <!-- Parcourt des tous les utilisateurs de la base de donnée-->
                    <?php $i = 0 ?>
                    <?php foreach ($users as $key => $item) : ?>
                        <tr>
                            <td class="text-center p-0" style="border-left:none;">
                                <img style="object-fit: contain;margin-top:12px;border-radius:25px 25px 25px 25px;" id="adImage" height="50" width="50" src="<?=$item->imageurl != '' ? $item->imageurl : '' ?>">
                            </td>
                            <td>
                                <div style="padding:20px 0;">
                                    <?= $item->name ?>
                                </div>
                            </td>
                            <td>
                                <div style="padding:20px 0;">
                                    <?= $item->email ?>
                                </div>
                            </td>
                            <td>
                                <!-- Bouton permettant de banir / débannir des utilisateurs  -->
                                <div class="custom-control custom-switch w-100 text-center">
                                    <div style="padding:20px 0;padding-left:5px;">
                                        <input type="checkbox" class="custom-control-input" name="bannSwitch"
                                            id="bannSwitch<?= $i ?>" <?php if($item->bann == 1)
                                        echo("checked");
                                        else
                                        echo("");
                                        ?> onclick="banUser('<?= $item->email ?>')">
                                        <label class="custom-control-label" for="bannSwitch<?= $i ?>"></label>
                                    </div>
                                </div>
                            </td>
                            <!-- Statut de l'utilisateur permettant de savoir si un utilisateur est actif ou banni -->
                            <td style="font-weight:bold;text-align:center;padding:25px 0;border-right:none;" id="<?= $item->email ?>Bann">
                                <?php if ($item->bann == 1) : ?>
                                    <span style="color:red;">
                                        Banni
                                    </span>
                                <?php elseif ($item->bann == 0) : ?>
                                    <span style="color:green;">
                                        Actif
                                    </span>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php $i++ ?>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php include './php/includes/styles/footer.php'; ?>
    <!--CDN POUR LE JQUERY-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script>
        var users = <?php echo json_encode($users); ?>;

        /**
         * Bannissement d'un utilisateur (true / false en BDD)
         * @return void
         */
        function banUser(email) {
            var el = document.getElementById(email + 'Bann');

            var user = users.find(u => {
                return u.email === email;
            });

            user.bann = user.bann == 0 ? 1 : 0;

            // Si le statu est 0, l'utilisateur est actif, sinon il est banni
            if (user.bann == 0) {
                el.innerHTML = `<span style="color:green;">Actif</span>`;
            } else if (user.bann == 1) {
                el.innerHTML = `<span style="color:red;">Banni</span>`;
            }

            // Appel ajax pour récupérer les données de l'utilisateur correspondant
            $.post('./php/ajax/modifyUserStatus.php', {
                email: email,
                bann: user.bann
            }).done(function(data) {
                console.log(data);
            });
        } 
    </script>
</body>

</html>