<?php
include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/check_session.php';
/**
 * @brief : Page de création d'annonce
 * @version : 1.0.0
 * @since : 01.02.19
 * @author : Bytyçi
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */

$varCategories = ECategoriesHelper::GetCategories();
$varStates = GetStates();

$email = ESession::getEmail();

$userAccount = EUserHelper::GetUserByEmail($email);
if ($userAccount === false)
{
    header("Location: ./error.php");
    exit();
}

if($userAccount->bann == 1)
{
    header("Location: ./banned.php");
    exit();
}


$name = $userAccount->name;
$edumail = $userAccount->email;
$privateEmail = $userAccount->privatemail;
$privatePhone = $userAccount->privatephone;

$socialMedias = GetSocialmedias();
$adSocialMedias = array();

// Load the social medias of the user
// stored within the user profile
$userSocialMedias = array();
foreach($userAccount->socialmedias as $sc){
        // Création des objets 'EAdsRSValue' avec les données provenant de la base de données
        $s = new EAdsRSValue(-1, $sc->code, $sc->default);
        array_push($userSocialMedias,$s);

}

// Vars par défaut quand on a pas d'annonce
$currentId =  0;
$pageTitle = "Création d'une annonce";
$buttonTitle = "Créer";
$infoMessage = "Votre annonce a été ajoutée";

/* @brief vars utilisée dans le formulaire pour afficher les valeurs d'une annonce */
$adCatID = -1;
$adName = "";
$adPrice = "";
$adImage = "";
$adState = -1;
$adDesc = "";
$adPrivateEmail = $userAccount->privatemaildefault;
$adPrivatePhone = $userAccount->privatephonedefault;
$adCriteriasValues = array();
$imageNA = false;

// On récupère un ID en paramètre dans l'URL
$strCurrentId = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING,FILTER_NULL_ON_FAILURE);

// On test si on a un id et qu'on passe en modification
if ($strCurrentId !== false && strlen($strCurrentId) > 0)
{
    $currentId = intval($strCurrentId);
}

// Modification?
if ($currentId > 0)
{
    $a = EAdsHelper::getAdById($currentId, -1);
    if ($a !== FALSE){
        $pageTitle = "Modification d'une annonce";
        $buttonTitle = "Sauvegarder";
        $infoMessage = "Votre annonce a été modifiée";

        $adName = $a->Name;
        $adCatID = $a->Categorie;
        $adPrice = $a->Price;
        $adImage = $a->rawImage;
        if (strlen($adImage) == 0)
            $imageNA = true;
        $adState = $a->state;
        $adDesc = $a->Description;
        $adPrivateEmail = $a->showPrivateEmail;
        $adPrivatePhone = $a->showPrivatePhone;

        // We have to check if the social medias defined within the user's profile
        // match the social media values array of the ADS
        // The problem is when the user adds or removes social media, the ADS has 
        // to synchronize the social medias
        $adSocialMedias = EAdsHelper::checkSocialMediaADS($a->socialmediasValues, $userSocialMedias);
        $adCriteriasValues = $a->criteriasValues;     
    }
}
else{
    $adSocialMedias = $userSocialMedias;
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- ============= LINKS ============= -->
    <link rel="stylesheet" href="./css/uikit.min.css">

    <link rel="stylesheet" href="../css/style.css">
    <title>Bourse aux livres</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript" src="./js/eelauth.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
        crossorigin="anonymous">
    <script src="./js/functions.js"></script>
    <script src="./js/inc.all.constants.js"></script>

    <!-- ============= CUSTOM ============= -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- ============= END ============= -->

</head>

<!-- Fonction permettant de charger les critères propre à une catégorie -->

<body style="background-color:transparent;">
<style>
    .background-image {
    position: absolute;
    background-image: url(../img/test.png);
    background-repeat: no-repeat;
    background-size: cover;
    height: 980px;
    width: 45%;
    top: 0;
    right: 0;
    z-index: -1;
    }
    </style>
    <!--HEADER-->
    <?php include './php/includes/styles/header.php'; ?>
    <div class="background-image">
        <img src="./img/delimiter3.png" alt="">
    </div>
    <form action="#" name="AdsForm" method="POST">
        <input type="hidden" name="inputCount" id="inputCount" value="" />
        <input type="hidden" name="imageSrc" id="imageSrc" value="" />
        <input type="hidden" name="adsID" value="<?php echo $currentId;?>" />
        <div class="container-fluid" id="AllElementFromAdsForm" style="margin-top:100px !important;">
            <!--PAGE DE CRÉATION D'ANNONCE-->
            <div class="col-md-8 offset-md-2">
                <div class="card mb-4 mt-4 shadow-lg uk-animation-slide-bottom-small p-1" style="margin: auto;border:none;border-radius:20px;">
                    <div class="card-body">
                            <div class="row p-1">
                                <div class="col-md-6">
                                    <label>Nom de l'article<span style="color: red;"> *</span></label>
                                    <input class="float-right" type="text" id="articleName" name="articleName" alt="articlename" placeholder="Nom de l'article"  value="<?php echo $adName; ?>">
                                    <br><label>Catégorie<span style="color: red;"> *</span></label>
                                    <!-- Div qui va prendre la fonction 'selectCategorie' qui va parcourir les données récupérés à partir de la base de donnée -->
                                    <select class="float-right" id="categorieSelect" name="categorieSelect" onchange="selectCategorie()">
                                        <!-- On parcourt $varCategories qui contient les données de la fonction 'GetCategories' -->
                                        <?php foreach ($varCategories as $key => $value){
                                        $sel = ($adCatID == $value->id) ? "selected" : "";
                                        echo '<option value="'.$value->id.'" '.$sel.'>'.$value->name.'</option>';
                                        }
                                        ?>
                                        <!-- FIN -->
                                    </select>
                                    <br><label>Prix<span style="color: red;"> *</span> (CHF)</label>
                                    <input class="float-right" maxlength="3" type="number" min="1" max="500" pattern="[0-9]*" id="articlePrice" name="articlePrice" alt="articleprice"  onkeypress='return restrictAlphabets(event)' placeholder="Prix" value="<?php echo $adPrice; ?>">
                                    <br>
                                    <span>
                                        <div style="display: inline-flex;"><label>Image</label>
                                            <div class="custom-control custom-switch" style="padding-left: 50px;">
                                                <input type="checkbox" class="custom-control-input" id="imageNA" name="imageNA"
                                                <?php echo ($imageNA == true) ? "checked" : "";?>>
                                            <label class="custom-control-label" for="imageNA">N/A</label>
                                            </div>
                                        </div>
                                    </span>
                                    <div id="ImageDiv">
                                    <span style="color: red;"> *</span>
                                    <input class="float-right" type="file" id="picUpload" onchange="onFileSelected(event)">
                                    <br><div class="w-100 text-center"><img class="mt-2 mb-1 w-75" id="adImage" height="400" width="400" style="object-fit: contain;" src="<?=$adImage != '' ? $adImage : '../img/default-img-ads.jpg' ?>"></div>
                                    </div>
                                    <div id="NoImageDiv">
                                    <br><div class="w-100 text-center"><img class="mt-2 mb-1 w-75" height="400" width="400" style="object-fit: contain;" src="../img/default-img-ads.jpg"></div>
                                    </div>
                                    <br><label>État<span style="color: red;"> *</span></label>
                                    <!-- Div qui va prendre la fonction 'selectState' qui va parcourir les données récupérés à partir de la base de donnée -->
                                    <select class="float-right" id="selectState" name="selectState">
                                        <!-- On parcourt $varStates qui contient les données de la fonction 'GetStates' -->
                                        <?php 
                                        foreach ($varStates as $key => $value){
                                        $sel = ($adState == $value->code) ? "selected" : "";
                                        echo '<option value="'.$value->code.'" '.$sel.'>'.$value->name.'</option>';
                                        }
                                        ?>
                                        <!-- FIN -->
                                    </select>
                                </div>
                                <div class="col-md-6">
                                <div class="row">
                                <?php
                                if($privateEmail != ""){?>
                                <div class="col-md-4">Email privé </div>
                                    <div><label class="form-control" style="visibility:hidden;display:none;" value="<?php echo $privateEmail ?>"></div>
                                    <div class="col-md-8">
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" name="showPrivateEmail" id="showPrivateEmail"
                                            <?php echo ($adPrivateEmail == 1) ? "checked" : "";?>>
                                        <label class="custom-control-label" for="showPrivateEmail">Afficher sur l'annonce</label>
                                        </div>
                                </div>
                                <?php } 
                                if($privatePhone != ""){
                                ?>
                                <div class="col-md-4">Téléphone</div>
                                <div><label class="form-control" style="visibility:hidden;display:none;" value="<?php echo $privatePhone ?>"></div>
                                <div class="col-md-8">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" name="showPrivatePhone" id="showPrivatePhone"
                                        <?php echo ($adPrivatePhone == 1) ? "checked" : "";?>>
                                        <label class="custom-control-label" for="showPrivatePhone">Afficher sur l'annonce</label>
                                    </div>

                                </div>

                                <?php } ?>
                                <div class="col-md-12" id="RS">
                                <!--CRITERIAS-->
                                </div>
                            </div>
                                </div>
                            </div>
                        <div class="row p-1" id="criterias">
                        </div>
                        <div class="row p-1">
                            <div class="col-md-12">
                                <label>Description </label>
                                <textarea id="description" style="width:100%;height:150px;" name="description" placeholder="..."><?php echo $adDesc; ?></textarea>
                            </div>
                        </div>
                        <div class="row p-1">
                            <div class="col-md-12">
                                <div style="margin:auto;text-align:center;">
                                    <a href="index.php" class="btn btn-primary mr-1" id="createAds" alt="<?php echo $pageTitle;?>"><?php echo $buttonTitle; ?></a>
                                    <a href="index.php" class="btn btn-dark" id="cancel" alt="cancel" value="Annuler">Annuler</a>
                                </div>
                            </div>
                            <div class="col-md-12 text-left" id="infoMessage"></div>
                            <div style="display:none;" class="alert col-md-5 alert-success mx-auto text-center mt-4" id="adsAdded"><?= $infoMessage ?></div>
                        </div>
                        <div style="display:none;" class="alert col-md-5 alert-success mx-auto text-center mt-4" id="adsAdded"><?= $infoMessage ?></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--FOOTER-->
    <?php include './php/includes/styles/footer.php'; ?>
    <!-- SCRIPT POUR LES FONCTIONS -->

    <!-- SCRIPT POUR AVOIR QUE DES NUMEROS SUR LES CHAMPS -->
    <script type="text/javascript">
	function restrictAlphabets(e){
		var x=e.which||e.keycode;
		if((x>=48 && x<=57) || x==8 || (x>=35 && x<=40)|| x==46)
			return true;
		else
			return false;
	    }
    </script>

    <!--SCRIPT LOAD CRITERIAS-->
    <script>
        var lastIdRs = 0;
        var listIM = <?php echo json_encode($socialMedias);?>;
        var socialMediaValues = <?php echo json_encode($adSocialMedias);?>;
        var criteriaValues = <?php echo json_encode($adCriteriasValues);?>;
        // Lorsque le document est chargé
        $(document).ready(function () {
            selectFunctions();
            initializeRS();
            $("#createAds").click(createAds);
            $("#imageNA").click(imageNAChanged);
<?php
            if ($imageNA)            
                echo "$('#ImageDiv').hide();";
            else
                echo "$('#NoImageDiv').hide();";
?>            
        });
        
        /**
         * Quand le state du check box pour l'image non-disponible change
         * @return void
         */
        function imageNAChanged(event) {
            // Show - Hide the right div
            if ($(this).is(':checked')){
                $('#NoImageDiv').show();
                $('#ImageDiv').hide();
            }else{
                $('#NoImageDiv').hide();
                $('#ImageDiv').show();
            }
        }

        /**
         * Création d'une annonce pour l'utilisateur les valeurs du profile
         * @return void
         */
        function createAds(event) {

            event.preventDefault();

            // Contrôler la validité des champs
            if (checkValues()) {
                var inputCount = $('#RS').find('input[type=checkbox]').length;
                $('#inputCount').val(inputCount);
                // Est-ce que le checkbox pour pas d'image est coché
                if ($('#imageNA').is(':checked') == false){
                    var valueImage = $('#adImage').attr('src');
                    $('#imageSrc').val(valueImage);
                }


                $.ajax({
                    method: 'POST',
                    url: "./php/ajax/addAds.php",
                    // .serialize permet de placer dans le post toutes les
                    // données des input de la forme
                    data: $("form[name='AdsForm']").serialize(),
                    dataType: 'json',
                    success: function (data) {
                        var msg = '';

                        switch (data.ReturnCode) {
                            case 0: // tout bon
                            showElementById("adsAdded", 3);
                            // Fonction à appeler après le timeout
                            setTimeout(function(e){
                                window.location = "./index.php";
                            },
                            1000);// Le nombre de milisecondes
                            break;
                            case 1: // pas d'image insérée
                                msg = data.Message
                            case 2: // problème de mise a jour
                            case 3: // problème d'insertion'
                                break;
                            case 4: // problème d'envoi d'email
                                msg = data.Message;
                                break;
                            case 5: // problème d'envoi d'email a l'admin
                                msg = data.Message;
                                break;
                            default:
                                msg = data.Message;
                        }
                        if (msg.length > 0) {
                            console.log(msg);
                            displayMessageById("infoMessage", msg, 5);
                        }
                    },
                    error: function (jqXHR) {
                        console.log(jqXHR);
                    }
                });
            }
        }

        /**
         * Insérer dans le html les réseaux sociaux de l'utilisateur
         * @return void
         */
        function initializeRS() {
            for (let i = 0; i < socialMediaValues.length; i++) {
                addLineRS(socialMediaValues[i].code, socialMediaValues[i].default);
            }
        }
        /**
         * Insérer le réseau social
         */
        function addLineRS(rsSelect, check) {
            lastIdRs++;
            // Créé la ligne pour un nouveau RS et fait les "col-xx-xx"
            let container = document.createElement('div');
            container.className = 'col-md-12 pl-0 pr-0';
            container.setAttribute('id', 'rs' + lastIdRs);

            // Créé la div servant aux colones
            let row = document.createElement('div');
            row.className = 'row';

            // Créé la div contenant la checkbox
            let CheckboxArround = document.createElement('div');
            CheckboxArround.className = "col-md-8";

            // Créé la div contenant la checkbox
            let divCheckbox = document.createElement('div');
            divCheckbox.className = "custom-control custom-switch";

            // Créé la checkbox
            let checkbox = document.createElement('input');
            checkbox.id = 'customSwitch' + lastIdRs;
            checkbox.className = 'custom-control-input';
            checkbox.setAttribute('name', 'rsShow' + lastIdRs);
            checkbox.setAttribute('type', 'checkbox');
            if (check)
                checkbox.setAttribute('checked', 'checked');
            // Append
            divCheckbox.appendChild(checkbox);
            
            // Créé le label pour la checkbox
            let checkLabel = document.createElement('label');
            let textLabel = document.createTextNode("Afficher sur l'annonce");

            checkLabel.className = "custom-control-label";
            checkLabel.setAttribute("for", "customSwitch" + lastIdRs);

            
            checkLabel.appendChild(textLabel);
            divCheckbox.appendChild(checkLabel);

            // Créé le réseau social
            let rsCode = document.createElement('input');
            let rsLabel = document.createElement('div');
            rsLabel.className = "col-md-4";
            rsLabel.style = "";
            for (let i = 0; i < listIM.length; i++) {
                if (listIM[i].code == rsSelect) {
                    rsLabel.innerHTML = listIM[i].name;
                    rsCode.setAttribute('name', 'rsCode' + lastIdRs);
                    rsCode.setAttribute('type', 'hidden');
                    rsCode.setAttribute('value', listIM[i].code);
                    break;
                }
            }
            row.appendChild(rsLabel);
            row.appendChild(rsCode);

            // Ajout de la colonne autour du checkbox
            row.appendChild(CheckboxArround);

            // Ajout de la div dans la row
            CheckboxArround.appendChild(divCheckbox);

            container.appendChild(row);

            // Ajout de la ligne dans le document
            $('#RS').append(container);
        }

        /**
         * @brief Cette fonction permet de charger la DATA envoyés par la fonction 'showCriterias' qui permet de charger les catégories avec leurs critères
         * @abstract [function] showCriterias Fonction permettant de charger les critères propre à une catégorie
         * @return void
         */
        function selectCategorie() {
            get_data("./php/ajax/getCategories.php", showCriterias);
        }

        function selectFunctions() {
            selectCategorie();
        }

        function checkValues() {

            var bRetValue = true;
            // NOM DE L'ARTICLE
            // On attribue à des variables les "values" récupéré sur les inputs du formulaire d'insertion d'annonce
            var articleName = $("#articleName").val();
            if (articleName.length == 0) {
                // Montrez le background de l'input en rouge
                $("#articleName").css("background-color", CONST_RED);
                // Mettre le focus sur l'edit
                $("#articleName").focus();
                // Set error
                bRetValue = false;
            } else {
                // Ce champ est ok
                // Supprimer le background de l'input au cas il aurait été affiché en rouge
                $("#articleName").css("background-color", "");
            }

            // PRIX DE L'ARTICLE
            // On attribue à des variables les "values" récupéré sur les inputs du formulaire d'insertion d'annonce
            var articlePrice = $("#articlePrice").val();
            if (articlePrice.length == 0) {
                // Montrez le background de l'input en rouge
                $("#articlePrice").css("background-color", CONST_RED);
                // Mettre le focus sur l'edit
                if (bRetValue)
                    $("#articlePrice").focus();
                // Set error
                bRetValue = false;
            } else {
                // Ce champ est ok
                // Supprimer le background de l'input au cas il aurait été affiché en rouge
                $("#articlePrice").css("background-color", "");
            }

            // CRITERIAS 
            $(".criteria").each(function () {
                var criteria = $(this);
                if (criteria.attr("required")){
                    if (criteria.val() == "") {
                        // Montrez le background de l'input en rouge
                        // Mettre le focus sur l'edit
                        if (bRetValue)
                            criteria.focus();
                        // Set error
                        bRetValue = false;
                        criteria.css("background-color", CONST_RED);
                    } else {
                        // Ce champ est ok
                        // Supprimer le background de l'input au cas il aurait été affiché en rouge
                        criteria.css("background-color", "white");
                    }
                }
            });

            return bRetValue;
        }
        
        /**
         * @brief Cette fonction permet de charger les critères propre à une catégorie sur le chargement de la page et sur le changement du select du combobox
         * @param [array] data Données récuperés de l'appel AJAX 
         */
        function showCriterias(data) {
            let content = '';
            let count = 0;
            // On parcourt la data que nous renvoie la fonction 'showCriterias'
            for (let i = 0; i < data.length; i++) {
                // Si l'id de la catégorie correspond à l'id de la catégorie sélectionné.
                if ($('#categorieSelect').val() == data[i].id) {
                    // On charge les critères qui sont propre à la catégorie correspondante (à partir du tableau associatif 'categories_criterias')
                    for (let j = 0; j < data[i].criterias.length; j++) {
                        var sVal = "";
                        if (criteriaValues.length > 0){
                            // On recher pour voir si c'est le même id pour le critère.
                            criteriaValues.forEach(function(item) {
                                if (data[i].criterias[j].id == item.id)
                                    sVal = item.value;
                            });
                        }
                        // Is the criteria required?
                        if (data[i].criterias[j].name.substring(0, 2) == "**"){
                            content += '<div class="col-md-6"><label title="Ce champ est obligatoire">' + data[i].criterias[j].name.substring(2) +
                                '<span style="color: red;" title="Ce champ est obligatoire"> *</span>' + '</label><input type="text" id="' + data[i].criterias[
                                    j].name + '" class="float-right m-1 criteria" required=true name="crit_' + data[i].criterias[j].id +
                                '" title="' + data[i].criterias[j].name.substring(2) + '" placeholder="Ce champ est obligatoire" value="' + sVal + '"></div>';

                        }
                        else{
                            // On concatène les critères dans le contenue chargé par la catégorie sélectionné
                            content += '<div class="col-md-6"><label title="Ce champ est facultatif">' + data[i].criterias[j].name +
                                '</label><input type="text" id="' + data[i].criterias[
                                    j].name + '" class="float-right m-1 criteria" name="crit_' + data[i].criterias[j].id +
                                '" title="' + data[i].criterias[j].name + '" placeholder="Ce champ est facultatif" value="' + sVal + '"></div>';
                        }
                    }
                    break;
                }//#endif same category
            }
            // On mets à jours les critères présent dans la div 'criterias'
            $('#criterias').html(content);
        }
    </script>
    <!--SCRIPT POUR LOAD LES IMGS-->
    <script>
        function onFileSelected(event) {
            // Fichier sélectionné
            var selectedFile = event.target.files[0];
            
            // Appel de la fonction qui permet de tester si l'extension du fichier est une image
            if (isImage(selectedFile)) {
                var reader = new FileReader();

                // Récupération de l'image à partir du formulaire
                var imgtag = document.getElementById("adImage");
                imgtag.title = selectedFile.name;

                // Type de l'image sélectionné
                var imgtype = selectedFile.type;

                // Évenement sur le chargement du fichier
                reader.onload = function (event) {
                    imgtag.src = event.target.result;
                };

                // Lecture de l'image
                reader.readAsDataURL(selectedFile);   
            }
            else{
                console.log("Veuillez sélectionner une image");
                // Faire message d'erreur sur le site et non en console
            }                
        }
        
        function getExtension(filename) {
            var parts = filename.split('.');
            return parts[parts.length - 1];
        }

        // Vérification si c'est une image
        function isImage(selectedFile) {
            var ext = getExtension(selectedFile.type);
            console.log(ext);
            switch (ext.toLowerCase()) {
            case 'image/jpg':
            case 'image/jpeg':
            case 'image/png':
                //etc
                return true;
            }
            return false;
        }
    </script>
    <!--CDN POUR LE JQUERY-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>
</body>

</html>