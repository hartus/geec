<?php
include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/check_session.php';
/**
 * @brief : page de visualisation et modification de son propre profile
 * @version : 1.0.0
 * @since : 23.01.19
 * @author : Cuthbert Sébastien
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */

$email = ESession::getEmail();
// todo inculde

$userAccount = EUserHelper::GetUserByEmail($email);
$name = $userAccount->name;
$edumail = $userAccount->email;
$privateEmail = $userAccount->privatemail;
$privatePhone = $userAccount->privatephone;

$userSocialMedia = $userAccount->socialmedias;
$nbVal = count($userSocialMedia);

$socialMedias = GetSocialmedias();

if($userAccount->bann == 1)
{
    header("Location: ./banned.php");
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- ============= LINKS ============= -->
    <link rel="stylesheet" href="./css/uikit.min.css">


    <link rel="stylesheet" href="../css/style.css">
    <script src="./js/functions.js"></script>
    <title>Bourse aux livres</title>
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/6.4.1/js/intlTelInput.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript" src="./js/eelauth.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/6.4.1/css/intlTelInput.css">
    <script src="./js/inc.all.constants.js"></script>

    <!-- ============= CUSTOM ============= -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- ============= END ============= -->
</head>

<body style="background-color:transparent;">
<style>
    .background-image {
    position: absolute;
    background-image: url(../img/test.png);
    background-repeat: no-repeat;
    background-size: cover;
    height: 980px;
    width: 45%;
    top: 0;
    right: 0;
    z-index: -1;
    }
    </style>
<?php include './php/includes/styles/header.php'; ?>
<div class="background-image">
        <img src="./img/delimiter3.png" alt="">
    </div>
    <!--HEADER-->
    <div class="container-fluid" style="margin-top:100px !important;">
        <div id="userInfo" class="mt-4">
            <!--PAGE DE PROFIL-->
            <div class="card mb-4 mt-0 shadow-lg uk-animation-slide-bottom-small" style="width: 55rem;margin: auto;border:none;border-radius:20px;">
                <div class="card-body">
                    <h5 class="card-title mb-0">Modification du profil</h5>
                </div>
            </div>
        </div>
        <!-- Informations personneles affichées -->
        <form action="#" name="userProfileForm">
            <div id="userIM">
                <input type="hidden" name="inputCount" id="inputCount" value="" />
                <input type="hidden" name="email" value="<?php echo $email;?>" />
                <div class="card mt-4 mb-4 shadow-lg uk-animation-slide-bottom-small" style="width: 55rem; margin: auto;border:none;border-radius:20px;">
                    <div class="card-body">
                        <h5 class="card-title">Informations du profil</h5>
                        <p class="card-text">
                        <div class="row mt-4 mb-4">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col">Nom :</div>
                                    <div class="col ml-4"><?php echo $name ?></div>
                                </div>
                                <div clasS="row mt-3">
                                    <div class="col">Email de connexion :</div>
                                    <div class="col ml-4"><?php echo $edumail ?></div>
                                </div>
                            </div>
                        </div>
                    </p>
                        <div class="row">
                            <div class="col-md-3">Email privé : </div>
                            <div class="col-md-5">
                                <input id="privateEmail" name="showPrivateEmail" class="form-control" type="email" value="<?php echo $privateEmail ?>">
                            </div>
                            <div class="col-md-4">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="emailSwitch"
                                        id="emailSwitch" <?php if($userAccount->privatemaildefault == 1)
                                    echo("checked");
                                    else
                                    echo("");
                                    ?>>
                                    <label class="custom-control-label" for="emailSwitch">Afficher sur le profil</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-3">Téléphone :</div>
                            <div class="col-md-5">
                                <input id="privatePhone" name="showPrivatePhone" class="form-control" type="text" placeholder="ex: +41 79 200 32 99"
                                    value="<?php echo $privatePhone ?>">
                            </div>
                            <div class="col-md-4">
                                <!--BUG SUR LE CHECKED DU CHECKBOX POUR LE TÉLÉPHONE-->
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="phoneSwitch"
                                        id="phoneSwitch" <?php if($userAccount->privatephonedefault == 1)
                                    echo("checked");
                                    else
                                    echo("");
                                    ?>>
                                    <label class="custom-control-label" for="phoneSwitch">Afficher sur le profil</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <h5 class="card-title text-left col-md-6 mb-0">Réseaux sociaux</h5>
                        </div>

                        <div class="row" id="RS">
                        </div>
                        <div class="row justify-content-md-center mt-3">
                            <button type="button" class="btn btn-primary mr-2" onclick="btnAddIM()">+</button>
                            <button type="button" class="btn btn-secondary mr-2" id="saveUserProfile">Sauvegarder</button>
                            <button type="button" class="btn btn-secondary" onclick="returToIndex()">Fermer</button>
                        </div>
                        

                        <div class="row">
                            <div class="col-md-12 text-right">
                            </div>
                            <div class="col-md-12 text-left" id="infoMessage"></div>
                        </div>
                    </div>
                </div>
                <div style="display:none;" class="alert col-md-5 alert-success mx-auto text-center" id="profileSaved">
                    Votre profil a été sauvegardé </div>
            </div>
            
        </form>

    </div>
    <!--FOOTER-->
    <?php include './php/includes/styles/footer.php'; ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous">
    </script>
    <script>
        let lastIdRs = 0;
        let listIM = <?php echo json_encode($socialMedias); ?> ;
        let userSocialMedia = <?php echo json_encode($userSocialMedia); ?> ;

        // Lorsque le document est chargé
        $(document).ready(function () {

            initializeRS();
            $("#saveUserProfile").click(saveUserProfile);

            $("#privatePhone").intlTelInput();
        });

        /**
         * Sauvegarder les valeurs du profile
         */
        function saveUserProfile(event) {
            event.preventDefault();
            // Contrôler la validité des champs
            if (checkValues()) {
                var inputCount = $('#RS').find('input[type=text]').length;
                $('#inputCount').val(inputCount);

                $.ajax({
                    method: 'POST',
                    url: "./php/ajax/modifyUser.php",
                    // .serialize permet de placer dans le post toutes les
                    // données des input de la forme
                    data: $("form[name='userProfileForm']").serialize(),
                    dataType: 'json',
                    success: function (data) {
                        var msg = '';

                        switch (data.ReturnCode) {
                            case 0: // tout bon
                                showElementById("profileSaved", 3);
                                break;
                            case 1: // problème de mise à jour
                            case 2: // problème encodage sur serveur
                            default:
                                msg = data.Message;
                                break;
                        }
                        if (msg.length > 0) {
                            displayMessageById("infoMessage", msg, 3);
                        }
                    },
                    error: function (jqXHR) {
                        console.log(jqXHR);
                    }
                });
            }
        }
        /**
         * Tester la validité des champs du formulaire
         * @return bool True si ok, autrement False.
         */
        function checkValues() {
            // Validation du email
            var email = $("#privateEmail").val();
            if (validateEmail(email) == false){
                $("#privateEmail").css("background-color", CONST_RED).focus();
                return false;
            }
            else
                $("#privateEmail").css("background-color", "");
            // Contrôle les valeurs des réseaux sociaux
            var vals = $(".scMediaValue");
            var bRetValue = true;
            vals.each(function (index) {
                var v = $(this).val();
                if (v.length <= 0) {
                    $(this).css("background-color", CONST_RED);
                    if (bRetValue)
                        $(this).focus();
                    bRetValue = false;
                } else
                    $(this).css("background-color", "");
            });
            // Contrôle que chaque réseau social est différents
            vals = $(".scMediaTypeValue");
            var arr = [];
            vals.each(function (index) {
                var v = $(this).val();
                if (arr.indexOf(v) >= 0) {
                    $(this).css("background-color", "#ff000061");
                    bRetValue = false;
                } else {
                    arr.push(v);
                    $(this).css("background-color", "");
                }
            });


            // Done
            return bRetValue;
        }

        function returToIndex() {
            window.location.href = 'index.php';
        }
        /**
         * Insérer dans le html les réseaux sociaux de l'utilisateur
         * @return void
         */
        function initializeRS() {
            for (let i = 0; i < userSocialMedia.length; i++) {
                addLineRS(userSocialMedia[i].value, userSocialMedia[i].code, userSocialMedia[i].default);
            }
        }

        function btnAddIM() {
            addLineRS();
        }

        /**
         * Insérer le réseau social
         */
        function addLineRS(textVal = "", rsSelect = -1, check = false) {
            lastIdRs++;
            // Créé la ligne pour un nouveau RS et fait les "col-xx-xx"
            let container = document.createElement('div');
            container.className = 'col-md-12';
            container.setAttribute('id', 'rs' + lastIdRs);

            // Créé la div servant aux colones
            let row = document.createElement('div');
            row.className = 'row mt-3';

            // Créé la div contenant le select
            let divSelect = document.createElement('div');
            divSelect.className = 'col-md-3';

            // Créé la div contenant l'input text
            let divText = document.createElement('div');
            divText.className = "col-md-4";

            // Créé la div contenant la checkbox
            let divCheckbox = document.createElement('div');
            divCheckbox.className = "col-md-4 custom-control custom-switch";

            // Créé la div contenant le bouton supprimer
            let divBtn = document.createElement('buton');
            divBtn.className = "col-md-1";

            // Créé le boutton "-"
            let btnMinus = document.createElement('button');
            btnMinus.innerHTML = ' - ';
            btnMinus.className = "btn btn-danger";
            btnMinus.setAttribute('onclick', 'deleteButton("rs' + lastIdRs + '")');

            // Créé l'input text
            let inputText = document.createElement('input');
            inputText.setAttribute('type', 'text');
            inputText.setAttribute('value', textVal);
            inputText.className = 'form-control scMediaValue';
            inputText.setAttribute('name', 'rsValue' + lastIdRs);

            // Créé la checkbox
            let checkbox = document.createElement('input');
            checkbox.id = 'customSwitch' + lastIdRs;
            checkbox.className = 'custom-control-input';
            checkbox.setAttribute('name', 'rsShow' + lastIdRs);
            checkbox.setAttribute('type', 'checkbox');
            if (check)
                checkbox.setAttribute('checked', 'checked');

            // Créé le label pour la checkbox
            let checkLabel = document.createElement('label');
            let textLabel = document.createTextNode("Afficher sur le profil");
            checkLabel.className = "custom-control-label";
            checkLabel.setAttribute("for", "customSwitch" + lastIdRs);
            checkLabel.appendChild(textLabel);

            // Créé le dropdown
            let dropdownSelect = document.createElement("select");
            dropdownSelect.className = 'form-control scMediaTypeValue';
            dropdownSelect.setAttribute('name', 'rsSelectValue' + lastIdRs);

            for (let i = 0; i < listIM.length; i++) {
                let option = document.createElement('option');
                option.setAttribute('value', listIM[i].code);

                if (listIM[i].code == rsSelect)
                    option.setAttribute('selected', 'selected');

                option.innerHTML = listIM[i].name;
                dropdownSelect.appendChild(option);
            }

            divSelect.appendChild(dropdownSelect);
            divText.appendChild(inputText);
            divCheckbox.appendChild(checkbox);
            divCheckbox.appendChild(checkLabel);
            divBtn.appendChild(btnMinus);

            // Ajout de la div dans la row
            row.appendChild(divSelect);
            row.appendChild(divText);
            row.appendChild(divCheckbox);
            row.appendChild(divBtn);

            container.appendChild(row);

            // Ajout de la ligne dans le document
            $('#RS').append(container);
        }

        function deleteButton(id) {
            $('#' + id).remove();
        }
    </script>
</body>

</html>