<?php
/**
 * @brief : Page pour présenter l'équipe de développement
 * @version : 1.0.0
 * @since : 07.06.19
 * @author : Bytyçi
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */

include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/check_session.php';
$email = ESession::getEmail();
$userAccount = EUserHelper::GetUserByEmail($email);
$name = $userAccount->name;
$edumail = $userAccount->email;
$id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING,FILTER_NULL_ON_FAILURE);

blockAds($id);

$roleUser = $userAccount->rolescode;

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- ============= LINKS ============= -->
    <link rel="stylesheet" href="./css/uikit.min.css">

    <link rel="stylesheet" href="../css/style.css">
    <title>Bourse aux livres</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript" src="./js/eelauth.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="./js/functions.js"></script>

    <!-- ============= CUSTOM ============= -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- ============= END ============= -->

</head>

<body>
<body style="background-color:transparent;">
    <style>
    .background-image {
    position: absolute;
    background-image: url(../img/test.png);
    background-repeat: no-repeat;
    background-size: cover;
    height: 980px;
    width: 45%;
    top: 0;
    right: 0;
    z-index: -1;
    }
    </style>
    <?php include './php/includes/styles/header.php'; ?>
    <div class="background-image">
        <img src="./img/delimiter3.png" alt="">
    </div>
    <div class="container-fluid mx-auto text-center" style="margin-top:80px !important;">
    <div class="col-md-4 col-lg-3 mt-5 pl-5 pr-5 mx-auto text-center">
    <div class="card mb-4 mt-4 shadow-lg uk-animation-slide-bottom-small mx-auto" style="border:transparent;border-radius:20px;">
        <span class="text-center p-3" style="font-size:18pt;font-weight:bold;">Credit</span>
        <div style="font-size:12pt;">
            <div class="row text-left pl-4">
                <div class="col" style="font-weight:bold;">SCRUM MASTER</div>
                <div class="col">D. AIGROZ</div>
            </div>
            <div class="row text-left pl-4 pt-2 pb-4">
                <div class="col" style="font-weight:bold;">DEV TEAM</div>
                <div class="col">A. GÜNER<br>Q. BYTYCI<br>S. CUTHBERT<br>T. HÜRLIMANN</div>
            </div>
        </div>
    </div>
    </div>
    </div>
</div>
    <!--CDN POUR LE JQUERY-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous">
    </script>
</body>

</html>