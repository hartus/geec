<?php
include $_SERVER['DOCUMENT_ROOT'].'/php/includes/styles/check_session.php';
/**
 * @brief : Page de redirection pour une ads donnée
 * @version : 1.0.0
 * @since : 14.03.19
 * @author : Bytyçi
 * @copyright : Entreprise Ecole CFPT-I © 2019
 */

$email = ESession::getEmail();

$idAd = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING,FILTER_NULL_ON_FAILURE);

$userAccount = EUserHelper::GetUserByEmail($email);


$name = $userAccount->name;
$edumail = $userAccount->email;
$privateEmail = $userAccount->privatemail;
$privatePhone = $userAccount->privatephone;


$ads = EAdsHelper::getAdById($idAd, ADS_STATUS_ALL); // $idAd va prendre en GET l'ID de l'ads pour récupérer les données après

$adsEmail = $ads->Email;
$userSocialMedia = getSocialmediaValue($idAd, $adsEmail); // Fonction permettant de récupérer les valeurs des médias sociaux pour des nom de réseaux sociaux correspondant

$varCategories = ECategoriesHelper::GetCategories();
$varStates = GetStates();

$cats = $varCategories;
$states = $varStates;

// Implémentation en PHP
$Category = "#err#";
foreach ($cats as $item) {
    if ($ads->categorie == $item->id) {
        $Category = $item->name;
    }
}

// Implémentation en PHP
$State = "#err#";
foreach ($states as $item) {
    if ($ads->state == $item->code) {
        $State = $item->name;
    }
}

if($userAccount->bann == 1)
{
    header("Location: ./banned.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- ============= LINKS ============= -->
    <link rel="stylesheet" href="./css/uikit.min.css">

    <title>Bourse aux livres</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript" src="./js/eelauth.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">

    <!-- ============= CUSTOM ============= -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- ============= END ============= -->

    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">-->
</head>
<body style="background-color:transparent;">
    <style>
    .background-image {
    position: absolute;
    background-image: url(../img/test.png);
    background-repeat: no-repeat;
    background-size: cover;
    height: 980px;
    width: 45%;
    top: 0;
    right: 0;
    z-index: -1;
    }
    </style>
<?php include './php/includes/styles/header.php'; ?>
<div class="background-image">
        <img src="./img/delimiter3.png" alt="">
    </div>
<form action="#" method="POST" class="p-5" id="frm">
    <input type="hidden" name="inputCount" id="inputCount" value="" />
    <input type="hidden" name="imageSrc" id="imageSrc" value="" />
    <input type="hidden" name="email" value="<?php echo $email;?>" />
    <div class="row" style="margin-top:60px !important;">
        <div class="col-md-12 offset-md-0 col-lg-8 offset-lg-2" style="padding-left:0px;padding-right:0px">
            <div class="card shadow-lg uk-animation-slide-bottom-small" style="border:none;border-radius:20px;">
                
                <div class="card-header p-4" style="border-radius:20px 20px 0px 0px;">
                    <div class="row">
                        <div class="col-md-7">
                            <h2 class="mb-0" id="ads-title" style="font-size:2.5rem;;position: absolute; transform: translate3D(0%, 25%, 0);"><?= $ads->Name?></h2>
                        </div>
                        <div class="col-md-5 text-right" style="margin-top:4px;">
                            <span id="priceText" style="visibility:hidden;display:none;font-weight: bold; font-size: 2.5rem; color: black; background-color: transparent; padding-right: 15px; padding-top: 15px; border-radius: 35px;">Prix : </span></h2>
                            <span id="price" style="font-weight: bold; font-size: 2.5rem; color: #47aa5b; background-color: rgba(86, 194, 107, .3); padding: 15px; border-radius: 35px;">
                            <?php
                            if ($ads->Price == 0) {
                                echo "Gratuit";
                            }
                            else {
                                echo "CHF ".$ads->Price;
                            }?>
                            </h2>
                        </div>
                    </div>
                </div>

                <div id="ads-info" class="card-body">
                    <div class="row mt-3">
                    <!--COLONNE GAUCHE-->
                    <div class="col-md-6 col-sm-12 text-center mb-3">
                        <img src="<?= $ads->rawImage ?>" style="object-fit: contain;border-radius:20px;max-height:300px;"> 
                    </div>
                    <!--COLONNE DROITE-->
                    <div class="col-md-6 col-sm-12 h-50">
                    <h2 style="margin-bottom:15px !important;font-size:17pt;">Informations</h2>
                    
                    <div class="row">
                            <div class="col"><span class="mt-2" style="font-weight:bold">Catégorie</span></div>
                            <div class="col"><?= $Category?></div>
                    </div>

                    <div class="row">
                            <div class="col"><span class="mt-2" style="font-weight:bold">État</span></div>
                            <div class="col"><?= $State?></div>
                    </div>

                    <?php foreach ($ads->criteriasValues as $criteria){?>
                    <!-- On fait appelle à la fonction qui va prendre le nom du critère par rapport à son id -->
                    <?php $criteriaName = ECategoriesHelper::getCriteriaNameByCode($criteria->id); ?>

                    <div class="row">
                        <div class="col"><span class="mt-2" style="font-weight:bold"><?php echo $criteriaName ?></span></div>
                        <div class="col"><?= $criteria->value?></div>
                    </div>
                    <?php }?>
                        
                    <h2 style="margin-top:10px;margin-bottom:15px !important;font-size:17pt;">Contacter le vendeur</h2>
                    
                    <div class="row">
                            <div class="col"><span class="mt-2" style="font-weight:bold">Email</span></div>
                            <div class="col"><a href="mailto:<?= $ads->Email?>"><?= $ads->Email?></a></div>
                    </div>
                        
                    <!--Si le checked est égal à true, on affiche l'email privé-->
                    <?php if($ads->showPrivateEmail==1) {?> <!-- @todo Régler le problème -->
                        <?php $privateEmail = EUserHelper::GetUserByEmail($adsEmail); ?>
                        <div class="row">
                            <div class="col"><span class="mt-2" style="font-weight:bold">Email privé</span></div>
                            <div class="col"><?= $privateEmail->privatemail ?></div>
                        </div>
                    <?php } ?>


                    <!--Si le checked est égal à true, on affiche l'email privé -->
                    <?php if($ads->showPrivatePhone==1) {?>
                        <?php $privatePhone = EUserHelper::GetUserByEmail($adsEmail); ?>
                        <div class="row">
                            <div class="col"><span class="mt-2" style="font-weight:bold">Téléphone</span></div>
                            <div class="col"><?= $privatePhone->privatephone ?></div>
                        </div>
                    <?php } ?>
                    
                    <?php foreach ($ads->socialmediasValues as $socialmediavalue){ /** @var $socialmedias ESocialMediaValue */ ?>
                        <!-- Affichage des réseaux sociaux si le default est checked -->
                        <?php if ($socialmediavalue->default) { ?>
                            <!-- On fait appelle à la fonction qui va prendre le nom du réseau social par rapport au code -->
                            <?php $socialMediaName = getSocialmediaNameByCode($socialmediavalue->code);?>
                                <div class="row">
                                    <div class="col"><span class="mt-2" style="font-weight:bold"><?= $socialMediaName ?></span></div>
                                    <div class="col"><?= $userSocialMedia[$socialMediaName] ?></div>
                                </div>
                        <?php } ?>
                    <?php }?>

                    </div>
                </div>
                <div class="row pb-3 pl-3 pr-3 pt-3">
                    <span class="mb-2" style="font-weight:bold">Description</span>
                    <div class="container-fluid p-0"><?= $ads->Description?></div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>
</body>
</html>