CREATE DATABASE  IF NOT EXISTS `geec` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `geec`;
-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: geec
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ads`
--

DROP TABLE IF EXISTS `ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ads` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERS_EDU_MAIL` varchar(55) NOT NULL,
  `NAME` varchar(45) NOT NULL,
  `PRICE` decimal(10,0) NOT NULL,
  `DESCRIPTION` mediumtext,
  `PUBLICATIONDATE` datetime DEFAULT NULL,
  `SALEDATE` datetime DEFAULT NULL,
  `RAWIMAGE` longtext,
  `STATES_CODE` int(11) NOT NULL,
  `CATEGORIES_ID` int(11) NOT NULL,
  `PRIVATE_MAIL_SHOW` tinyint(4) DEFAULT NULL,
  `PHONE_NUMBER_SHOW` tinyint(4) DEFAULT NULL,
  `ACTIVED` tinyint(4) DEFAULT '0',
  `ACTIVATIONDATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_ADS_STATES1_idx` (`STATES_CODE`),
  KEY `fk_ADS_CATEGORIES1_idx` (`CATEGORIES_ID`),
  KEY `fk_ADS_USERS1_idx` (`USERS_EDU_MAIL`),
  CONSTRAINT `fk_ADS_CATEGORIES1` FOREIGN KEY (`CATEGORIES_ID`) REFERENCES `categories` (`ID`),
  CONSTRAINT `fk_ADS_STATES1` FOREIGN KEY (`STATES_CODE`) REFERENCES `states` (`CODE`),
  CONSTRAINT `fk_ADS_USERS1` FOREIGN KEY (`USERS_EDU_MAIL`) REFERENCES `users` (`EDU_MAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ads_criterias_values`
--

DROP TABLE IF EXISTS `ads_criterias_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ads_criterias_values` (
  `ADS_ID` int(11) NOT NULL,
  `CRITERIAS_ID` int(11) NOT NULL,
  `CRITVAL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ADS_ID`,`CRITERIAS_ID`),
  KEY `fk_ADS_CRITERIAS_VALUES_CRITERIAS1_idx` (`CRITERIAS_ID`),
  CONSTRAINT `fk_ADS_CRITERIAS_VALUES_ADS1` FOREIGN KEY (`ADS_ID`) REFERENCES `ads` (`ID`),
  CONSTRAINT `fk_ADS_CRITERIAS_VALUES_CRITERIAS1` FOREIGN KEY (`CRITERIAS_ID`) REFERENCES `criterias` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ads_socialmedias_default`
--

DROP TABLE IF EXISTS `ads_socialmedias_default`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ads_socialmedias_default` (
  `ADS_ID` int(11) NOT NULL,
  `SOCIALMEDIAS_CODE` int(2) NOT NULL,
  `DEFAULT` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ADS_ID`,`SOCIALMEDIAS_CODE`),
  KEY `fk_ADS_SOCIALMEDIAS_DEFAULT_SOCIALMEDIAS1_idx` (`SOCIALMEDIAS_CODE`),
  CONSTRAINT `fk_ADS_SOCIALMEDIAS_DEFAULT_ADS1` FOREIGN KEY (`ADS_ID`) REFERENCES `ads` (`ID`),
  CONSTRAINT `fk_ADS_SOCIALMEDIAS_DEFAULT_SOCIALMEDIAS1` FOREIGN KEY (`SOCIALMEDIAS_CODE`) REFERENCES `socialmedias` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories_criterias`
--

DROP TABLE IF EXISTS `categories_criterias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories_criterias` (
  `CATEGORIES_ID` int(11) NOT NULL,
  `CRITERIAS_ID` int(11) NOT NULL,
  PRIMARY KEY (`CATEGORIES_ID`,`CRITERIAS_ID`),
  KEY `fk_CATEGORIES_CRITERIAS_CRITERIAS1_idx` (`CRITERIAS_ID`),
  CONSTRAINT `fk_CATEGORIES_CRITERIAS_CATEGORIES1` FOREIGN KEY (`CATEGORIES_ID`) REFERENCES `categories` (`ID`),
  CONSTRAINT `fk_CATEGORIES_CRITERIAS_CRITERIAS1` FOREIGN KEY (`CRITERIAS_ID`) REFERENCES `criterias` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `criterias`
--

DROP TABLE IF EXISTS `criterias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `criterias` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `preferences`
--

DROP TABLE IF EXISTS `preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `preferences` (
  `FLAGHOLIDAY` tinyint(4) NOT NULL,
  `DAYFORACTIVATION` int(11) DEFAULT NULL,
  PRIMARY KEY (`FLAGHOLIDAY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `CODE` int(2) NOT NULL,
  `NAME` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `socialmedias`
--

DROP TABLE IF EXISTS `socialmedias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `socialmedias` (
  `CODE` int(2) NOT NULL,
  `NAME` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `states` (
  `CODE` int(11) NOT NULL,
  `NAME` varchar(45) NOT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_socialmedias`
--

DROP TABLE IF EXISTS `user_socialmedias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_socialmedias` (
  `USERS_EDU_MAIL` varchar(55) NOT NULL,
  `SOCIALMEDIAS_CODE` int(2) NOT NULL,
  `DEFAULT` tinyint(4) DEFAULT NULL,
  `SCVALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`USERS_EDU_MAIL`,`SOCIALMEDIAS_CODE`),
  KEY `fk_USER_SOCIALMEDIAS_SOCIALMEDIAS1_idx` (`SOCIALMEDIAS_CODE`),
  CONSTRAINT `fk_USER_SOCIALMEDIAS_SOCIALMEDIAS1` FOREIGN KEY (`SOCIALMEDIAS_CODE`) REFERENCES `socialmedias` (`CODE`),
  CONSTRAINT `fk_USER_SOCIALMEDIAS_USERS1` FOREIGN KEY (`USERS_EDU_MAIL`) REFERENCES `users` (`EDU_MAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `EDU_MAIL` varchar(55) NOT NULL,
  `EDU_NAME` varchar(30) DEFAULT NULL,
  `PRIVATE_MAIL` varchar(55) DEFAULT NULL,
  `PRIVATE_MAIL_DEFAULT` tinyint(4) DEFAULT NULL,
  `PHONE_NUMBER` varchar(12) DEFAULT NULL,
  `PHONE_NUMBER_DEFAULT` tinyint(4) DEFAULT NULL,
  `URL_PROFIL_PIC` varchar(255) DEFAULT NULL,
  `ROLES_CODE` int(2) NOT NULL,
  `ACCEPT_CONDITIONS` tinyint(1) DEFAULT NULL,
  `BANN` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`EDU_MAIL`),
  KEY `fk_USERS_ROLES1_idx` (`ROLES_CODE`),
  CONSTRAINT `fk_USERS_ROLES1` FOREIGN KEY (`ROLES_CODE`) REFERENCES `roles` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-16 10:42:26
