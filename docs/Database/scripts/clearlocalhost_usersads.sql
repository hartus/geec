-- Clear all the users, ads from the GEEC
--
-- Host: localhost    Database: geec
-- ------------------------------------------------------

DELETE FROM geec.ads_criterias_values;
DELETE FROM geec.ads;
DELETE FROM geec.users;
