-- -----------------------------------------------------
-- TABLE de code des roles
-- -----------------------------------------------------

INSERT INTO `geec`.`roles` (`CODE`, `NAME`) VALUES ('99', 'Administrator');
INSERT INTO `geec`.`roles` (`CODE`, `NAME`) VALUES ('1', 'Elève');

-- -----------------------------------------------------
-- TABLE de code des etats
-- -----------------------------------------------------

INSERT INTO `geec`.`states` (`CODE`, `NAME`) VALUES ('1', 'Endommagé');
INSERT INTO `geec`.`states` (`CODE`, `NAME`) VALUES ('2', 'Bon état');
INSERT INTO `geec`.`states` (`CODE`, `NAME`) VALUES ('3', 'Neuf');

-- -----------------------------------------------------
-- TABLE de code des médias sociaux
-- -----------------------------------------------------

INSERT INTO `geec`.`socialmedias` (`CODE`, `NAME`) VALUES ('1', 'Facebook');
INSERT INTO `geec`.`socialmedias` (`CODE`, `NAME`) VALUES ('2', 'Instagram');
INSERT INTO `geec`.`socialmedias` (`CODE`, `NAME`) VALUES ('3', 'Twitter');

-- -----------------------------------------------------
-- TABLE de code des catégories
-- -----------------------------------------------------

INSERT INTO `geec`.`categories` (`ID`, `NAME`) VALUES ('1', 'Livres');
INSERT INTO `geec`.`categories` (`ID`, `NAME`) VALUES ('2', 'Calculatrices');

-- -----------------------------------------------------
-- TABLE de code des critères
-- -----------------------------------------------------

INSERT INTO `geec`.`criterias` (`ID`, `NAME`) VALUES ('1', 'ISBN');
INSERT INTO `geec`.`criterias` (`ID`, `NAME`) VALUES ('2', 'Année parution');
INSERT INTO `geec`.`criterias` (`ID`, `NAME`) VALUES ('3', 'Editeur');
INSERT INTO `geec`.`criterias` (`ID`, `NAME`) VALUES ('4', 'Marque');
INSERT INTO `geec`.`criterias` (`ID`, `NAME`) VALUES ('5', 'Modèle');

-- -----------------------------------------------------
-- TABLE de code de mapping entre les catégories et les critères
-- -----------------------------------------------------

INSERT INTO `geec`.`categories_criterias` (`CATEGORIES_ID`, `CRITERIAS_ID`) VALUES ('1', '1');
INSERT INTO `geec`.`categories_criterias` (`CATEGORIES_ID`, `CRITERIAS_ID`) VALUES ('1', '2');
INSERT INTO `geec`.`categories_criterias` (`CATEGORIES_ID`, `CRITERIAS_ID`) VALUES ('1', '3');
INSERT INTO `geec`.`categories_criterias` (`CATEGORIES_ID`, `CRITERIAS_ID`) VALUES ('2', '4');
INSERT INTO `geec`.`categories_criterias` (`CATEGORIES_ID`, `CRITERIAS_ID`) VALUES ('2', '5');
