echo on

set apacheroot=c:\Apache24
set geecroot=C:\WebRoot\geec

net stop "Apache2.4"

copy /Y "%geecroot%\docs\batches\httpd - geec.conf" "%apacheroot%\conf\httpd.conf"


net start "Apache2.4"
pause