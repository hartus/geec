# geec

Bourse d&#39;échange aux livres pour la maturité professionnelle


## Introduction
Ce projet web a pour but d'échanger des livres pour les élèves de la maturité professionnelle.

## Organisation

Il y a deux répertoires principaux
- docs
- src

docs contient la documentation du projet, src contient le code source


## Developers
To use the git repository, you have to be member of the git project located in bitbucket (Atlassian)

Check with Sandrine WEBERS

Once you have obtained an access to the GEEC repository you have to:
- install a valid version of git
- register your email and name with the following commands (git bash)
````
git config --global user.email "edu-myname@eduge.ch"
git config --global user.name "Firstname Name"
````

